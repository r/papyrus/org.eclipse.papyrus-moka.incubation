/*****************************************************************************
 * Copyright (c) 2017 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.parametric.ui.dnd;

import java.util.List;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.requests.DropObjectsRequest;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.ISpecializationType;
import org.eclipse.papyrus.infra.gmfdiag.dnd.strategy.ExpansionElementDropStrategy;
import org.eclipse.papyrus.moka.parametric.ui.dnd.command.BlockCreationCommand;
import org.eclipse.papyrus.sysml16.service.types.util.SysMLServiceTypeUtil;
import org.eclipse.papyrus.uml.diagram.composite.edit.parts.ClassCompositeCompartmentEditPart;

public class BlockDropStrategy extends ExpansionElementDropStrategy {

	private static final int LOWEST_PRIORITY = -1000;
	private static final String INTERNAL_STRUCTURE_ID = ClassCompositeCompartmentEditPart.VISUAL_ID;

	public BlockDropStrategy() {

	}

	@Override
	public Command doGetCommand(Request request, final EditPart targetEditPart) {
		final CompositeCommand cc = new CompositeCommand(getLabel());
		if (targetEditPart instanceof GraphicalEditPart) {
			IGraphicalEditPart graphicalEditPart = (IGraphicalEditPart) targetEditPart;
			List<EObject> sourceElements = getSourceEObjects(request);
			if (sourceElements.isEmpty()) {
				return null;
			}

			final ISpecializationType constraintPropertyElementType = (ISpecializationType) ElementTypeRegistry
					.getInstance().getType(SysMLServiceTypeUtil.ORG_ECLIPSE_PAPYRUS_SYSML16_CONSTRAINTBLOCK);
			if (INTERNAL_STRUCTURE_ID.equals(graphicalEditPart.getNotationView().getType())) {

				final EObject targetElement = getTargetSemanticElement(targetEditPart);
				for (EObject sourceElement : sourceElements) {
					if (constraintPropertyElementType.getMatcher().matches(sourceElement)) {

						Point dropedPoint = null;
						if (request instanceof DropObjectsRequest) {
							dropedPoint = ((DropObjectsRequest) request).getLocation();
						}

						cc.add(new BlockCreationCommand("Create the corresponding constraint block parameter.",
								graphicalEditPart, targetElement, sourceElement, dropedPoint));
					}
				}
			}
		}
		return cc.canExecute() ? new ICommandProxy(cc.reduce()) : null;
	}

	@Override
	public String getID() {
		return "org.eclipse.papyrus.moka.parametric.ui.dnd.BlockDropStrategy"; //$NON-NLS-1$
	}

	@Override
	public String getLabel() {
		return "Constraint Block inside a Parametric diagram"; //$NON-NLS-1$
	}

	@Override
	public String getDescription() {
		return "This strategy is is a specialization in order to be able to drop a Constraint Block inside a Parametric diagram."; //$NON-NLS-1$
	}

	@Override
	public int getPriority() {
		return LOWEST_PRIORITY;
	}

	@Override
	public String getCategoryID() {
		return "org.eclipse.papyrus.moka.parametric.ui"; //$NON-NLS-1$
	}

	@Override
	public String getCategoryLabel() {
		return "Parametric Constraint Block Drag and Drop"; //$NON-NLS-1$
	}
}
