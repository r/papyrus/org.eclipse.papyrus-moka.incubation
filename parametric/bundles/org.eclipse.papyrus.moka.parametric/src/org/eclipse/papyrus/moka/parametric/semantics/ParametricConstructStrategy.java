/*****************************************************************************
 * Copyright (c) 2017 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.moka.parametric.semantics;

import org.eclipse.papyrus.moka.fuml.structuredclassifiers.IObject_;
import org.eclipse.papyrus.moka.parametric.utils.NameUtils;
import org.eclipse.papyrus.moka.pscs.actions.additions.CS_NotNormativeDefaultConstructStrategy;
import org.eclipse.papyrus.moka.pscs.structuredclassifiers.ICS_Object;
import org.eclipse.papyrus.moka.pscs.structuredclassifiers.ICS_Reference;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Connector;

public class ParametricConstructStrategy extends CS_NotNormativeDefaultConstructStrategy {

	@Override
	public IObject_ constructObject(ICS_Object context, Class type) {
		this.locus = context.getLocus();
		return super.constructObject(context, type);
	}

	@Override
	public void generateArrayPattern(ICS_Reference context, Connector connector) {
		if (context.getReferent() instanceof IEvaluable) {
			// Deal with creation of binding links
			((IEvaluable)context.getReferent()).getBindingLinks().add(new BindingLink(connector, context.getReferent())) ;
		}
		else {
			super.generateArrayPattern(context, connector);
		}
	}

	@Override
	public boolean isArrayPattern(Connector c) {
		if (c.getAppliedStereotype(NameUtils.SYSML_BINDINGCONNECTOR_STEREOTYPE_NAME) == null) {
			return false;
		}
		else {
			if (c.getEnds().size() == 2) {
				if (c.getEnds().get(0).getLower() == 1) {
					if (c.getEnds().get(1).getLower() == 1) {
						// if (this.canInstantiate((Property) c.getEnds().get(0).getRole()) && this.canInstantiate((Property) c.getEnds().get(1).getRole())) {
						int cardinality1 = this.getCardinality(c.getEnds().get(0));
						int cardinality2 = this.getCardinality(c.getEnds().get(1));
						return cardinality1 == cardinality2;
						//}
					}
				}
			}
		}
		return false ;
	}

	@Override
	public boolean isStarPattern(Connector c) {
		if (c.getAppliedStereotype(NameUtils.SYSML_BINDINGCONNECTOR_STEREOTYPE_NAME) == null) {
			return false;
		}
		// TODO star pattern
		return false ;
	}



}
