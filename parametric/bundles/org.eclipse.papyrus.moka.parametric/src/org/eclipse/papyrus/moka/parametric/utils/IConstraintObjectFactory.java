/*****************************************************************************
 * Copyright (c) 2017 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.moka.parametric.utils;

import org.eclipse.papyrus.moka.parametric.semantics.ConstraintObject;
import org.eclipse.uml2.uml.Class; 

public interface IConstraintObjectFactory {
	
	public boolean canInstantiateConstraintObject(Class type) ;
	
	public ConstraintObject instantiate(Class type) ;

}
