/*****************************************************************************
 * Copyright (c) 2017 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.moka.parametric.semantics;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IFeatureValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IValue;
import org.eclipse.papyrus.moka.parametric.utils.NameUtils;
import org.eclipse.papyrus.moka.pscs.structuredclassifiers.CS_Object;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.InstanceValue;
import org.eclipse.uml2.uml.Slot;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.ValueSpecification;

public class ParametricObject extends CS_Object implements IEvaluable {

	protected InstanceSpecification sourceInstanceSpec ;
	protected List<BindingLink> bindingLinks = new ArrayList<BindingLink>() ;
	private EvaluationGraph eval;

	public InstanceSpecification getSourceInstanceSpec() {
		return sourceInstanceSpec;
	}

	public void setSourceInstanceSpec(InstanceSpecification sourceInstanceSpec) {
		this.sourceInstanceSpec = sourceInstanceSpec;
	}

	public List<BindingLink> getBindingLinks() {
		return bindingLinks;
	}

	public void evaluate() {
		if (eval == null) {
			 eval = new EvaluationGraph(this) ;
		}
		
		eval.evaluate() ;
	}

	@Override
	public ValueSpecification specify() {
		// Return an instance value that specifies this structured value.
		// Debug.println("[specify] StructuredValue...");
		InstanceValue instanceValue = UMLFactory.eINSTANCE.createInstanceValue();
		InstanceSpecification instance = UMLFactory.eINSTANCE.createInstanceSpecification();
		instanceValue.setType(null);
		instanceValue.setInstance(instance);
		instance.getClassifiers().addAll(this.getTypes());
		List<IFeatureValue> featureValues = this.getFeatureValues();
		// Debug.println("[specify] " + featureValues.size() + " feature(s).");
		for (int i = 0; i < featureValues.size(); i++) {
			IFeatureValue featureValue = featureValues.get(i);
			if (featureValue.getFeature().getType().getAppliedStereotype(NameUtils.SYSML_CONSTRAINTBLOCK_STEREOTYPE_NAME) == null) {
				Slot slot = UMLFactory.eINSTANCE.createSlot();
				slot.setDefiningFeature(featureValue.getFeature());
				// Debug.println("[specify] feature = " + featureValue.feature.name
				// + ", " + featureValue.values.size() + " value(s).");
				List<IValue> values = featureValue.getValues();
				for (int j = 0; j < values.size(); j++) {
					IValue value = values.get(j);
					ValueSpecification valueSpec = value.specify();
					slot.getValues().add(valueSpec);
				}
				instance.getSlots().add(slot);
			}
		}
		return instanceValue;
	}

}
