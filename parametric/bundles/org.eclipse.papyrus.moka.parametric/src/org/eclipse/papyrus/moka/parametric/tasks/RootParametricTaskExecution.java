/*****************************************************************************
 * Copyright (c) 2020 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.parametric.tasks;

import org.eclipse.papyrus.moka.engine.uml.scheduling.UMLRootTaskExecution;
import org.eclipse.papyrus.moka.fuml.structuredclassifiers.IObject_;
import org.eclipse.papyrus.moka.kernel.scheduling.control.IExecutionLoop;
import org.eclipse.papyrus.moka.parametric.ParametricEvaluator;
import org.eclipse.papyrus.moka.parametric.PostParametricEvaluationTask;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.InstanceValue;
import org.eclipse.uml2.uml.ValueSpecification;

public class RootParametricTaskExecution extends UMLRootTaskExecution<InstanceSpecification>{
	
	protected ParametricEvaluator evaluator;
	protected PostParametricEvaluationTask postEvalTask;
	
	public RootParametricTaskExecution(IExecutionLoop loop, InstanceSpecification executionRoot) {
		super(loop, executionRoot);
	}

	public void setEvaluator(ParametricEvaluator evaluator) {
		this.evaluator = evaluator;
	}
	
	public void setPostEvalTask(PostParametricEvaluationTask postEvalTask) {
		this.postEvalTask = postEvalTask;
	}

	@Override
	public void execute() {
		if (root.getClassifiers().isEmpty())
			return;
		
		evaluator.init(root, locus);
		IObject_ o = evaluator.evaluate();
		
		ValueSpecification v = o.specify() ;
		postEvalTask.run(root, ((InstanceValue)v).getInstance());
	}
	
}
