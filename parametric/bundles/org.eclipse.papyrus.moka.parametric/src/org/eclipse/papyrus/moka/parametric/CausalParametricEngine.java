/*****************************************************************************
 * Copyright (c) 2020 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.parametric;

import org.eclipse.papyrus.moka.engine.uml.UMLExecutionEngine;
import org.eclipse.papyrus.moka.fuml.loci.ILocus;
import org.eclipse.papyrus.moka.fuml.tasks.IUMLTaskExecutionFactory;
import org.eclipse.papyrus.moka.parametric.semantics.ParametricConstructStrategy;
import org.eclipse.papyrus.moka.parametric.semantics.ParametricExecutionFactory;
import org.eclipse.papyrus.moka.parametric.semantics.ParametricLocus;
import org.eclipse.papyrus.moka.parametric.tasks.CausalParametricTaskExecutionFactory;
import org.eclipse.papyrus.moka.pscs.loci.CS_Executor;

public class CausalParametricEngine extends UMLExecutionEngine{
	
	private ParametricEvaluator evaluator;
	private PostParametricEvaluationTask postEvalTask;
	
	public CausalParametricEngine() {
		this(new ParametricEvaluator(), new SerializeResultTask());
	}

	public CausalParametricEngine(ParametricEvaluator evaluator, PostParametricEvaluationTask task) {
		super();
		this.evaluator = evaluator;
		this.postEvalTask = task;
	}

	@Override
	public ILocus createLocus() {
		ILocus locus = new ParametricLocus();
		locus.setExecutor(new CS_Executor());
		locus.setFactory(new ParametricExecutionFactory());
		return locus;
	}

	@Override
	public void installSemanticStrategies() {
		super.installSemanticStrategies();
		locus.getFactory().setStrategy(new ParametricConstructStrategy());
	}

	@Override
	protected IUMLTaskExecutionFactory createUMLTaskFactory() {
		return new CausalParametricTaskExecutionFactory(controller.getExecutionLoop());
	}
}
