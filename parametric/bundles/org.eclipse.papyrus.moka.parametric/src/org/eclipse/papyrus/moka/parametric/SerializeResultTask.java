/*****************************************************************************
 * Copyright (c) 2020 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.parametric;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.papyrus.infra.emf.utils.ServiceUtilsForEObject;
import org.eclipse.swt.widgets.Display;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.InstanceValue;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Slot;
import org.eclipse.uml2.uml.ValueSpecification;

public class SerializeResultTask implements PostParametricEvaluationTask {

	@Override
	public void run(InstanceSpecification root, InstanceSpecification result) {
		try {
			TransactionalEditingDomain dom = ServiceUtilsForEObject.getInstance().getTransactionalEditingDomain(root);
			SerializeComputationResultCommand command = new SerializeComputationResultCommand(dom, (InstanceSpecification)result, (InstanceSpecification)root) ;
			command.setLabel("Workflow Test Execution"); //$NON-NLS-1$
			Display.getDefault().asyncExec(new Runnable() {
				public void run() {
					dom.getCommandStack().execute(command);
				}
			});
		}
		catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	protected class SerializeComputationResultCommand extends RecordingCommand {

		private final InstanceSpecification computed;
		private final InstanceSpecification sourceInstanceSpec ;

		public SerializeComputationResultCommand(TransactionalEditingDomain domain, InstanceSpecification computed, InstanceSpecification sourceInstanceSpec) {
			super(domain);
			this.computed = computed ;
			this.sourceInstanceSpec = sourceInstanceSpec ;
		}

		private boolean serializeComputationResult(InstanceSpecification computed, InstanceSpecification source) {
			// For serialization, a new package is created
			// New instance specifications are created (even if instance specifications related to the new instance spec could be reused)
			Package nearestPackage = source.getNearestPackage() ;
			SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy //$NON-NLS-1$
			Date now = new Date();
			String strDate = sdfDate.format(now);
			Package resultPackage = nearestPackage.createNestedPackage("" + source.getName() + " - Exec - " + strDate) ; //$NON-NLS-1$ //$NON-NLS-2$

			List<InstanceSpecification> toBeAdded = new ArrayList<InstanceSpecification>() ;
			toBeAdded.add(computed) ;
			while (!toBeAdded.isEmpty()) {
				InstanceSpecification instance = toBeAdded.remove(0) ;
				if (instance.getModel() == null) {
					resultPackage.getPackagedElements().add(instance) ;
					//instance.setName(! instance.getClassifiers().isEmpty() ? instance.getClassifiers().get(0).getName() : "null") ;
				}
				for (Slot s : instance.getSlots()) {
					for (ValueSpecification v : s.getValues()) {
						if (v instanceof InstanceValue) {
							toBeAdded.add(((InstanceValue)v).getInstance()) ;
						}
					}
				}
			}

			computed.setName("" + sourceInstanceSpec.getName()); //$NON-NLS-1$
			return true;
		}

		@Override
		protected void doExecute() {
			serializeComputationResult(computed, sourceInstanceSpec) ;
		}
	}

}
