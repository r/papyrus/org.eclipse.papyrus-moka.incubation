/*****************************************************************************
 * Copyright (c) 2017 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.moka.parametric.semantics;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.papyrus.moka.parametric.utils.NameUtils;
import org.eclipse.papyrus.moka.pscs.structuredclassifiers.CS_Object;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Property;

public class ConstraintObject extends CS_Object implements IEvaluable {

	protected List<BindingLink> bindingLinks = new ArrayList<BindingLink>();

	protected EvaluationGraph graph ;
	public void evaluate() {
		if (this.isCompositeConstraint()) {
			if (graph == null) {
				graph =  new EvaluationGraph(this);
			}
			graph.evaluate();
		}
	}

	public boolean isCompositeConstraint() {
		Class constraintBlock = (Class) this.getTypes().get(0);
		for (Property p : constraintBlock.allAttributes()) {
			if (p.getType() != null
					&& p.getType().getAppliedStereotype(NameUtils.SYSML_CONSTRAINTBLOCK_STEREOTYPE_NAME) != null) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean isCompositeConstraint(Class constraintBlock) {
		if (constraintBlock.getAppliedStereotype(NameUtils.SYSML_CONSTRAINTBLOCK_STEREOTYPE_NAME) == null) {
			return false ;
		}
		for (Property p : constraintBlock.allAttributes()) {
			if (p.getType() != null
					&& p.getType().getAppliedStereotype(NameUtils.SYSML_CONSTRAINTBLOCK_STEREOTYPE_NAME) != null) {
				return true;
			}
		}
		return false;
	}

	public boolean isInputParam(Property p) {
		// FIXME this rule generally applies to causal constraint objects
		if (p.getType().getAppliedStereotype(NameUtils.SYSML_CONSTRAINTBLOCK_STEREOTYPE_NAME) == null) {
			return !p.isDerived();
		}
		return false;
	}

	public boolean isOutputParam(Property p) {
		// FIXME this rule generally applies to causal constraint objects
		if (p.getType().getAppliedStereotype(NameUtils.SYSML_CONSTRAINTBLOCK_STEREOTYPE_NAME) == null) {
			return p.isDerived();
		}
		return false;
	}

	public List<BindingLink> getBindingLinks() {
		return bindingLinks;
	}
}
