/*****************************************************************************
 * Copyright (c) 2017 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.moka.parametric.semantics;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.papyrus.moka.fuml.structuredclassifiers.IObject_;
import org.eclipse.papyrus.moka.parametric.utils.IConstraintObjectFactory;
import org.eclipse.papyrus.moka.parametric.utils.NameUtils;
import org.eclipse.papyrus.moka.pscs.loci.CS_Locus;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;

public class ParametricLocus extends CS_Locus {
	
	protected static List<IConstraintObjectFactory> registeredFactories ;
	
	protected IConstraintObjectFactory getConstraintObjectFactory(Class constraintType) {
		if (registeredFactories == null) {
			instantiateConstraintObjectFactories();
		}
		for (IConstraintObjectFactory f : registeredFactories) {
			if (f.canInstantiateConstraintObject(constraintType)) {
				return f ;
			}
		}
		return null ;
	}
	
	protected void instantiateConstraintObjectFactories() {
		// Read the extension point to find the corresponding execution engine. Create an instance
		// of this latter and then return it.
		registeredFactories = new ArrayList<IConstraintObjectFactory>() ;
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IConfigurationElement[] config = registry.getConfigurationElementsFor(NameUtils.MOKA_CONSTRAINTOBJECTFACTORY_EXT_POINT);
		try {
			for (int i = 0; i < config.length; i++) {
				Object o = config[i].createExecutableExtension("class"); //$NON-NLS-1$
				registeredFactories.add((IConstraintObjectFactory)o) ;
			}
		} catch (CoreException ex) {
			org.eclipse.papyrus.infra.core.Activator.log.equals(ex);
		} catch (Exception ex) {
			org.eclipse.papyrus.infra.core.Activator.log.equals(ex);
		}
	}

	@Override
	public IObject_ instantiate(Class type) {
		IObject_ object = null ;
		
		
		
		// Instantiate a constraint object if there is a factory available for this type
		IConstraintObjectFactory constraintObjectFactory = getConstraintObjectFactory(type) ;
		if (constraintObjectFactory != null) {
			object = constraintObjectFactory.instantiate(type);
			object.addType(type);
			object.createFeatureValues();
			this.add(object);;
		}
		// If no factory is available, but the constraint is a composite constraint, instantiate a generic ConstraintObject
		else if (ConstraintObject.isCompositeConstraint(type)) {
			object = new ConstraintObject();
			object.addType(type);
			object.createFeatureValues();
			this.add(object);;
		}
		// instantiate a parametric object if the type is not stereotyped by Constraint Block, but contains both constraint properties
		// and parts
		else if (isParametricBlock(type)) {
			object = new ParametricObject();
			object.addType(type);
			object.createFeatureValues();
			this.add(object);;
		}
		else {
			object = super.instantiate(type) ;
		}
		return object ;
	}

	// returns if the type is not stereotyped by Constraint Block, but contains both constraint properties
	// and parts
	public static boolean isParametricBlock(Class type) {
		if (type.getAppliedStereotype(NameUtils.SYSML_CONSTRAINTBLOCK_STEREOTYPE_NAME) != null)
			return false ;
		boolean containsConstraintProperties = false ;
		boolean containsParts = false ;
		for (int i = 0 ; i < type.getAttributes().size() && !(containsConstraintProperties && containsParts) ; i++) {
			Property p = type.getAttributes().get(i) ;
			Type t = p.getType() ;
			if (t != null) {
				if (t.getAppliedStereotype(NameUtils.SYSML_CONSTRAINTBLOCK_STEREOTYPE_NAME) != null) {
					containsConstraintProperties = true ;
				}
				else {
					containsParts = true ;
				}
			}
		}
		return containsConstraintProperties && containsParts ;
	}
	
	public static boolean isDataSource(Classifier type) {
		if (type instanceof DataType && type.getAppliedStereotype(NameUtils.DATAVISUALIZATION_DATASOURCE_STEREOTYPE_NAME) != null) {
			return true ;
		}
		return false ;
	}
}
