/*****************************************************************************
 * Copyright (c) 2017 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.moka.parametric.semantics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IValue;
import org.eclipse.papyrus.moka.fuml.structuredclassifiers.IObject_;
import org.eclipse.papyrus.moka.fuml.structuredclassifiers.IReference;
import org.eclipse.papyrus.moka.fuml.structuredclassifiers.Object_;
import org.eclipse.papyrus.moka.fuml.structuredclassifiers.Reference;
import org.eclipse.papyrus.moka.parametric.utils.NameUtils;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;

public class BindingLink {

	protected Connector bindingConnector ;
	protected IObject_ container ;
	protected Map<ConnectorEnd, List<Property>> connectorEnd2ObjectMap = new HashMap<ConnectorEnd, List<Property>>() ;

	public BindingLink(Connector bindingConnector, IObject_ container) {
		this.bindingConnector = bindingConnector;
		this.container = container;
		for (ConnectorEnd cEnd : bindingConnector.getEnds()) {
			List<Property> propertyPath = new ArrayList<>() ;
			if (cEnd.getAppliedStereotype(NameUtils.SYSML_NESTEDCONNECTOREND_STEREOTYPE_NAME) != null) {
				Stereotype nestedConnectordEndStereotype = cEnd.getAppliedStereotype(NameUtils.SYSML_NESTEDCONNECTOREND_STEREOTYPE_NAME) ;
				Object l = cEnd.getValue(nestedConnectordEndStereotype, NameUtils.SYSML_PROPERTYPATH_PROPERTY_NAME) ;
				propertyPath.addAll((EObjectResolvingEList<Property>)l) ;
			}
			propertyPath.add((Property)cEnd.getRole()) ;
			connectorEnd2ObjectMap.put(cEnd, propertyPath) ;
		}
	}

	public IObject_ getLastObjectOnPath(IObject_ obj, List<Property> propertyPath) {
		List<IValue> values = new ArrayList<>() ;
		values.add(obj) ;
		for (int i = 0 ; i < propertyPath.size() ; i++) {
			if (! values.isEmpty()) {
				if (values.get(0) instanceof Object_ || values.get(0) instanceof Reference) {
					IObject_ tmpObj = (values.get(0) instanceof Reference ? ((Reference)values.get(0)).referent : (Object_)values.get(0)) ;
					values = tmpObj.getFeatureValue(propertyPath.get(i)).getValues() ;
				}
			}
			//values = values.get(0).getFeatureValue(propertyPath.get(i)) ;
		}
		return values.isEmpty() ? null : ((values.get(0) instanceof Reference ? ((Reference)values.get(0)).referent : (Object_)values.get(0))) ;
	}
	
	public List<IValue> getValue(ConnectorEnd end) {
		List<Property> propertyPath = this.connectorEnd2ObjectMap.get(end) ;
		if (propertyPath != null) {
			return evaluatePropertyPathValue(propertyPath) ;
		}
		return null ;
	}
	
	public IObject_ getFirstObjectFromPropertyPath(ConnectorEnd end) {
		List<Property> propertyPath = this.connectorEnd2ObjectMap.get(end) ;
		if (propertyPath != null) {
			List<Property> partialPropertyPath = new ArrayList<Property>() ;
			partialPropertyPath.add(propertyPath.get(0)) ;
			List<IValue> result = evaluatePropertyPathValue(partialPropertyPath) ;
			if (result != null && !result.isEmpty()) {
				if (result.get(0) instanceof IObject_) {
					return (IObject_)result.get(0) ;
				}
				else if (result.get(0) instanceof IReference) {
					return ((IReference)result.get(0)).getReferent() ;
				}
			}
		}
		return null ;
	}
	
	public List<IValue> evaluatePropertyPathValue (List<Property> propertyPath) {
		List<IValue> values = new ArrayList<>() ;
		values.add(this.container) ;
		for (int i = 0 ; i < propertyPath.size() ; i++) {
			if (! values.isEmpty()) {
				if (values.get(0) instanceof Object_ || values.get(0) instanceof Reference) {
					IObject_ tmpObj = (values.get(0) instanceof Reference ? ((Reference)values.get(0)).referent : (Object_)values.get(0)) ;
					values = tmpObj.getFeatureValue(propertyPath.get(i)).getValues() ;
				}
			}
			//values = values.get(0).getFeatureValue(propertyPath.get(i)) ;
		}
		return values ;
	}
	
	public void propagateValues(ConnectorEnd from, ConnectorEnd to) {
		List<IValue> valuesToPropagate = getValue(from) ;
		List<IValue> values = new ArrayList<>() ;
		values.add(this.container) ;
		List<Property> toPropertyPath = this.connectorEnd2ObjectMap.get(to) ;
		for (int i = 0 ; i < toPropertyPath.size()-1 ; i++) {
			if (! values.isEmpty()) {
				if (values.get(0) instanceof Object_ || values.get(0) instanceof Reference) {
					IObject_ tmpObj = (values.get(0) instanceof Reference ? ((Reference)values.get(0)).referent : (Object_)values.get(0)) ;
					values = tmpObj.getFeatureValue(toPropertyPath.get(i)).getValues() ;
				}
			}
			//values = values.get(0).getFeatureValue(propertyPath.get(i)) ;
		}
		if (! values.isEmpty()) {
			if (values.get(0) instanceof Object_ || values.get(0) instanceof Reference) {
				IObject_ tmpObj = (values.get(0) instanceof Reference ? ((Reference)values.get(0)).referent : (Object_)values.get(0)) ;
				tmpObj.setFeatureValue((Property)to.getRole(), valuesToPropagate, 0);
			}
		}
	}
	
}
