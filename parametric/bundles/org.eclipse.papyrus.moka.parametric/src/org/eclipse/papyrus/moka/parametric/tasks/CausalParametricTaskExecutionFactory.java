/*****************************************************************************
 * Copyright (c) 2020 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.parametric.tasks;

import org.eclipse.papyrus.moka.engine.uml.scheduling.UMLTaskExecutionFactory;
import org.eclipse.papyrus.moka.fuml.tasks.IUMLRootTaskExecution;
import org.eclipse.papyrus.moka.kernel.scheduling.control.IExecutionLoop;
import org.eclipse.papyrus.moka.parametric.ParametricEvaluator;
import org.eclipse.papyrus.moka.parametric.PostParametricEvaluationTask;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.InstanceSpecification;

public class CausalParametricTaskExecutionFactory extends UMLTaskExecutionFactory{
	
	public CausalParametricTaskExecutionFactory(IExecutionLoop loop) {
		super(loop);
	}
	
	private ParametricEvaluator evaluator;
	protected PostParametricEvaluationTask postEvalTask;
	
	@Override
	public IUMLRootTaskExecution<?> createRootExecution(Element executionRoot) {
		RootParametricTaskExecution rootExecution = null;
		if (executionRoot instanceof InstanceSpecification) {
			rootExecution = new RootParametricTaskExecution(executionLoop, (InstanceSpecification) executionRoot);
			rootExecution.setEvaluator(evaluator);
			rootExecution.setPostEvalTask(postEvalTask);
			return rootExecution;
		}
		
		return super.createRootExecution(executionRoot);
	}
	
	public ParametricEvaluator getEvaluator() {
		return evaluator;
	}

	public void setEvaluator(ParametricEvaluator evaluator) {
		this.evaluator = evaluator;
	}

	public PostParametricEvaluationTask getPostEvalTask() {
		return postEvalTask;
	}

	public void setPostEvalTask(PostParametricEvaluationTask postEvalTask) {
		this.postEvalTask = postEvalTask;
	}
	
}
