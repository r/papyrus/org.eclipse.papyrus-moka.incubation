/*****************************************************************************
 * Copyright (c) 2017 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.moka.parametric.semantics;

import org.eclipse.uml2.uml.ConnectorEnd;

public class UpdateDescriptor {
	
	protected BindingLink link ;
	protected ConnectorEnd from ;
	protected ConnectorEnd to ;
	
	public UpdateDescriptor(BindingLink link, ConnectorEnd from, ConnectorEnd to) {
		super();
		this.link = link;
		this.from = from;
		this.to = to;
	}
	
	public void performUpdate() {
		link.propagateValues(from, to);
//		System.out.println("Propagating from " 
//						+ from.getRole().getQualifiedName() 
//						+ " to "
//						+ to.getRole().getQualifiedName());
	}
	
}
