/*****************************************************************************
 * Copyright (c) 2017 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.moka.parametric.semantics;

import org.eclipse.papyrus.moka.fuml.loci.ISemanticVisitor;
import org.eclipse.papyrus.moka.pscs.loci.CS_ExecutionFactory;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.InstanceValue;

public class ParametricExecutionFactory extends CS_ExecutionFactory {

	@Override
	public ISemanticVisitor instantiateVisitor(Element element) {
		ISemanticVisitor visitor = null;
		if (element instanceof InstanceValue) {
			visitor = new Parametric_InstanceValueEvaluation();
		} else {
			visitor = super.instantiateVisitor(element);
		}
		return visitor;
	}

}
