/*****************************************************************************
 * Copyright (c) 2017 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.moka.parametric.utils;

public class NameUtils {
	
	// extension points
	public static String MOKA_CONSTRAINTOBJECTFACTORY_EXT_POINT = "org.eclipse.papyrus.moka.parametric.constraintObjectFactory" ; //$NON-NLS-1$
	
	// stereotypes
	public static String DATAVISUALIZATION_DATASOURCE_STEREOTYPE_NAME = "DataVisualizationProfile::DataSource" ; //$NON-NLS-1$
	public static String SYSML_BINDINGCONNECTOR_STEREOTYPE_NAME = "SysML::Blocks::BindingConnector" ; //$NON-NLS-1$
	public static String SYSML_CONSTRAINTBLOCK_STEREOTYPE_NAME = "SysML::ConstraintBlocks::ConstraintBlock" ; //$NON-NLS-1$
	public static String SYSML_NESTEDCONNECTOREND_STEREOTYPE_NAME = "SysML::Blocks::NestedConnectorEnd" ; //$NON-NLS-1$
	public static String MOKA_PARAMETRIC_APPLICATION_STEREOTYPE_NAME = "MokaParametric::StereotypeApplication";
	
	// attributes
	public static String SYSML_PROPERTYPATH_PROPERTY_NAME = "propertyPath" ; //$NON-NLS-1$
}
