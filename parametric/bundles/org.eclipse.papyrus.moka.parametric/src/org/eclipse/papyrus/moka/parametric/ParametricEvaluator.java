/*****************************************************************************
 * Copyright (c) 2020 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.parametric;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.papyrus.moka.fuml.loci.ILocus;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IFeatureValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IntegerValue;
import org.eclipse.papyrus.moka.fuml.structuredclassifiers.IObject_;
import org.eclipse.papyrus.moka.fuml.structuredclassifiers.Reference;
import org.eclipse.papyrus.moka.parametric.semantics.ParametricConstructStrategy;
import org.eclipse.papyrus.moka.parametric.semantics.ParametricObject;
import org.eclipse.papyrus.moka.parametric.utils.NameUtils;
import org.eclipse.papyrus.moka.pscs.structuredclassifiers.ICS_Object;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Slot;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.ValueSpecification;

public class ParametricEvaluator {
	protected ParametricObject mainObject;
	protected List<ParametricObject> preEvaluateObjects;
	
	public ParametricObject init(InstanceSpecification instance, ILocus locus) {
		IObject_ object = locus.instantiate((Class)instance.getClassifiers().get(0)) ;
		
		if( !(object instanceof ParametricObject) )
			return null;
		
		ParametricConstructStrategy strategy = (ParametricConstructStrategy)locus.getFactory().getStrategy("constructStrategy") ; //$NON-NLS-1$
		strategy.constructObject((ICS_Object)object, (Class)object.getTypes().get(0)) ;
		if (object instanceof ParametricObject) {
			((ParametricObject)object).setSourceInstanceSpec(instance);
		}
		List<Slot> instanceSlots = instance.getSlots();
		for (int i = 0; i < instanceSlots.size(); i++) {
			Slot slot = instanceSlots.get(i);
			if (slot.getAppliedStereotype(NameUtils.MOKA_PARAMETRIC_APPLICATION_STEREOTYPE_NAME) != null) {
				Stereotype s = slot.getAppliedStereotype(NameUtils.MOKA_PARAMETRIC_APPLICATION_STEREOTYPE_NAME) ;
				Element e = (Element)slot.getValue(s, "element") ;
				if (e != null) { // The model is correct if the stereotype applied on e is the type of the defining feature of slot
					IObject_ o = locus.instantiate((Class)slot.getDefiningFeature().getType()) ;
					for (Property p : ((Class)slot.getDefiningFeature().getType()).getAllAttributes()) {
						Object v = e.getValue((Stereotype)slot.getDefiningFeature().getType(), p.getName()) ;
						List<IValue> values = new ArrayList<IValue>() ;
						if (p.getType() instanceof PrimitiveType) {
							IntegerValue intValue = new IntegerValue() ;
							intValue.value = (Integer)v ;
							intValue.type = (PrimitiveType)p.getType() ;
							values.add(intValue) ;
						}
						o.setFeatureValue(p, values, 0);
					}
					List<IValue> values = new ArrayList<IValue>();
					values.add(o) ;
					object.setFeatureValue(slot.getDefiningFeature(), values, 0);
				}
			}
			else {
				List<IValue> values = new ArrayList<IValue>();
				List<ValueSpecification> slotValues = slot.getValues();
				for (int j = 0; j < slotValues.size(); j++) {
					ValueSpecification slotValue = slotValues.get(j);
					values.add(locus.getExecutor().evaluate(slotValue));
				}
				object.setFeatureValue(slot.getDefiningFeature(), values, 0);
			}
		}
		
		mainObject = (ParametricObject)object;
		preEvaluateObjects = this.computeEvalList(object) ;		
		return mainObject;
	}
	
	protected List<ParametricObject> computeEvalList(IObject_ object) {
		List<ParametricObject> objectsToEvalFirst = new ArrayList<ParametricObject>() ;
		for (IFeatureValue featureValues : object.getFeatureValues()) {
			for (IValue v : featureValues.getValues()) {
				if (v instanceof Reference) {
					IObject_ obj = ((Reference)v).getReferent() ;
					if (obj instanceof ParametricObject) {
						objectsToEvalFirst.add((ParametricObject)obj) ;
					}
					objectsToEvalFirst.addAll(0, this.computeEvalList(obj)) ;
				}
			}
		}
		return objectsToEvalFirst ;
	}
	
	public IObject_ evaluate() {
		for( ParametricObject o : preEvaluateObjects )
			o.evaluate();
		
		mainObject.evaluate();
		
		return mainObject;
	}
	
	public ParametricObject getParametricObject() {
		return mainObject;
	}
}
