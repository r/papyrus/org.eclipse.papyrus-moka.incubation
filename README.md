To build annotations
====================
> mvn clean verify -P simex,annotations

To build externalcontrol
========================
> mvn clean verify -P externalcontrol

To build fmi
=============
> mvn clean verify -P fmi

To build instancespecification
==============================
> mvn clean verify -P instancespecification

To build parametric
===================
> mvn clean verify -P parametric

To build scripting
==================
> mvn clean verify -P scripting

To build simex
==============
> mvn clean verify -P simex

To build suml
=============
> mvn clean verify -P suml

To build suml
=============
> mvn clean verify -P visualization
