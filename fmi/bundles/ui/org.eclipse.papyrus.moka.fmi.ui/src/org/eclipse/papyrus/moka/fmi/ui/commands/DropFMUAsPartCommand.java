/*****************************************************************************
 * 
 * Copyright (c) 2016 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST Initial API and implementation
 * 
 *****************************************************************************/
package org.eclipse.papyrus.moka.fmi.ui.commands;

import java.util.Collections;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.requests.DropObjectsRequest;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.papyrus.infra.gmfdiag.common.helper.NotationHelper;
import org.eclipse.papyrus.moka.fmi.ui.util.FMUViewUtil;
import org.eclipse.papyrus.moka.ssp.profile.custom.StereotypeStrings;
import org.eclipse.papyrus.uml.tools.utils.NamedElementUtil;
import org.eclipse.uml2.uml.AggregationKind;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;

public class DropFMUAsPartCommand extends AbstractTransactionalCommand {

	



	protected Class targetSimulator;
	protected Class sourceType;
	protected DropObjectsRequest request;
	protected Property newPart;
	protected GraphicalEditPart targetGraphicalEditPart;

	protected View targetView;

	protected TransactionalEditingDomain domain;

	

	public DropFMUAsPartCommand(DropObjectsRequest request, TransactionalEditingDomain domain,
			Class containerClass, Class sourceType, GraphicalEditPart targetEditPart) {
		super(domain, "Create a composite part from FMU and create graphical view",
				getWorkspaceFiles(containerClass));
		this.targetSimulator = containerClass;
		this.sourceType = sourceType;
		this.request = request;
		this.targetGraphicalEditPart = targetEditPart;
		
		this.domain = domain;
	}

	@Override
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {

		createNewPart();
		
		Point location = request.getLocation().getCopy();
		targetGraphicalEditPart.getContentPane().translateToRelative(location);
		FMUViewUtil.createGraphicalViews(newPart, targetGraphicalEditPart, NotationHelper.findView(targetGraphicalEditPart),location, null, Collections.<Port>emptyList());

		request.setResult(newPart);
		return CommandResult.newOKCommandResult(newPart);

	}

	protected void createNewPart() {
		

		String partName = sourceType.getName();

		newPart = targetSimulator.createOwnedAttribute(partName, sourceType);
		newPart.setName(NamedElementUtil.getDefaultNameWithIncrementFromBase(partName,targetSimulator.getOwnedAttributes()));
		newPart.setAggregation(AggregationKind.COMPOSITE_LITERAL);
		
		Stereotype stereotype = newPart.getApplicableStereotype(StereotypeStrings.SSDCOMPONENT_QUALIFIEDNAME);
		if (stereotype != null) {
			newPart.applyStereotype(stereotype);
		}
		
	}





	
}
