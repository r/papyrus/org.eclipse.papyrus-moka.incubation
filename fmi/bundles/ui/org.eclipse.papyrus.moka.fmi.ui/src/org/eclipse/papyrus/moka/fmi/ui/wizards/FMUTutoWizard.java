/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.moka.fmi.ui.wizards;

import org.eclipse.papyrus.uml.diagram.wizards.wizards.PapyrusExampleInstallerWizard;

/**
 * Copy wizard for the HelloWorld example
 */
public class FMUTutoWizard extends PapyrusExampleInstallerWizard {

	public FMUTutoWizard() {
		super();
	}
}
