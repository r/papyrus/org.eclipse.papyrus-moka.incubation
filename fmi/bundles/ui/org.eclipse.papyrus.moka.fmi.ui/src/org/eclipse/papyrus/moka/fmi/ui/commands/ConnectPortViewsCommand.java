/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.moka.fmi.ui.commands;

import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.papyrus.moka.fmi.ui.util.GroupPortUtils;

public class ConnectPortViewsCommand  extends RecordingCommand{

	private GroupPortCommand groupPortCommand;

	public ConnectPortViewsCommand(TransactionalEditingDomain domain, GroupPortCommand groupPortCommand) {
		super(domain);
		this.groupPortCommand = groupPortCommand;
		
	}

	@Override
	protected void doExecute() {
		View sourcePortView = groupPortCommand.getSourcePortView();
		View targetPortView = groupPortCommand.getTargetPortView();
		
		if (sourcePortView != null && targetPortView != null) {
			GroupPortUtils.connectBuses(sourcePortView, targetPortView);
		}
		
	}
}
