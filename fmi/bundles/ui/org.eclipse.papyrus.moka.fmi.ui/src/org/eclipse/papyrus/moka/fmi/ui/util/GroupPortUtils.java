/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.moka.fmi.ui.util;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.notation.Bounds;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.papyrus.ease.module.PapyrusUtilsModule;
import org.eclipse.papyrus.ease.module.RequestUtils;
import org.eclipse.papyrus.moka.fmi.fmiprofile.FMIPort;
import org.eclipse.papyrus.moka.ssp.omsimulatorprofile.OMSimulatorBus;
import org.eclipse.papyrus.moka.ssp.omsimulatorprofile.util.OMSimulatorProfileUtil;
import org.eclipse.papyrus.uml.service.types.element.UMLDIElementTypes;
import org.eclipse.papyrus.uml.service.types.element.UMLElementTypes;
import org.eclipse.uml2.uml.AggregationKind;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.util.UMLUtil;

public class GroupPortUtils {

	public static boolean canBeGrouped(List<EObject> elements) {
		return (elements.stream()
				.allMatch(elem -> elem instanceof Port
						&& UMLUtil.getStereotypeApplication((Port) elem, FMIPort.class) != null)
				&& 
				elements.stream().map(p -> (Element) p).collect(Collectors.groupingBy(Element::getOwner))
						.size() == 1);
	}
	

	
	public static List<GraphicalEditPart> getConnectedPortsEditPart(List<GraphicalEditPart> portsEditParts){
		List<GraphicalEditPart> result = new ArrayList<GraphicalEditPart>();
		
		for (GraphicalEditPart portEditPart:portsEditParts) {
			List<ConnectionEditPart> sourceConnections = portEditPart.getSourceConnections();
			for (ConnectionEditPart sourceConnection: sourceConnections ) {
				result.add((GraphicalEditPart) sourceConnection.getTarget());
			}
			
			List<ConnectionEditPart> targetConnections = portEditPart.getTargetConnections();
			for (ConnectionEditPart sourceConnection: targetConnections ) {
				result.add((GraphicalEditPart) sourceConnection.getSource());
			}
			
		}
		
		return result;
	}



	public static View groupPorts(
			List<GraphicalEditPart> portsToGroupEditParts) {
		org.eclipse.uml2.uml.Class owningClass = ((Port) portsToGroupEditParts.get(0).resolveSemanticElement()).getClass_();
		
		
		Port newPort = (Port) PapyrusUtilsModule.createSemanticElement(owningClass,
				UMLElementTypes.PORT.getId());
		
		newPort.setAggregation(AggregationKind.COMPOSITE_LITERAL);
		
		ResourceSet resSet = owningClass.eResource().getResourceSet();
		Stereotype busStereo = (Stereotype) resSet.getEObject(OMSimulatorProfileUtil.OMSIMULATOR_BUS_URI, true);
		OMSimulatorBus bus =  (OMSimulatorBus) UMLUtil.safeApplyStereotype(newPort, busStereo);

		
		List<Port> semanticPorts = new ArrayList<>();
		List<EObject> portsViews = new ArrayList<>();
		GraphicalEditPart parent = null;
		
		for (Object selectedElement : portsToGroupEditParts) {
			if (selectedElement instanceof GraphicalEditPart) {		
				GraphicalEditPart editPart = (GraphicalEditPart) selectedElement;
			
				semanticPorts.add((Port) editPart.resolveSemanticElement());
				portsViews.add(editPart.getNotationView());
				
				if (parent == null) {
					parent = (GraphicalEditPart) editPart.getParent();
				}
			}

		}
		
		Node parentView = (Node) parent.getNotationView();

		Integer sumX = 0;
		Integer sumY = 0;
		
		
		for (EObject portView : portsViews) {
			Bounds constraint = (Bounds) ((Node)portView).getLayoutConstraint();
			sumX +=constraint.getX();
			sumY += constraint.getY();
			
		}
		
		
		double xMean = sumX/portsViews.size();
		double yMean = sumY/portsViews.size();
		Bounds parentConstraints = (Bounds) ((Node)parentView).getLayoutConstraint();
		
		
		RequestUtils.deleteObjectsWithRequest(portsViews);
		
		
		bus.getSignals().addAll(semanticPorts);
		
		return PapyrusUtilsModule.createRelativeView(
				newPort, 
				UMLDIElementTypes.PORT_SHAPE.getId(),
				parentView ,
				xMean/parentConstraints.getWidth(), 
				yMean/parentConstraints.getHeight(),
				null, null);
		
		
	}
	
	
	public static View connectBuses(View sourceBusView, View targetBusView) {
		return PapyrusUtilsModule.createSemanticElementAndLinkView(UMLDIElementTypes.CONNECTOR_EDGE.getId(), (Node)sourceBusView, 0.5, 0.5, (Node)targetBusView,	0.5, 0.5).getView();
		
	}
}
