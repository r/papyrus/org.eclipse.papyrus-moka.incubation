/*****************************************************************************
 * Copyright (c) 2016 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 *****************************************************************************/
package org.eclipse.papyrus.moka.fmi.ui.dnd;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.requests.DropObjectsRequest;
import org.eclipse.papyrus.infra.gmfdiag.dnd.strategy.TransactionalDropStrategy;
import org.eclipse.papyrus.moka.fmi.profile.util.FMIProfileUtil;
import org.eclipse.papyrus.moka.fmi.ui.Activator;
import org.eclipse.papyrus.moka.fmi.ui.commands.DropFMUAsPartCommand;
import org.eclipse.papyrus.moka.fmi.ui.commands.DropFMUPartCommand;
import org.eclipse.papyrus.moka.fmi.ui.commands.UpdateFMUTypeCommand;
import org.eclipse.papyrus.moka.fmi.util.FMIUtil;
import org.eclipse.swt.graphics.Image;
//import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.TypedElement;

public class FMUToPartDropStrategy extends TransactionalDropStrategy {

	public String getLabel() {
		return "FMU drag & drop support";
	}

	public String getDescription() {
		return "Drop FMU into a composite structure. When the dropped object is a FMU class, it creates a new part or updates an existing part. When the dropped object is a part, is is only displayed.";
	}

	public Image getImage() {
		return null;
	}

	public String getID() {
		return Activator.PLUGIN_ID + ".component2part";
	}

	@Override
	protected Command doGetCommand(Request request, final EditPart targetEditPart) {

		if (!(request instanceof DropObjectsRequest)) {
			return null;
		}
		DropObjectsRequest dropRequest = (DropObjectsRequest) request;
		EObject targetSemanticElement = getTargetSemanticElement(targetEditPart);

		if ((targetSemanticElement instanceof Class) && (dropRequest.getLocation() != null)
				&& (targetEditPart instanceof GraphicalEditPart)) {
			 GraphicalEditPart targetGraphicalEditPart = (GraphicalEditPart) targetEditPart;
			 Class targetSimulator = (Class) targetSemanticElement;

			CompoundCommand result = new CompoundCommand();
			List<EObject> sourceElements = getSourceEObjects(request);

			
			for (EObject sourceElement : sourceElements) {
				if (sourceElement instanceof Class && FMIProfileUtil.isFMU((Class) sourceElement)) {
					Class sourceType = (Class) sourceElement;
					Command resultCommand = new ICommandProxy(new DropFMUAsPartCommand(dropRequest,
							getTransactionalEditingDomain(targetGraphicalEditPart), targetSimulator, sourceType,
							targetGraphicalEditPart));

					result.add(resultCommand);
				}else if (sourceElement instanceof Property && ((Property) sourceElement).getType() instanceof Class && FMIProfileUtil.isFMU((Class) ((Property) sourceElement).getType())) {
					Command resultCommand = new ICommandProxy(new DropFMUPartCommand(dropRequest,
							getTransactionalEditingDomain(targetGraphicalEditPart), targetSimulator, (Property) sourceElement,
							targetGraphicalEditPart));

					result.add(resultCommand);
				}
			}
			
			if (result.isEmpty()) {
				return null;
			}else {
				return result;
			}
			
		}else if ((targetSemanticElement instanceof Property) 
				&& ((TypedElement) targetSemanticElement).getType() instanceof Class 
				&& FMIProfileUtil.isFMU((Class) ((TypedElement) targetSemanticElement).getType())
				&& (dropRequest.getLocation() != null)
				&& (targetEditPart instanceof GraphicalEditPart)) {
			
			GraphicalEditPart targetGraphicalEditPart = (GraphicalEditPart) targetEditPart;
			Property targetFMUPart= (Property) targetSemanticElement;
			
			List<EObject> sourceElements = getSourceEObjects(request);
			if (sourceElements.size() == 1) {
				EObject sourceElement = sourceElements.get(0);
				if (sourceElement != targetFMUPart.getType() && sourceElement instanceof Class && FMIProfileUtil.isFMU((Class) sourceElement) ) {
					return new ICommandProxy(new UpdateFMUTypeCommand( getTransactionalEditingDomain(targetGraphicalEditPart), targetFMUPart, (Class)sourceElement, targetGraphicalEditPart));
				}
				
				
			}
		
		}
		return null;
	}

	
	@Override
	public int getPriority() {
		// TODO Auto-generated method stub
		return 0;
	}

}
