/*****************************************************************************
 * 
 * Copyright (c) 2019 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST Initial API and implementation
 * 
 *****************************************************************************/
package org.eclipse.papyrus.moka.fmi.ui.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.papyrus.moka.fmi.ui.util.FMUViewUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Property;

public class UpdateFMUTypeCommand extends AbstractTransactionalCommand {

	



	protected Property fmuPartToUpdate;
	protected Class newType;
	protected GraphicalEditPart targetGraphicalEditPart;


	protected TransactionalEditingDomain domain;

	

	public UpdateFMUTypeCommand(  TransactionalEditingDomain domain,
			Property targetFMUPart, Class sourceElement, GraphicalEditPart targetEditPart) {
		super(domain, "Create FMU graphical view",
				getWorkspaceFiles(targetFMUPart));
		this.fmuPartToUpdate = targetFMUPart;
		this.newType = sourceElement;
		this.targetGraphicalEditPart = targetEditPart;
		
		this.domain = domain;
	}

	@Override
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		
		FMUViewUtil.updateFMUType(fmuPartToUpdate, newType, targetGraphicalEditPart);
		return CommandResult.newOKCommandResult();

	}


	





	
}
