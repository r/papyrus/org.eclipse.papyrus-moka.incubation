/*****************************************************************************
 * 
 * Copyright (c) 2019 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST Initial API and implementation
 * 
 *****************************************************************************/
package org.eclipse.papyrus.moka.fmi.ui.commands;

import java.util.Collections;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.requests.DropObjectsRequest;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.papyrus.infra.gmfdiag.common.helper.NotationHelper;
import org.eclipse.papyrus.moka.fmi.ui.util.FMUViewUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;

public class DropFMUPartCommand extends AbstractTransactionalCommand {

	



	protected Class targetSimulator;
	protected DropObjectsRequest request;
	protected Property newPart;
	protected GraphicalEditPart targetGraphicalEditPart;

	protected View targetView;

	protected TransactionalEditingDomain domain;

	

	public DropFMUPartCommand(DropObjectsRequest request, TransactionalEditingDomain domain,
			Class containerClass, Property sourceFMUPart, GraphicalEditPart targetEditPart) {
		super(domain, "Create FMU graphical view",
				getWorkspaceFiles(containerClass));
		this.targetSimulator = containerClass;
		this.newPart = sourceFMUPart;
		this.request = request;
		this.targetGraphicalEditPart = targetEditPart;
		
		this.domain = domain;
	}

	@Override
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {

		Point location = request.getLocation().getCopy();
		targetGraphicalEditPart.getContentPane().translateToRelative(location);
		FMUViewUtil.createGraphicalViews(newPart, targetGraphicalEditPart, NotationHelper.findView(targetGraphicalEditPart),location, null,  Collections.<Port>emptyList());
		request.setResult(newPart);
		return CommandResult.newOKCommandResult(newPart);

	}







	
}
