/*****************************************************************************
 * 
 * Copyright (c) 2019 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST Initial API and implementation
 * 
 *****************************************************************************/

package org.eclipse.papyrus.moka.fmi.ui.handlers;

import java.util.List;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.expressions.IEvaluationContext;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.papyrus.ease.module.PapyrusUtilsModule;
import org.eclipse.papyrus.infra.ui.command.AbstractCommandHandler;
import org.eclipse.papyrus.moka.fmi.ui.commands.ConnectPortViewsCommand;
import org.eclipse.papyrus.moka.fmi.ui.commands.GroupPortCommand;
import org.eclipse.papyrus.moka.fmi.ui.util.GroupPortUtils;

public class GroupPortsHanlder extends AbstractCommandHandler {

	protected GroupPortCommand groupPortCommand;
	protected boolean isExecuting = false;

	@Override
	protected Command getCommand(IEvaluationContext context) {

		List<EObject> selection = getSelectedElements();

		if (selection != null && selection.size() > 0) {

			if (!isExecuting && groupPortCommand == null && GroupPortUtils.canBeGrouped(selection)) {
				return new GroupPortCommand(getEditingDomain(context), (List<GraphicalEditPart>) getSelection());
			} else {
				if (isExecuting && groupPortCommand == null) {
					isExecuting = false;
					groupPortCommand = new GroupPortCommand(getEditingDomain(context),
							(List<GraphicalEditPart>) getSelection());
					return groupPortCommand;
				} else if (groupPortCommand != null) {
					Command result = new ConnectPortViewsCommand(getEditingDomain(context), groupPortCommand);
					groupPortCommand = null;
					return result;

				}
			}

		}

		return null;
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		isExecuting = true;
		// we execute twice to create the busses and then get the connection
		super.execute(event);
		PapyrusUtilsModule.refreshPapyrus();
		Object result = super.execute(event);
		isExecuting = false;
		return result;
	}

}
