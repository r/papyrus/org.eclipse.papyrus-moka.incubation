/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.moka.fmi.ui.commands;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.papyrus.ease.module.PapyrusUtilsModule;
import org.eclipse.papyrus.infra.emf.utils.EMFHelper;
import org.eclipse.papyrus.moka.fmi.ui.util.GroupPortUtils;

public class GroupPortCommand extends RecordingCommand{

	protected List<GraphicalEditPart> portsToGroupEditParts;
	private View targetPortView;
	private View sourcePortView;
	
	

	
	public GroupPortCommand(TransactionalEditingDomain domain, List<GraphicalEditPart> portsToGroupEditParts) {
		super(domain);
		this.portsToGroupEditParts = portsToGroupEditParts;
	}

	
	
	@Override
	protected void doExecute() {
		sourcePortView = GroupPortUtils.groupPorts(portsToGroupEditParts);
		List<GraphicalEditPart> connectedPortsEditParts = GroupPortUtils.getConnectedPortsEditPart(portsToGroupEditParts);
		List<EObject> connectedPortsSemanticElements = getSemanticElements(connectedPortsEditParts);
		if (GroupPortUtils.canBeGrouped(connectedPortsSemanticElements)) {
			targetPortView = GroupPortUtils.groupPorts(connectedPortsEditParts);
			PapyrusUtilsModule.refreshPapyrus();
		}
		
		
	}


	
	
	public List<EObject> getSemanticElements(List<GraphicalEditPart> editParts){
		List<EObject> eObjects = new ArrayList<EObject>();

		

		// Treat non-null selected object (try to adapt and return EObject)
		if (!editParts.isEmpty()) {

			// Parse current selection
			for (Object current : editParts) {
				
				EObject eObject = EMFHelper.getEObject(current);
				if (eObject != null) {
					// we avoid to add null element in the list!
					eObjects.add(eObject);
				}
			}
		}

		return eObjects;
	}



	public View getSourcePortView() {
		
		return sourcePortView;
	}



	public View getTargetPortView() {
		
		return targetPortView;
	}
	
}
