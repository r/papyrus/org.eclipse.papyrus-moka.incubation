/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.ease.fmi;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.papyrus.moka.ssp.omsimulatorprofile.util.OMSimulatorProfileUtil;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;

public class BusConnectorHandler extends AbstractConnectorHandler{

	List<FMIBusHandler> busses = new ArrayList<>();
	
	

	public BusConnectorHandler(FMISimulatorHandler parent, Connector connector) {
		super(parent, connector);
		
		for (ConnectorEnd end : connector.getEnds()) {
			Port endPort = (Port) end.getRole();
			Property endPart = end.getPartWithPort();
			if (endPort != null && endPart != null && OMSimulatorProfileUtil.isBus(endPort)) {
				FMUInstanceHandler instance = parent.getInstance(endPart.getName());
				if (instance != null) {
					FMIBusHandler busHandler = (FMIBusHandler) instance.getPortHandler(endPort);
					busses.add(busHandler);
				}
				
				
			}
			
		}
	}

	
	public List<FMIBusHandler> getBusses() {
		return busses;
	}
	
	@Override
	public FMIBusHandler getSourcePort() {
		return busses.get(0);
	}
	
	@Override
	public FMIBusHandler getTargetPort() {
		return busses.get(1);
	}
	
	
	public  List<IConnectorHandler> getSubConnections(){
		List<IConnectorHandler> subConnections = new ArrayList<>();
		
		FMIBusHandler sourceBus = getSourcePort();
		int busSize = sourceBus.getPorts().size();
		
		for (int i =0; i< busSize; i++){
			subConnections.add(new SubConnectorHandler(this, i));
		}
		return subConnections;
	}


	
	
	
}
