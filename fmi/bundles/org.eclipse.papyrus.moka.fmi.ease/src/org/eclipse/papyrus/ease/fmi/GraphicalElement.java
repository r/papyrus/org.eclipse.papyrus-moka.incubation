/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.ease.fmi;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;

public class GraphicalElement {

	protected GraphicalEditPart graphicalEditPart;
	protected EObject displayedElement;

	public GraphicalEditPart getDefaultEditPart() {
		if (graphicalEditPart == null) {
			graphicalEditPart = GraphicalElementUtils.getDefaultEditPart(displayedElement);
		}

	return graphicalEditPart;

	}

	public GraphicalElement(EObject displayedElement) {
		this.displayedElement = displayedElement;
	}
}
