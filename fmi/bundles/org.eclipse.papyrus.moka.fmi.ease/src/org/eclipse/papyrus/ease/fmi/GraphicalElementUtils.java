/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.ease.fmi;

import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.parts.IDiagramGraphicalViewer;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.papyrus.ease.module.PapyrusUtilsModule;
import org.eclipse.papyrus.editor.PapyrusMultiDiagramEditor;
import org.eclipse.papyrus.infra.gmfdiag.common.utils.DiagramEditPartsUtil;

public class GraphicalElementUtils {

	public static GraphicalEditPart getDefaultEditPart(EObject  displayedElement) {
		
			PapyrusMultiDiagramEditor papyrusEditor = PapyrusUtilsModule.getActivePapyrusEditor();
			if (papyrusEditor != null) {
				IDiagramGraphicalViewer papyrusViewer = (IDiagramGraphicalViewer) PapyrusUtilsModule
						.getActivePapyrusViewer(papyrusEditor);
				if (papyrusViewer != null) {
					List<View> views = DiagramEditPartsUtil.getEObjectViews(displayedElement);
					GraphicalEditPart firstEditPartFound = null;
					Iterator<View> viewIter = views.iterator();
					while (firstEditPartFound == null && viewIter.hasNext()) {
						View nextView = viewIter.next();
						if (!(nextView instanceof Diagram)) {
							firstEditPartFound = (GraphicalEditPart) DiagramEditPartsUtil.getEditPartFromView(nextView,
									papyrusViewer.getContents());
							if (firstEditPartFound != null) {
								while (firstEditPartFound.getParent() instanceof GraphicalEditPart && !(firstEditPartFound.getParent() instanceof DiagramEditPart) && ((GraphicalEditPart)firstEditPartFound.getParent()).getNotationView().getElement() == displayedElement) {
									firstEditPartFound = (GraphicalEditPart) firstEditPartFound.getParent();
								}
								return firstEditPartFound;
								
							}
							
						}

					}
				
				}

			}
			return null;
		}

	
	
	public static GraphicalEditPart getCurrentDiagramEditpart() {
		PapyrusMultiDiagramEditor papyrus = PapyrusUtilsModule.getActivePapyrusEditor();
		if (papyrus != null) {
			GraphicalViewer viewer = PapyrusUtilsModule.getActivePapyrusViewer(papyrus);
			
			if (viewer!= null) {
				return (GraphicalEditPart) viewer.getContents();
			}
		}
		return null;
	}

	
}
