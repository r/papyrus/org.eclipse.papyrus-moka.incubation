/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.ease.fmi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PrecisionRectangle;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.papyrus.infra.gmfdiag.common.helper.NotationHelper;
import org.eclipse.papyrus.moka.fmi.ui.util.FMUViewUtil;
import org.eclipse.papyrus.moka.ssp.omsimulatorprofile.util.OMSimulatorProfileUtil;
import org.eclipse.papyrus.uml.diagram.composite.custom.utils.CompositeEditPartUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;

public class FMUInstanceHandler extends GraphicalElement {

	protected FMISimulatorHandler parent;
	private Property prop;
	private Class type;
	protected FMUHandler fmuHandler;
	private Map<Port, AbstractPortHandler> portMap = new HashMap<>();

	private Node nodeView;

	private int x;
	private int y;
	private int width;
	private int height;

	public FMUInstanceHandler(FMISimulatorHandler parent, Property instance) {
		super(instance);
		this.parent = parent;
		prop = instance;
		type = (Class) prop.getType();

		fmuHandler = new FMUHandler(type);


	}
	
	
	public FMISimulatorHandler getParent() {
		return parent;
	}
	
	public Node getNodeView() {
		return nodeView;
	}

	public String getName() {
		return prop.getName();
	}

	public Property getUMLPart() {
		return prop;
	}

	public FMUHandler getFMUHandler() {
		return fmuHandler;
	}

	public String getFMUPath() {
		return fmuHandler.getFMUPath();
	}

	public AbstractPortHandler getPortHandler(Port port) {
		AbstractPortHandler handler = portMap.get(port);
		if (handler == null) {
			if (OMSimulatorProfileUtil.isBus(port)) {
				handler = new FMIBusHandler(this, port);
				portMap.put(port, handler);
			} else {
				handler = new FMIPortHandler(this, port);
				portMap.put(port, handler);
			}

		}
		return handler;

	}

	public AbstractPortHandler getPortOrBus(String portName) {
		Port umlPort = fmuHandler.getUMLPort(portName);
		return getPortHandler(umlPort);
	}

	public void updateType(FMUHandler fmuHandler) {
		if (fmuHandler.getFMUClass() != this.fmuHandler.getFMUClass()) {
			this.fmuHandler = fmuHandler;
			portMap.clear();
			FMUViewUtil.updateFMUType(prop, fmuHandler.getFMUClass(), getDefaultEditPart());
		}

	}

	public void createView(int x, int y) {
		createView(x, y, Collections.<Port>emptyList());
	}
	
	public void createView(int x, int y, List<Port> portsToHide) {
		GraphicalEditPart parentEditPart = parent.getDefaultEditPart();
		if (parentEditPart != null) {
			GraphicalEditPart compositeCompartmentEditPart = (GraphicalEditPart) CompositeEditPartUtil
					.getCompositeCompartmentEditPart(parentEditPart);
			if (compositeCompartmentEditPart != null) {
				nodeView = FMUViewUtil.createGraphicalViews(prop, compositeCompartmentEditPart, NotationHelper.findView(compositeCompartmentEditPart),new Point(x, y), null, portsToHide);
				if (nodeView != null) {
					PrecisionRectangle bounds = NotationHelper.getAbsoluteBounds(nodeView);

					this.x = bounds.x;
					this.y = bounds.y;
					this.height = bounds.height;
					this.width = bounds.width;

				}
			}

		} else {
			throw new RuntimeException(
					"Failed to find a graphical view for the containing simulator. You should create a diagram first");
		}

	}

	

	public int getX() {
		return x;
	}

	public void setX(int x) {
		throw new UnsupportedOperationException();
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		throw new UnsupportedOperationException();
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		throw new UnsupportedOperationException();
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		throw new UnsupportedOperationException();
	}
	
	public List<FMIBusHandler> getBusses(){
		List<FMIBusHandler> result = new ArrayList<FMIBusHandler>();
		
		for (String busName : fmuHandler.getBusses()) {
			result.add((FMIBusHandler) getPortOrBus(busName));
		}
		
		return result;
	}

}
