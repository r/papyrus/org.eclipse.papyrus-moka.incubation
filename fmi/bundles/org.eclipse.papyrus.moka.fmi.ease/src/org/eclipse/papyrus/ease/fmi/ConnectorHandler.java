/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.ease.fmi;

import org.eclipse.papyrus.sysml14.portsandflows.FlowDirection;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;

public class ConnectorHandler extends AbstractConnectorHandler{


	private FMIPortHandler sourcePort;
	private FMIPortHandler targetPort;
	

	public ConnectorHandler(FMISimulatorHandler parent, Connector connector) {
		super(parent, connector);
		
		for (ConnectorEnd end : connector.getEnds()) {
			Port endPort = (Port) end.getRole();
			Property endPart = end.getPartWithPort();
			if (endPort != null && endPart != null) {
				
				FMUInstanceHandler instance = parent.getInstance(endPart.getName());
				FMIPortHandler portHandler = (FMIPortHandler) instance.getPortHandler(endPort);
				if (portHandler.getDirection() == FlowDirection.OUT) {
					sourcePort = portHandler;
				}else {
					targetPort = portHandler;
				}
			}
			
		}
	}
	
	@Override
	public FMIPortHandler getSourcePort() {
		return sourcePort;
	}
	
	@Override
	public FMIPortHandler getTargetPort() {
		return targetPort;
	}
	
	
}
