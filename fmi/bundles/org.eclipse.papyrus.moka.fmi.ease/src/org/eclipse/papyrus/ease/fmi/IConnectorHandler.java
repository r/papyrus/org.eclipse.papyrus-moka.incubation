/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.ease.fmi;

import java.util.List;

public interface IConnectorHandler {

	
	public AbstractPortHandler getSourcePort();
	
	public AbstractPortHandler getTargetPort();
	
	public FMISimulatorHandler getParent();
	
	public List<FMUInstanceHandler> getConnectedFMUs();
}
