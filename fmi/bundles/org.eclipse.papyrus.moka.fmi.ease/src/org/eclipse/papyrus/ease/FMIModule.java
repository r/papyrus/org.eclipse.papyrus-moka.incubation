/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.ease;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.NotEnabledException;
import org.eclipse.core.commands.NotHandledException;
import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.ease.modules.AbstractScriptModule;
import org.eclipse.ease.modules.WrapToScript;
import org.eclipse.ease.modules.platform.PlatformModule;
import org.eclipse.ease.tools.ResourceTools;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.papyrus.ease.fmi.FMISimulatorHandler;
import org.eclipse.papyrus.ease.fmi.FMUHandler;
import org.eclipse.papyrus.ease.module.PapyrusUtilsModule;
import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.papyrus.moka.fmi.fmi2uml.FMI2UML;
import org.eclipse.papyrus.moka.fmi.fmiprofile.CS_FMU;
import org.eclipse.papyrus.moka.fmi.fmu.FMUResource;
import org.eclipse.papyrus.moka.fmi.fmumetamodel.FMUBundle;
import org.eclipse.papyrus.moka.fmi.modeldescription.DocumentRoot;
import org.eclipse.papyrus.moka.fmi.modeldescription.FmiFactory;
import org.eclipse.papyrus.moka.fmi.modeldescription.FmiModelDescriptionType;
import org.eclipse.papyrus.moka.fmi.modeldescription.util.FmiResourceFactoryImpl;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.util.UMLUtil;

public class FMIModule extends AbstractScriptModule {

	private static final String ELK_ROUTING_COMMAND_ID = "org.eclipse.elk.core.ui.command.layout";

	@WrapToScript
	public FMUHandler importFMU(Package targetPackage, String fmuPath) {

		Object resolved = ResourceTools.resolve(fmuPath);
		URI sourceURI = null;
		if (resolved instanceof IFile && "fmu".equals(((IFile) resolved).getFileExtension())) {
			sourceURI = URI.createPlatformResourceURI(((IFile) resolved).getFullPath().toPortableString(), true);
		} else if (resolved instanceof File) {
			sourceURI = URI.createFileURI(((File) resolved).getAbsolutePath());
		}
		if (sourceURI != null) {
			ResourceSet resSet = null;
			if (targetPackage.eResource() != null) {
				if (targetPackage.eResource() != null && targetPackage.eResource().getResourceSet() != null) {
					resSet = targetPackage.eResource().getResourceSet();
				} else {
					resSet = new ResourceSetImpl();
				}
			}

			Resource inputResource = resSet.getResource(sourceURI, true);
			if (inputResource instanceof FMUResource && !inputResource.getContents().isEmpty()) {
				EObject rootResource = inputResource.getContents().get(0);
				if (rootResource instanceof FMUBundle) {
					Class fmuClass = FMI2UML.getFMUClass((FMUBundle) rootResource, targetPackage);
					return new FMUHandler(fmuClass);
				}
			}
		}

		return null;

	}

	@WrapToScript
	public void generateModelDescription(Class fmuClass) {

		FmiModelDescriptionType desc = ModelDescriptionGenerator.getModelDescription(fmuClass, fmuClass.getName());
		if (desc != null) {

			Display.getDefault().syncExec(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					DirectoryDialog fd = new DirectoryDialog(PlatformUI.getWorkbench().getDisplay().getActiveShell(),
							SWT.SAVE);
					fd.setText("Save model description");

					String selected = fd.open();
					if (selected != null) {
						FmiResourceFactoryImpl resFact = new FmiResourceFactoryImpl();
						URI uri = URI.createFileURI(selected);
						uri = uri.appendSegment(fmuClass.getName()).appendSegment("modelDescription.xml");

						Resource outputRes = resFact.createResource(uri);
						// Resource outputRes = resSet.createResource(URI.createFileURI(selected));
						DocumentRoot root = FmiFactory.eINSTANCE.createDocumentRoot();
						root.setFmiModelDescription(desc);
						outputRes.getContents().add(root);
						Map<Object, Object> options = new HashMap<>();
						options.put(XMLResource.OPTION_ENCODING, "utf-8");
						try {
							outputRes.save(options);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

				}

			});

		}
	}

	@WrapToScript
	public FMISimulatorHandler getSimulatorHandler(Class simulatorClass) {
		EcoreUtil.resolveAll(simulatorClass);
		return new FMISimulatorHandler(simulatorClass);
	}


	@WrapToScript
	public FMISimulatorHandler getSimulatorHandler(String qualifiedName)
			throws RuntimeException, CoreException, URISyntaxException, IOException, ServiceException {
		
		Package rootPackage = PapyrusUtilsModule.getActivePapyrusModel();
		if (rootPackage != null) {
			EObject namedElement = PapyrusUtilsModule.getPapyrusNamedElement(rootPackage, qualifiedName);
			if (namedElement instanceof Class) {
				return getSimulatorHandler((Class) namedElement);
			}
		}
		
		return null;
	}

	

	@WrapToScript
	public HashMap<String, FMUHandler> getFMUHandlers() {
		HashMap<String, FMUHandler> result = new HashMap<>();
		Package  rootPackage = PapyrusUtilsModule.getActivePapyrusModel();
		if (rootPackage != null) {
			for (Iterator<EObject> iterator = rootPackage.eAllContents(); iterator.hasNext();) {
				EObject obj = (EObject) iterator.next();
				if (obj instanceof Class && UMLUtil.getStereotypeApplication((Class) obj, CS_FMU.class) != null) {
					FMUHandler handler = new FMUHandler(obj);
					result.put(handler.getQualifiedName(), handler);

				}
			}
		} else {
			throw (new RuntimeException("Root package not found. Papyrus should be opened and have focus."));
		}
		return result;
	}

	@WrapToScript
	public void createInstanceView(int X, int Y) {

	}

	@WrapToScript
	public void autoRoute() throws ExecutionException, NotDefinedException, NotEnabledException, NotHandledException {
		
		PlatformModule.executeCommand(ELK_ROUTING_COMMAND_ID, Collections.<String, String>emptyMap());

	}

}
