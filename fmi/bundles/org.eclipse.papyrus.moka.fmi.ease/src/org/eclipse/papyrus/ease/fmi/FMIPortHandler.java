/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.ease.fmi;

import org.eclipse.papyrus.sysml14.deprecatedelements.FlowPort;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.util.UMLUtil;

public class FMIPortHandler extends AbstractPortHandler{

	private FlowPort fmiPort;

	public FMIPortHandler(FMUInstanceHandler instance, Port port) {
		
		super(instance, port);
		fmiPort = UMLUtil.getStereotypeApplication(port, FlowPort.class);
		
	}
	
	
	
	public org.eclipse.papyrus.sysml14.portsandflows.FlowDirection getDirection() {
		return fmiPort.getDirection();
	}
	
	
	
}
