/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.ease.fmi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateConnectionViewRequest;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.IHintedType;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.papyrus.infra.gmfdiag.common.commands.CommonDeferredCreateConnectionViewCommand;
import org.eclipse.papyrus.infra.gmfdiag.common.utils.ServiceUtilsForEditPart;
import org.eclipse.papyrus.uml.diagram.composite.providers.UMLElementTypes;
import org.eclipse.uml2.uml.ConnectableElement;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Port;

public abstract class AbstractConnectorHandler extends GraphicalElement implements IConnectorHandler {
	protected Connector connector;
	protected FMISimulatorHandler parent;
	protected List<FMUInstanceHandler> connectedFMUs = new ArrayList<>();

	protected Map<ConnectorEnd, AbstractPortHandler> portMap = new HashMap<>();

	public AbstractConnectorHandler(FMISimulatorHandler parent, Connector connector) {
		super(connector);
		this.parent = parent;
		this.connector = connector;

		for (ConnectorEnd end : connector.getEnds()) {
			ConnectableElement partWithPort = end.getPartWithPort();
			if (partWithPort != null) {
				FMUInstanceHandler instance = parent.getInstance(partWithPort.getName());
				if (instance != null) {
					connectedFMUs.add(instance);
					portMap.put(end, instance.getPortHandler((Port) end.getRole()));
				}

			}
		}

	}

	public Connector getConnector() {
		return connector;
	}

	public FMISimulatorHandler getParent() {
		return parent;
	}

	public View createView() {
		if (parent.getDefaultEditPart() == null) {
			throw (new RuntimeException("Failed to find an view for the simulator, you should first create a diagram"));
		}

		List<GraphicalEditPart> portEditParts = new ArrayList<>();
		for (AbstractPortHandler handler : portMap.values()) {
			GraphicalEditPart portEditPart = handler.getGraphicalEditPart();
			if (portEditPart == null) {
				throw (new RuntimeException(
						"The view of the port " + handler.getFullName() + " should be created first"));
			}
			portEditParts.add(handler.getGraphicalEditPart());
		}

		CreateConnectionViewRequest.ConnectionViewDescriptor linkdescriptor = new CreateConnectionViewRequest.ConnectionViewDescriptor(
				UMLElementTypes.Connector_Edge, ((IHintedType) UMLElementTypes.Connector_Edge).getSemanticHint(),
				parent.getDefaultEditPart().getDiagramPreferencesHint());
//		ConnectionViewDescriptor descriptor = new ConnectionViewDescriptor(new SemanticHintElementAdapter(connector,  UMLElementTypes.Connector_Edge) ,
//				 UMLElementTypes.Connector_Edge.getId(),
//				parent.getDefaultEditPart().getDiagramPreferencesHint());
//		CreateConnectionViewRequest createRequest = new CreateConnectionViewRequest(descriptor);
//		Command createConnectionCmd = CreateConnectionViewRequest.getCreateCommand(createRequest, port1.getGraphicalEditPart(),
//				port2.getGraphicalEditPart());
		TransactionalEditingDomain domain;
		try {
			domain = ServiceUtilsForEditPart.getInstance().getTransactionalEditingDomain(portEditParts.get(0));
			CommonDeferredCreateConnectionViewCommand aLinkCommand = new CommonDeferredCreateConnectionViewCommand(
					domain, ((IHintedType) UMLElementTypes.Connector_Edge).getSemanticHint(),
					new SimpleViewAdapter(portEditParts.get(0).getNotationView()),
					new SimpleViewAdapter(portEditParts.get(1).getNotationView()), portEditParts.get(0).getViewer(),
					portEditParts.get(0).getDiagramPreferencesHint(), linkdescriptor, null);
			aLinkCommand.setElement(connector);
			if (aLinkCommand.canExecute()) {
				aLinkCommand.execute(null, null);
				return (View) (((IAdaptable) aLinkCommand.getCommandResult().getReturnValue()).getAdapter(View.class));
			}

		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (org.eclipse.core.commands.ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;

	}

	protected static final class SemanticHintElementAdapter extends EObjectAdapter {
		private IElementType type;

		/**
		 * constructor
		 * 
		 * @param element
		 * @param hint
		 */
		public SemanticHintElementAdapter(EObject element, IElementType type) {
			super(element);
			this.type = type;
		}

		public Object getAdapter(Class adapter) {
			if (adapter.equals(IElementType.class)) {
				return type;
			}
			return super.getAdapter(adapter);
		}
	}

	protected static final class SimpleViewAdapter implements IAdaptable {
		private View view;

		public SimpleViewAdapter(View element) {

			this.view = element;
		}

		public Object getAdapter(Class adapter) {
			if (adapter.isAssignableFrom(View.class)) {
				return view;
			}
			return null;
		}
	}

	public List<FMUInstanceHandler> getConnectedFMUs() {
		return connectedFMUs;
	}

}
