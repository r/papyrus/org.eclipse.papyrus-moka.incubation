/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.ease.fmi;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.papyrus.moka.fmi.fmiprofile.CS_FMU;
import org.eclipse.papyrus.moka.fmi.fmiprofile.FMIPort;
import org.eclipse.papyrus.moka.ssp.omsimulatorprofile.OMSimulatorBus;
import org.eclipse.papyrus.moka.ssp.omsimulatorprofile.util.OMSimulatorProfileUtil;
import org.eclipse.uml2.uml.AggregationKind;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.util.UMLUtil;

public class FMUHandler {

	Class fmuClass;
	CS_FMU csFMU;

	protected HashMap<String, Port> portMap = new HashMap<String, Port>();

	protected HashMap<String, Port> busMap = new HashMap<String, Port>();

	public FMUHandler(EObject fmuObject) {
		if (fmuObject instanceof CS_FMU) {
			csFMU = (CS_FMU) fmuObject;
			fmuClass = csFMU.getBase_Class();
		} else if (fmuObject instanceof Class) {
			fmuClass = (Class) fmuObject;
			csFMU = UMLUtil.getStereotypeApplication(fmuClass, CS_FMU.class);
		}

		if (fmuClass != null) {
			for (Property prop : fmuClass.getAllAttributes()) {
				if (prop instanceof Port) {
					if (UMLUtil.getStereotypeApplication(prop, FMIPort.class) != null) {
						portMap.put(prop.getName(), (Port) prop);
					} else if (UMLUtil.getStereotypeApplication(prop, OMSimulatorBus.class) != null) {
						busMap.put(prop.getName(), (Port) prop);
					}

				}
			}
		}
	}

	public Class getFMUClass() {
		return fmuClass;
	}

	public String getName() {
		return fmuClass.getName();
	}

	public String getQualifiedName() {
		return fmuClass.getQualifiedName();
	}

	public CS_FMU getCosimFMU() {
		return csFMU;
	}

	public Port createBus(String busName, List<String> portNames) {
		List<Port> ports = new ArrayList<>();

		for (String portName : portNames) {
			Port port = portMap.get(portName);
			if (port != null) {
				ports.add(port);
			} else {
				throw (new RuntimeException(
						"Failed to find a port named " + portName + " in FMU " + fmuClass.getQualifiedName()));
			}
		}
		Port busPort = (Port) fmuClass.createOwnedAttribute(busName, null, UMLPackage.eINSTANCE.getPort());
		ResourceSet resSet = UMLUtil.getResourceSet(fmuClass);
		Stereotype busStereo = (Stereotype) resSet.getEObject(OMSimulatorProfileUtil.OMSIMULATOR_BUS_URI, true);

		OMSimulatorBus bus = (OMSimulatorBus) UMLUtil.safeApplyStereotype(busPort, busStereo);
		bus.getSignals().addAll(ports);

		busMap.put(busPort.getName(), busPort);
		busPort.setAggregation(AggregationKind.COMPOSITE_LITERAL);
		busPort.setIsBehavior(true);
		return busPort;

	}

	public String getFMUPath() {

		try {
			return FileLocator.toFileURL(new URL(csFMU.getFmuBundle().eResource().getURI().toString())).getFile();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;

	}

	
	public List<Port> getAllPorts(){
		List<Port> result = new ArrayList<Port>();
		for (Property prop : fmuClass.getAllAttributes()) {
			if (prop instanceof Port) {
				result.add((Port) prop);
			}
		}
		return result;
	}
	

	public Port getUMLPort(String portName) {

		for (Property property : fmuClass.getAllAttributes()) {
			if (property instanceof Port && portName.equals(property.getName())) {
				return (Port) property;
			}
		}

		throw new RuntimeException(
				"Failed to find port with name " + portName + " in fmu " + fmuClass.getQualifiedName());

	}
	
	
	public List<String> getBusses(){
		List<String> result = new ArrayList<>();
		
		for (String busName: busMap.keySet()) {
			result.add(busName);
		}
		
		return result;
			
	}
}
