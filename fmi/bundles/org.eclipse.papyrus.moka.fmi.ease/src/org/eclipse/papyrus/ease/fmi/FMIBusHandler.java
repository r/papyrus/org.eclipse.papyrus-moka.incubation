/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.ease.fmi;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.papyrus.moka.ssp.omsimulatorprofile.OMSimulatorBus;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.util.UMLUtil;



public class FMIBusHandler extends AbstractPortHandler {

	protected OMSimulatorBus bus;
	
	public FMIBusHandler(FMUInstanceHandler instance, Port port) {	
		super(instance, port);
		bus = UMLUtil.getStereotypeApplication(port, OMSimulatorBus.class);
	}
	
	
	public List<FMIPortHandler> getPorts() {
		List<FMIPortHandler> result = new ArrayList<>();
		
		for (Port port : bus.getSignals()) {
			result.add((FMIPortHandler) parent.getPortHandler(port));
		}
		
		return result;
	}

}
