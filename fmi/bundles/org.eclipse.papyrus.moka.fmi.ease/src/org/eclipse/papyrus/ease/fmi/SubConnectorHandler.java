/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.ease.fmi;

import java.util.List;

import org.eclipse.papyrus.sysml14.portsandflows.FlowDirection;

public class SubConnectorHandler implements IConnectorHandler {

	
	private BusConnectorHandler containingBusConnector;
	private int index;

	public  SubConnectorHandler(BusConnectorHandler containingBus, int index) {
		this.containingBusConnector = containingBus;
		this.index = index;
		
		
	}
	
	@Override
	public AbstractPortHandler getSourcePort() {
		 FMIBusHandler sourceBus = containingBusConnector.getSourcePort();
		 FMIBusHandler targetBus = containingBusConnector.getTargetPort();
		 
		 FMIPortHandler portInSourceBus = sourceBus.getPorts().get(index);
		 FMIPortHandler portInTargetBus = targetBus.getPorts().get(index);
		 
		 if (portInSourceBus.getDirection() == FlowDirection.OUT) {
			 return portInSourceBus;
		 }else {
			return  portInTargetBus;
		 }
		 
	
	}

	@Override
	public AbstractPortHandler getTargetPort() {
		 FMIBusHandler sourceBus = containingBusConnector.getSourcePort();
		 FMIBusHandler targetBus = containingBusConnector.getTargetPort();
		 
		 FMIPortHandler portInSourceBus = sourceBus.getPorts().get(index);
		 FMIPortHandler portInTargetBus = targetBus.getPorts().get(index);
		 
		 if (portInSourceBus.getDirection() == FlowDirection.IN) {
			 return portInSourceBus;
		 }else {
			return  portInTargetBus;
		 }
	}

	@Override
	public FMISimulatorHandler getParent() {
		
		return containingBusConnector.getParent();
	}

	@Override
	public List<FMUInstanceHandler> getConnectedFMUs() {
		
		return containingBusConnector.getConnectedFMUs();
	}

}
