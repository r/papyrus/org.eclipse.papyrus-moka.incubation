/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.ease.fmi;

import java.util.Map;

import org.eclipse.gmf.runtime.notation.Node;

import org.eclipse.papyrus.ease.module.PapyrusUtilsModule;
import org.eclipse.papyrus.moka.fmi.ui.util.FMUViewUtil;
import org.eclipse.papyrus.uml.diagram.composite.edit.parts.PortEditPart;
import org.eclipse.papyrus.uml.diagram.composite.providers.UMLElementTypes;
import org.eclipse.uml2.uml.ConnectableElement;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;

public class AbstractPortHandler {
	protected Property part;
	protected Port umlPort;

	protected PortEditPart editPart = null;
	protected FMUInstanceHandler parent;

	private Node nodeView = null;

	public AbstractPortHandler(FMUInstanceHandler instance, Port port) {
		parent = instance;

		if (instance != null) {
			this.part = instance.getUMLPart();
		}
		
		umlPort = port;

	}

	public String getName() {
		return umlPort.getName();

	}

	public Node getNodeView() {
		return nodeView;
	}

	public String getFullName() {
		return part.getName() + ":" + umlPort.getName();
	}

	public Property getUMLPart() {
		return part;
	}

	public ConnectableElement getUMLPort() {
		return umlPort;
	}

	public void setGraphicalEditPart(PortEditPart editPart) {
		this.editPart = editPart;
	}

	public PortEditPart getGraphicalEditPart() {
		if (editPart != null) {
			return editPart;
		} else if (parent.getDefaultEditPart() != null) {
			Map<Port, PortEditPart> editPartMap = FMUViewUtil.getPortEditPartsMap(parent.getDefaultEditPart());
			editPart = editPartMap.get(umlPort);
		}
		return editPart;
	}

	public Node createView(double x, double y) {
		Node parentNode = parent.getNodeView();
		if (parentNode == null) {
			parent.createView(50, 50);
			parentNode = parent.getNodeView();

		}
		nodeView = (Node) PapyrusUtilsModule.createRelativeView(umlPort, UMLElementTypes.Port_Shape.getId(), parentNode,
				x, y, FMUViewUtil.PORT_DEFAULT_HEIGHT, FMUViewUtil.PORT_DEFAULT_HEIGHT);
		return nodeView;

	}
	
	
	public FMUInstanceHandler getParent() {
		return parent;
	}

}
