/**
 * Copyright (c) 2018 CEA LIST.
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 *   CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.moka.ssp.omsimulatorprofile.util;

import org.eclipse.emf.common.util.URI;
import org.eclipse.papyrus.moka.ssp.omsimulatorprofile.OMSimulatorBus;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.util.UMLUtil;

public class OMSimulatorProfileUtil {

	public static URI OMSIMULATOR_PROFILE_UML_URI = URI.createURI("pathmap://PAPYRUS_SSP_PROFILE/OMSimulator.profile.uml");
	
	public static URI OMSIMULATOR_BUS_URI = OMSIMULATOR_PROFILE_UML_URI.appendFragment("_DwNAgMVaEeiLVbFVoMGQrQ");
	
	public static URI OMSIMULATOR_BUS_CONNECTOR_URI = OMSIMULATOR_PROFILE_UML_URI.appendFragment("_XeSSkCYvEemDsJKy8GtZKA");

	public static boolean isBus(Port port) {
		
		return UMLUtil.getStereotypeApplication(port, OMSimulatorBus.class) != null;
	}
	
	
}
