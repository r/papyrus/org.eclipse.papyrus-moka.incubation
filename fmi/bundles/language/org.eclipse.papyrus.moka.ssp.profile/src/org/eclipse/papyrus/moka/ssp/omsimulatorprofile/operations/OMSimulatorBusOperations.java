/**
 * Copyright (c) 2019 CEA LIST.
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0 
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 *   CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.moka.ssp.omsimulatorprofile.operations;

import java.util.List;

import org.eclipse.papyrus.moka.fmi.fmiprofile.FMIPort;
import org.eclipse.papyrus.moka.ssp.omsimulatorprofile.OMSimulatorBus;

import org.eclipse.papyrus.sysml14.portsandflows.FlowDirection;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * <!-- begin-user-doc -->
 * A static utility class that provides operations related to '<em><b>Bus</b></em>' model objects.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following operations are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.moka.ssp.omsimulatorprofile.OMSimulatorBus#getDirection() <em>Get Direction</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OMSimulatorBusOperations {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright (c) 2018 CEA LIST.\n\n All rights reserved. This program and the accompanying materials\n are made available under the terms of the Eclipse Public License 2.0\n which accompanies this distribution, and is available at\n https://www.eclipse.org/legal/epl-2.0 \r\n\r\nSPDX-License-Identifier: EPL-2.0\n\n Contributors:\n  CEA LIST - Initial API and implementation";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OMSimulatorBusOperations() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public static  FlowDirection getDirection(OMSimulatorBus omSimulatorBus) {
		List<Port> ports = omSimulatorBus.getSignals();
		if (ports.isEmpty()) {
			return FlowDirection.INOUT;
		}
		boolean allInputs = true;
		boolean allOutputs = true;
		for (Port port: ports) {
			FMIPort fmiPort = UMLUtil.getStereotypeApplication(port, FMIPort.class);
			if (fmiPort!=null) {
				FlowDirection direction = fmiPort.getDirection();
				allInputs &= direction== FlowDirection.IN;
				allOutputs &= direction == FlowDirection.OUT;
			}
		}
		
		if (allInputs) {
			return FlowDirection.IN;
		}else if (allOutputs) {
			return FlowDirection.OUT;
		}else {
			return FlowDirection.INOUT;
		}
		
		
	}

} // OMSimulatorBusOperations