/*****************************************************************************
 * Copyright (c) 2019 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.moka.ssp.omsimulatorprofile.edit;

import java.util.Collections;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.IEditCommandRequest;
import org.eclipse.papyrus.moka.ssp.omsimulatorprofile.OMSimulatorBus;
import org.eclipse.papyrus.moka.ssp.omsimulatorprofile.util.OMSimulatorProfileUtil;
import org.eclipse.uml2.uml.ConnectableElement;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.util.UMLUtil;

public class OMSimulatorBusEditHelper extends AbstractEditHelperAdvice{
	
@Override
protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
	EObject createdElement = request.getElementToConfigure();
	if (createdElement instanceof Connector) {
	
		Connector connector = (Connector) createdElement;
		ConnectableElement sourceRole = getSourceRole(request);
		ConnectableElement targetRole = getTargetRole(request);
		
		if ( sourceRole instanceof Port && targetRole instanceof Port) {
			Port port1 = (Port) sourceRole;
			Port port2 = (Port)targetRole;
			if (UMLUtil.getStereotypeApplication(port1, OMSimulatorBus.class ) != null && UMLUtil.getStereotypeApplication(port2, OMSimulatorBus.class) != null) {
				return new AbstractTransactionalCommand(request.getEditingDomain(), "applyOMSimulatorBusStereo ",  Collections.emptyList()) {
					
					@Override
					protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
						UMLUtil.safeApplyStereotype(connector, (Stereotype) UMLUtil.getResourceSet(connector).getEObject(OMSimulatorProfileUtil.OMSIMULATOR_BUS_CONNECTOR_URI, true));
						return CommandResult.newOKCommandResult();
					}
				};
			}
		}
	}
	return null;
}
	



/**
 * This method provides the source role provided as {@link ConfigureRequest} parameter.
 * 
 * @return the target role
 */
private ConnectableElement getSourceRole(IEditCommandRequest req) {
	ConnectableElement result = null;
	Object paramObject = req.getParameter(CreateRelationshipRequest.SOURCE);
	if(paramObject instanceof ConnectableElement) {
		result = (ConnectableElement)paramObject;
	}

	return result;
}

/**
 * This method provides the target role provided as {@link ConfigureRequest} parameter.
 * 
 * @return the target role
 */
private ConnectableElement getTargetRole(IEditCommandRequest req) {
	ConnectableElement result = null;
	Object paramObject = req.getParameter(CreateRelationshipRequest.TARGET);
	if(paramObject instanceof ConnectableElement) {
		result = (ConnectableElement)paramObject;
	}

	return result;
}
	
}
