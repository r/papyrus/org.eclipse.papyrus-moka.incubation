/*****************************************************************************
 * 
 * Copyright (c) 2017 CEA LIST. 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST Initial API and implementation
 * 
 *****************************************************************************/
package org.eclipse.papyrus.moka.externalcontrol.engine;

import java.util.List;

import org.eclipse.papyrus.moka.engine.uml.time.UMLTimedExecutionEngine;
import org.eclipse.papyrus.moka.externalcontrol.advice.IControllerAdviceFactory;
import org.eclipse.papyrus.moka.externalcontrol.control.queue.ExternallyControlledExecutionLoop;
import org.eclipse.papyrus.moka.externalcontrol.controller.ExternalController;
import org.eclipse.papyrus.moka.externalcontrol.controller.IControlledVisitorPushPullStrategy;
import org.eclipse.papyrus.moka.externalcontrol.controller.NodeEdgeAlternatePushPullStrategy;
import org.eclipse.papyrus.moka.externalcontrol.semantics.ExternalControlExecutionFactory;
import org.eclipse.papyrus.moka.externalcontrol.semantics.ExternalControlExecutor;
import org.eclipse.papyrus.moka.fuml.loci.ExecutionFactory;
import org.eclipse.papyrus.moka.fuml.loci.IExecutionFactory;
import org.eclipse.papyrus.moka.fuml.loci.IExecutor;
import org.eclipse.papyrus.moka.fuml.loci.ILocus;
import org.eclipse.papyrus.moka.kernel.scheduling.control.ExecutionController;
import org.eclipse.papyrus.moka.kernel.scheduling.control.IExecutionController;
import org.eclipse.papyrus.moka.pssm.loci.SM_Locus;

/**
 * Clients should extend this class to provide an specialized Moka execution
 * engine, relying on a default semantic ExecutionFactory, customized thanks to
 * Advices provided by the AdviceFactories.
 * 
 * @author sr246418
 *
 */

public abstract class AbstractExternalControlExecutionEngine extends UMLTimedExecutionEngine {

	/**
	 * The ExternalController instance
	 */
	protected ExternalController externalController;

	public AbstractExternalControlExecutionEngine() {
		super();
	}

	/**
	 * Clients should implement this method to provide Advice Factories. The
	 * factories will return advices that will influence the default execution of
	 * semantic visitors provided by the delegated execution factory.
	 * 
	 * @return a list of Controller Advice factories. It should not be null, but can
	 *         be empty
	 */
	protected abstract List<IControllerAdviceFactory> getAdviceFactories();

	/**
	 * Clients should implement this method to return the default IExecutionFactory,
	 * which will instantiate the semantic visitors for each executable model
	 * element.
	 * 
	 * @return the default IExecutionFactory on top of which this customization is
	 *         implemented
	 */
	protected abstract IExecutionFactory getDelegatedExecutionFactory();

	/**
	 * Clients can override this method to provide another ILocus type. By default a
	 * {@link org.eclipse.papyrus.moka.fuml.statemachines.Semantics.Loci.SM_Locus}
	 * is used.
	 * 
	 * @return the locus that will be used for this semantic customization. Should
	 *         not be null.
	 */
	protected ILocus getLocus() {
		return new SM_Locus();
	}

	/**
	 * Clients can override this method to provide another IExecutor type. By
	 * default a
	 * {@link org.eclipse.papyrus.moka.composites.Semantics.impl.Loci.LociL3.CS_Executor}
	 * is used.
	 * 
	 * @return the IExecutor that will be used for this semantic customization.
	 *         Should not be null.
	 */
	protected IExecutor getExecutor() {
		return new ExternalControlExecutor();
	}

	/**
	 * Clients can override this method to provide another
	 * IControlledVisitorPushPullStrategy to the ExternalController. By default a
	 * {@link org.eclipse.papyrus.moka.externalcontrol.controller.NodeEdgeAlternatePushPullStrategy}
	 * is returned.
	 * 
	 * @return the IControlledVisitorPushPullStrategy that will be used by the
	 *         External Control executor. Should not be null.
	 */
	protected IControlledVisitorPushPullStrategy getControllerPushPullStrategy() {
		return new NodeEdgeAlternatePushPullStrategy();
	}

	@Override
	public ILocus createLocus() {
		ILocus locus = getLocus();
		locus.setExecutor(getExecutor());
		IExecutionFactory delegatedFactory = getDelegatedExecutionFactory();
		
		
		ExecutionFactory externalControlExecutionFactory = new ExternalControlExecutionFactory(delegatedFactory,
				externalController, getAdviceFactories());
		locus.setFactory(externalControlExecutionFactory);
		return locus;
	} 

	@Override
	public IExecutionController createController() {
		this.externalController = new ExternalController(getControllerPushPullStrategy());
		IExecutionController controller = new ExecutionController();
		controller.setExecutionLoop(new ExternallyControlledExecutionLoop(this.externalController));
		return controller;
	}

	// @Override
	// public void start(SubMonitor monitor) throws ExecutionEngineException {
	// super.start(monitor);
	// }

	// @Override
	// protected void run_() {
	// // Starts externally controlled execution loop
	// RootExecution rootExecution = new RootExecution((Behavior) this.executionEntryPoint, this.executionArguments, locus);
	// ExecutionController.getInstance().setExecutionLoop(new ExternallyControlledExecutionLoop(this.externalController));
	// ExecutionController.getInstance().start(rootExecution);
	// }

}
