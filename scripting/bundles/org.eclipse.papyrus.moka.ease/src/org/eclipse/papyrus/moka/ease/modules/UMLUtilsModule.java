/*****************************************************************************
 * Copyright (c) 2020 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.ease.modules;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.ease.modules.AbstractScriptModule;
import org.eclipse.ease.modules.WrapToScript;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.papyrus.uml.tools.utils.UMLUtil;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.ProfileApplication;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.TypedElement;

public class UMLUtilsModule extends AbstractScriptModule {
	public static final String MODULE_ID = "/Modeling/UMLUtils";
	
	

	/**
	 * True if the first class is a subclass of the second
	 * 
	 * @param className
	 * @param superclassName
	 * @return boolean
	 */
	@WrapToScript
	public static boolean isSubClass(String className, String superclassName) {
		return UMLUtil.isSubClass(className, superclassName);
	}

	/**
	 * 
	 * @param element
	 * @param type
	 * @return boolean
	 */
	@WrapToScript
	public static boolean isTypeOf(Element element, String type) {
		if (element instanceof Type) {
			Type t = (Type) element;
			return isSubClass(t.getQualifiedName(), type);
		}

		if (element instanceof TypedElement) {
			TypedElement typed = (TypedElement) element;
			Type elmType = typed.getType();
			return isSubClass(elmType.getQualifiedName(), type);
		}
		return false;
	}

	/**
	 * Return true when the second parameter correspond to the class of the element
	 * or one of its super classes
	 * 
	 * @param element
	 * @param ancestorQualifiedClass
	 * @return boolean
	 */
	@WrapToScript
	public static boolean isInstanceOf(Element element, String ancestorQualifiedClass) {

		try {
			Class ancestorClass = Class.forName(ancestorQualifiedClass);
			return ancestorClass.isAssignableFrom(element.getClass());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return false;
	}

	/**
	 * Return the stereotype application of the element which is stereotyped by the
	 * second parameter or null if the element doesn't have the stereotype
	 * 
	 * @param element
	 * @param stereotype
	 * @param strict
	 *            if true it only searches in the element class, else it also
	 *            searches on its ancestors
	 * @return
	 */
	@WrapToScript
	public static EObject getStereotypeApplication(Element element, String stereotype, boolean strict) {
		Stereotype st = UMLUtil.getAppliedStereotype(element, stereotype, strict);

		if (st == null)
			return null;

		return element.getStereotypeApplication(st);
	}

	

	/**
	 * Return all elements typed by the specified UML class owned by the the first
	 * parameter and its sub-containers
	 * 
	 * @param element
	 * @param className
	 * @return List<Element>
	 */
	@WrapToScript
	public static List<Element> getElementsByUMLClass(Element element, String className) {
		List<Element> result = new ArrayList<Element>();
		List<Element> allElements = new ArrayList<Element>();

		for (Element ele : element.getOwnedElements()) {
			if (!(ele instanceof PackageImport) && !(ele instanceof ProfileApplication)) {
				allElements.add(ele);
			}
		}

		while (!allElements.isEmpty()) {
			Element current = allElements.get(0);
			allElements.remove(0);
			EClass eclass = current.eClass();
			if (eclass.getName().compareTo(className) == 0) {
				result.add(current);
			} else if (!current.getOwnedElements().isEmpty()) {
				for (Element ele : current.getOwnedElements()) {
					if (!allElements.contains(ele)) {
						allElements.add(ele);
					}
				}
			}
		}
		return result;
	}

	/**
	 * Return the property value named by the second parameter of the element
	 * 
	 * @param element
	 * @param attName
	 * @return Object
	 */
	@WrapToScript
	public static Object getPropertyValue(Element element, String attName) {
		Object result = null;
		EClass eclass = element.eClass();
		EList<EStructuralFeature> feats = eclass.getEAllStructuralFeatures();
		EStructuralFeature feature = null;
		for (Iterator<EStructuralFeature> it = feats.iterator(); it.hasNext() && feature == null;) {
			EStructuralFeature current = it.next();
			if (current.getName().compareTo(attName) == 0) {
				feature = current;
			}
		}
		if (feature != null) {
			result = element.eGet(feature);
		}

		return result;
	}

	/**
	 * Return the property value named by the third parameter of the element
	 * stereotype application named by the second parameter
	 * 
	 * @param element
	 * @param stereotype
	 * @param attName
	 * @return Object
	 */
	@WrapToScript
	public static Object getStereotypePropertyValue(Element element, String stereotype, String attName) {
		Object result = null;
		EObject st = getStereotypeApplication(element, stereotype, true);
		if (st != null) {
			EClass eclass = st.eClass();
			EList<EStructuralFeature> feats = eclass.getEAllStructuralFeatures();
			EStructuralFeature feature = null;
			for (Iterator<EStructuralFeature> it = feats.iterator(); it.hasNext() && feature == null;) {
				EStructuralFeature current = it.next();
				if (current.getName().compareTo(attName) == 0) {
					feature = current;
				}
			}
			if (feature != null) {
				result = st.eGet(feature);
			}
		}
		return result;
	}


	

}
