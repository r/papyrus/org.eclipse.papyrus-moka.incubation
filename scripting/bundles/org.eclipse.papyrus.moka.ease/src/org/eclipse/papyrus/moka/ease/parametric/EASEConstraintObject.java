/*****************************************************************************
 * Copyright (c) 2018 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   David López david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.ease.parametric;

import org.eclipse.papyrus.moka.parametric.semantics.ConstraintObject;

public class EASEConstraintObject extends ConstraintObject {

	private RunnableScript runnableScript;

	public void evaluate() {
		if (runnableScript == null) 
			runnableScript = RunnableScriptFactory.getInstance().getRunnableScriptFor(this);

		//TODO null checks here !
		
		runnableScript.evaluate();
	}
}
