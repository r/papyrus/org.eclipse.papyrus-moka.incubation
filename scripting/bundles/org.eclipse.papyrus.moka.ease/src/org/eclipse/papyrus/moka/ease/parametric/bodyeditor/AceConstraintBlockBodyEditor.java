/*****************************************************************************
 * Copyright (c) 2020 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.ease.parametric.bodyeditor;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.aceeditor.AceBodyEditor;
import org.eclipse.papyrus.infra.properties.ui.modelelement.ModelElement;
import org.eclipse.papyrus.moka.parametric.utils.NameUtils;
import org.eclipse.papyrus.sysml16.constraintblocks.ConstraintBlock;
import org.eclipse.papyrus.uml.properties.modelelement.UMLModelElement;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.Stereotype;

public class AceConstraintBlockBodyEditor extends AceBodyEditor{
	
	private Listener onCommitListener = new Listener() {
		@Override
		public void handleEvent(Event event) {
			onCommiting(event.text);
		}
	};
	
	@Override
	public void createWidget(Composite parent, int style) {
		super.createWidget(parent, style);
		addChangeListener(onCommitListener);
	}

	protected ConstraintBlock blockFromContext(ModelElement context) {
		if( ! (context instanceof UMLModelElement) ) 
			return null;
		
		UMLModelElement me = (UMLModelElement) context;
		
		if( !(me.getSource() instanceof OpaqueExpression) )
			return null;
		
		OpaqueExpression oe = (OpaqueExpression) me.getSource();
		
		if( !(oe.getOwner() instanceof Constraint) )
			return null;		
		
		Constraint constraint = (Constraint) oe.getOwner(); 
		
		if( !(constraint.getOwner() instanceof Class) )
			return null;
		
		Class c = (Class) constraint.getOwner();
		
		Stereotype st = c.getAppliedStereotype(NameUtils.SYSML_CONSTRAINTBLOCK_STEREOTYPE_NAME);
		EObject stApp = c.getStereotypeApplication(st);
		
		if( !(stApp instanceof ConstraintBlock) )
			return null;
		
		return (ConstraintBlock) stApp;
		
	}
	
	protected void onCommiting(String newValue) {
	}

}
