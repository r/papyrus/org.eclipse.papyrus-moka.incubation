/*****************************************************************************
 * Copyright (c) 2018 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.ease.semantics.proxy;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class ValueIteratorProxy<ValueType> implements Iterator<Object>{
	private int ptr;
	private List<ValueType> wrapped; 
	private ProxyAdapter<ValueType> adapter;
	
	public ValueIteratorProxy(List<ValueType> wrapped, ProxyAdapter<ValueType> adapter) {
		this.wrapped = wrapped;
		this.adapter = adapter;
	}

	@Override
	public boolean hasNext() {
		return ptr < wrapped.size();
	}

	@Override
	public Object next() {
		if( !hasNext() )
			throw new NoSuchElementException();
		
		Object value = adapter.getProxyFor(wrapped.get(ptr));
		ptr++;
		
		return value;
	}

}
