/*****************************************************************************
 * Copyright (c) 2018 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.ease.semantics.proxy;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MapProxy implements Map<String, Object>  {

	private ReflectiveAccessor accessor;
	
	public MapProxy(ReflectiveAccessor accessor) {
		this.accessor = accessor;
	}

	@Override
	public int size() {
		return accessor.featuresCount();
	}

	@Override
	public boolean isEmpty() {
		return accessor.featuresCount() == 0;
	}

	@Override
	public boolean containsKey(Object key) {
		return accessor.hasFeature((String) key);
	}

	@Override
	public boolean containsValue(Object value) {
		//TODO : Probably not used from the scripts
		return false;
	}

	@Override
	public Object get(Object key) {
		return accessor.getFeatureValue((String)key);
	}

	@Override
	public Object put(String key, Object value) {
		Object old = get(key);
		accessor.setFeatureValue(key, value);
		return old;
	}

	@Override
	public Object remove(Object key) {
		//TODO: Not a good idea to remove the feature from the proxy
		return null;
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> m) {
		//TODO : Probably not used from the scripts
	}

	@Override
	public void clear() {
		//TODO : Probably not used from the scripts
		
	}

	@Override
	public Set<String> keySet() {		
		return accessor.getFeatureNames();
	}

	@Override
	public Collection<Object> values() {
		return null;
	}

	@Override
	public Set<Entry<String, Object>> entrySet() {
		return null;
	}

	@Override
	public String toString() {
		
		StringBuilder sb = new StringBuilder();
		sb.append('{');
		
		int index = 0;
		int n = size();
		
		for(String key : keySet() ) {
			index ++;
			Object value = get(key);
			
			sb.append(key);
			sb.append(':');
			
			if( value instanceof List ) {
				List list = (List)value;
				sb.append('[');
				for( int i = 0; i < list.size(); i++ ) {
					sb.append(list.get(i));
					if( i < list.size() - 1 )
						sb.append(", ");
				}
				
				sb.append(']');				
			}else {
				sb.append(value);
			}
			if( index < n )
				sb.append(',').append(' ');
		}
		
		sb.append('}');
		
		return sb.toString();
	}
	
	
	public ReflectiveAccessor getAccessor() {
		return accessor;
	}
}
