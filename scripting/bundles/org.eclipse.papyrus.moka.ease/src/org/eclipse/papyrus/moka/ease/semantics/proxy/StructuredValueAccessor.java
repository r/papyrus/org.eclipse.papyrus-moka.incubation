/*****************************************************************************
 * Copyright (c) 2018 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.ease.semantics.proxy;

import java.util.HashMap;

import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IFeatureValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IStructuredValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IValue;
import org.eclipse.uml2.uml.StructuralFeature;

public class StructuredValueAccessor extends BaseReflectiveAccessor<StructuralFeature, IValue> {
	
	
	private IStructuredValue sv;
	
	
	public StructuredValueAccessor(IStructuredValue sv, ProxyAdapter<IValue> adapter) {
		super( adapter );
		this.sv = sv;
		this.features = new HashMap<>();
		
		for( IFeatureValue f : sv.getFeatureValues() )
			features.put(f.getFeature().getName(), f.getFeature());
	}
	
	/**
	 * 
	 * @param featureName
	 * @return the primitive (or a list wrapper) contained in the feature value
	 */
	public Object getFeatureValue(String featureName) {
		StructuralFeature sf = features.get(featureName);
		IFeatureValue value = sv.getFeatureValue(sf);
		
		return getProxyForFeatureValues(value.getValues(), sf);
	}
	
	/**
	 * 
	 * @param featureName
	 * @param obj Primitive object (java.lang.Integer, java.lang.Boolean ...) doesn't work with lists
	 */
	public void setFeatureValue(String featureName, Object obj) {
		StructuralFeature sf = features.get(featureName);
		IFeatureValue value = sv.getFeatureValue(sf);
		
		putValueInValueList(value.getValues(), sf, obj);
	}

	@Override
	public Object getAccessedObject() {
		return sv;
	}

}