/*****************************************************************************
 * Copyright (c) 2018 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.ease.semantics.proxy;

import java.util.Set;

public interface ReflectiveAccessor {
	
	public Object getFeatureValue(String featureName);
	
	public void setFeatureValue(String featureName, Object obj);
	
	public int featuresCount();
	
	public boolean hasFeature(String featureName);
	
	public Set<String> getFeatureNames();
	
	public Object getAccessedObject();
}
