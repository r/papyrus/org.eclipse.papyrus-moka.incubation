/*****************************************************************************
 * Copyright (c) 2018 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.ease.semantics.proxy;

import java.util.Map;

import org.eclipse.papyrus.moka.fuml.loci.ILocus;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.BooleanValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IBooleanValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IEnumerationValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IEvaluation;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IFeatureValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IIntegerValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IPrimitiveValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IRealValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IStringValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IStructuredValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IUnlimitedNaturalValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IntegerValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.RealValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.StringValue;
import org.eclipse.papyrus.moka.fuml.structuredclassifiers.IObject_;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.StructuralFeature;
import org.eclipse.uml2.uml.Type;

public class KernelProxyAdapter implements ProxyAdapter<IValue>{
	
	private ILocus locus;
	
	public KernelProxyAdapter() {		
	}
	
	public void setLocus(ILocus locus) {
		this.locus = locus;
	}

	@Override
	public Object getProxyFor(IValue value) {
		
		if( value instanceof IStructuredValue ) 
			return getProxyForStructuredValue( (IStructuredValue) value);
		
		if( value instanceof IEnumerationValue ) 
			return getProxyForEnumerationValue((IEnumerationValue) value);
		
		if( value instanceof IPrimitiveValue ) 
			return getProxyForPrimitiveValue( (IPrimitiveValue) value );
		
		//Not supported, 
		throw new RuntimeException("Proxies for " + value.getClass() + " are not supported yet.");
	}
	

	public Object getProxyForStructuredValue(IStructuredValue value) {
		return new MapProxy(new StructuredValueAccessor(value, this));
	}

	public Object getProxyForEnumerationValue(IEnumerationValue value) {
		//not implemented yet 
		throw new RuntimeException("Proxies for " + value.getClass() + " are not supported yet.");
	}

	public Object getProxyForPrimitiveValue(IPrimitiveValue value) {
		if( value instanceof IBooleanValue ) 
			return ((IBooleanValue)value).getValue();
		
		if( value instanceof IIntegerValue ) 
			return ((IIntegerValue)value).getValue();
		
		if( value instanceof IRealValue ) 
			return ((IRealValue)value).getValue();
		
		if( value instanceof IStringValue ) 
			return ((IStringValue)value).getValue();
		
		if( value instanceof IUnlimitedNaturalValue) 
			return ((IUnlimitedNaturalValue)value).getValue();
		
		//Not a supported primitive
		return null;
	}
	
	private IObject_ instanceLocusObject( org.eclipse.uml2.uml.Class type, Map<String, Object> mapValues) {
		IObject_ iObject = locus.instantiate( type );
		
		for( IFeatureValue fv : iObject.getFeatureValues() ) {
			StructuralFeature sf = fv.getFeature();
			String featureName = sf.getName();
			
			IValue value = null;
			
			if( !mapValues.containsKey(featureName) ) {
				//It's most likely a property
				Property p = (Property)sf;
				IEvaluation eval = locus.getFactory().createEvaluation( p.getDefaultValue() );
				value = eval.evaluate();
			}else {
				value = getValueFor(sf, mapValues.get(featureName) );
			}
			
			fv.getValues().add( value );
		}
		
		return iObject;
	}
	
	@Override
	public IValue getValueFor(StructuralFeature feature, Object obj) {
		
		Type type = feature.getType();
		
		if( obj instanceof Map ) {	
			if( type instanceof org.eclipse.uml2.uml.Class ) { //Is this the right type ?
				return instanceLocusObject((org.eclipse.uml2.uml.Class) type, (Map)obj);
			}
		}
		
		if( obj instanceof Double || obj instanceof Float ) {
			RealValue realValue = new RealValue();
			realValue.setValue( (Double)obj );
			return realValue;
		}
		
		if( obj instanceof Integer ) {
			IntegerValue intValue = new IntegerValue();
			intValue.setValue( (Integer)obj);
			return intValue;
		}
		
		if( obj instanceof Boolean ) {
			BooleanValue boolValue = new BooleanValue();
			boolValue.setValue( (Boolean)obj );
			return boolValue;
		}
		
		if( obj instanceof String ) {
			StringValue strValue = new StringValue();
			strValue.setValue( (String)obj );
			return strValue;
		}
		
		//Not supported primitive?
		return null;
	}
	
}
