/*****************************************************************************
 * Copyright (c) 2018 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.ease.semantics.proxy;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.eclipse.uml2.uml.StructuralFeature;

public class ValueListProxy<ValueType> implements List {
	private List<ValueType> wrapped;
	private ProxyAdapter<ValueType> adapter;
	private StructuralFeature feature;
	
	public ValueListProxy(StructuralFeature feature, List<ValueType> wrapped, ProxyAdapter<ValueType> adapter) {
		super();
		this.wrapped = wrapped;
		this.adapter = adapter;
		this.feature = feature;
	}

	@Override
	public int size() {
		return wrapped.size();
	}

	@Override
	public boolean isEmpty() {
		return wrapped.isEmpty();
	}

	//TODO This probably won't work
	@Override
	public boolean contains(Object o) {
		return wrapped.contains(o);
	}

	@Override
	public Iterator iterator() {		
		return new ValueIteratorProxy<ValueType>(wrapped, adapter);
	}

	//TODO This won't work from the script
	@Override
	public Object[] toArray() {
		return wrapped.toArray();
	}

	@Override
	public Object[] toArray(Object[] a) {
		return wrapped.toArray(a);
	}

	@Override
	public boolean add(Object e) {
		return wrapped.add(adapter.getValueFor(feature, e));
	}
	
	@Override
	public boolean remove(Object o) {
		return wrapped.remove(o);
	}

	@Override
	public boolean containsAll(Collection c) {
		return wrapped.containsAll(c);
	}

	//TODO Wrap individually the collection values
	@Override
	public boolean addAll(Collection c) {
		return wrapped.addAll(c);
	}

	//TODO Wrap individually the collection values
	@Override
	public boolean addAll(int index, Collection c) {
		return wrapped.addAll(index, c);
	}

	@Override
	public boolean removeAll(Collection c) {
		return wrapped.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection c) {
		return wrapped.retainAll(c);
	}

	@Override
	public void clear() {
		wrapped.clear();
	}

	@Override
	public Object get(int index) {
		return adapter.getProxyFor(wrapped.get(index));
	}

	@Override
	public Object set(int index, Object element) {
		return wrapped.set(index, adapter.getValueFor(feature, element));
	}

	@Override
	public void add(int index, Object element) {
		wrapped.add(index, adapter.getValueFor(feature, element));
	}

	@Override
	public Object remove(int index) {
		return wrapped.remove(index);
	}

	//TODO This probably won't work
	@Override
	public int indexOf(Object o) {
		return wrapped.indexOf(o);
	}

	//TODO This probably won't work
	@Override
	public int lastIndexOf(Object o) {
		return wrapped.lastIndexOf(o);
	}

	@Override
	public ListIterator listIterator() {
		return wrapped.listIterator();
	}

	@Override
	public ListIterator listIterator(int index) {
		return wrapped.listIterator(index);
	}

	@Override
	public List subList(int fromIndex, int toIndex) {
		return wrapped.subList(fromIndex, toIndex);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append('[');
		
		int n = size();
		
		for( int i = 0; i < n; i++ ) {
			sb.append(get(i));
			
			if( i < n - 1 )
				sb.append(',').append(' ');
		}
		
		sb.append(']');
		
		return sb.toString();
	}
	
	
}
