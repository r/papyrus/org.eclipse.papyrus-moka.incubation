/*****************************************************************************
 * Copyright (c) 2018 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.ease.semantics.proxy;

import java.util.HashMap;

import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.Slot;
import org.eclipse.uml2.uml.ValueSpecification;

public class InstanceSpecificationAccessor extends BaseReflectiveAccessor<Slot, ValueSpecification> {
	
	
	private InstanceSpecification is;

	public InstanceSpecificationAccessor(InstanceSpecification is, ProxyAdapter<ValueSpecification> adapter) {
		super(adapter);
		this.features = new HashMap<>();
		
		for( Slot s : is.getSlots() ) 
			features.put(s.getDefiningFeature().getName(), s);
		
		this.is = is;
	}

	@Override
	public Object getFeatureValue(String featureName) {
		Slot slot = features.get(featureName);
		return getProxyForFeatureValues(slot.getValues(), slot.getDefiningFeature());
	}

	@Override
	public void setFeatureValue(String featureName, Object obj) {
		Slot slot = features.get(featureName);
		
		putValueInValueList(slot.getValues(), slot.getDefiningFeature(), obj);
	}

	@Override
	public Object getAccessedObject() {
	
		return is;
	}
	
}
