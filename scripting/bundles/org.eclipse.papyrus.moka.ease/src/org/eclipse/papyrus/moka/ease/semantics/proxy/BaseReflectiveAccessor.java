/*****************************************************************************
 * Copyright (c) 2018 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.ease.semantics.proxy;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.uml2.uml.StructuralFeature;

public abstract class BaseReflectiveAccessor<ContainerType, ValueType> implements ReflectiveAccessor{

	protected Map<String, ContainerType> features;
	private ProxyAdapter<ValueType> adapter;
	
	public BaseReflectiveAccessor(ProxyAdapter<ValueType> adapter) {
		super();
		this.adapter = adapter;
	}

	@Override
	public int featuresCount() {
		return features.size();
	}

	@Override
	public boolean hasFeature(String featureName) {
		return features.containsKey(featureName);
	}

	@Override
	public Set<String> getFeatureNames() {
		return features.keySet();
	}

	protected Object getProxyForFeatureValues(List<ValueType> featureValues, StructuralFeature sf) {
		
		//The feature has multiple values, maybe return an empty list
		if( sf.isMultivalued() )
			return new ValueListProxy<ValueType>(sf, featureValues, adapter);
		
		//but it doesn't a value
		if( featureValues.size() == 0 )
			return null;	
		
		//it does has a value
		return adapter.getProxyFor(featureValues.get(0));
	}
	
	protected void putValueInValueList(List<ValueType> values, StructuralFeature feature, Object obj) {
		if( feature.isMultivalued() ) {
			if( obj instanceof List ) {
				List l = (List)obj;
				
				for( Object o : l ) {
					values.add(adapter.getValueFor(feature, o));
				}
			}
			
			return;
		}
		
		//This comes from a 
		if( values.size() == 0 )
			values.add(adapter.getValueFor(feature, obj));
		else
			values.set(0, adapter.getValueFor(feature, obj));
	}
}
