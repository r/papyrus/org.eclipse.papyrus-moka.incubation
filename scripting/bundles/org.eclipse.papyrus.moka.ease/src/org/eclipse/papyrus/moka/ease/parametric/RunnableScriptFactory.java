/*****************************************************************************
 * Copyright (c) 2018 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.ease.parametric;

import org.eclipse.ease.IScriptEngine;
import org.eclipse.papyrus.moka.ease.parametric.EASEEngineManager.EASELanguage;
import org.eclipse.papyrus.moka.ease.parametric.javascript.JavascriptRunnableScriptCompiler;
import org.eclipse.papyrus.moka.ease.parametric.python.JythonRunnableScriptCompiler;
import org.eclipse.papyrus.moka.ease.parametric.python.PythonRunnableScriptCompiler;
import org.eclipse.papyrus.moka.ease.semantics.proxy.KernelProxyAdapter;
import org.eclipse.papyrus.moka.ease.semantics.proxy.ScriptProxy;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.ValueSpecification;

public class RunnableScriptFactory {
	
	private RunnableScriptFactory() {
		
	}
	
	private static RunnableScriptFactory instance = new RunnableScriptFactory();
	
	public static RunnableScriptFactory getInstance() {		
		return instance;
	}
	
	public RunnableScript getRunnableScriptFor(EASEConstraintObject obj) {
		
		OpaqueExpression oe = getOpaqueExpression(obj);
		
		if( oe == null )
			return null;
					
		EASELanguage lang = getLanguageFor(oe);
		if( lang == null )
			return null;
		
		String body = getScriptBody(oe);
		if( body == null )
			return null;
		
		RunnableScriptCompiler compiler = getCompiler(lang);
		
		KernelProxyAdapter adapter = ScriptProxy.getKernelAdapter();
		adapter.setLocus(obj.getLocus());	
		
		return compiler.compileRunnableScript(obj, adapter.getProxyFor(obj), body);
	}

	private RunnableScriptCompiler getCompiler(EASELanguage lang) {
		
		IScriptEngine engine = EASEEngineManager.getEngine(lang);
		
		switch(lang) {
			case Javascript:
				return new JavascriptRunnableScriptCompiler(engine); 
			case Jython:
				return new JythonRunnableScriptCompiler(engine);
			case Python:
				return new PythonRunnableScriptCompiler(engine);
		}
		
		return null;
	}
	
	private OpaqueExpression getOpaqueExpression(EASEConstraintObject obj) {
		Classifier type = obj.getTypes().get(0);
		Constraint constraint = type.getOwnedRules().get(0);
		
		ValueSpecification spec = constraint.getSpecification();
		
		if (!(spec instanceof OpaqueExpression))
			return null;
		
		return (OpaqueExpression) spec;
	}
	
	private EASELanguage getLanguageFor(OpaqueExpression exp) {
		return EASELanguage.fromSimpleName(exp.getLanguages().get(0));
	}
	
	private String getScriptBody(OpaqueExpression exp) {
		if (exp.getBodies().size() == 0)
			return null;
		
		return exp.getBodies().get(0);
	}
}
