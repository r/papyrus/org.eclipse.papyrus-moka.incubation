/*****************************************************************************
 * Copyright (c) 2020 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.ease.semantics.proxy;

import org.eclipse.papyrus.moka.modeling.utils.InstanceSpecificationGeneratorUtil;
import org.eclipse.papyrus.sysml16.constraintblocks.ConstraintBlock;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Slot;
import org.eclipse.uml2.uml.ValueSpecification;
import org.eclipse.uml2.uml.util.UMLUtil;

public class ParametricInstanceSpecGenerator extends InstanceSpecificationGeneratorUtil{

	private boolean initializedDerived;


	public ParametricInstanceSpecGenerator(Package rootContainer , boolean initializeDerived) {
		super(rootContainer);
		this.initializedDerived = initializeDerived;
	}
	
	@Override
	protected boolean canGenerateSlot(Classifier contextClassifier, Property property) {
		if (! initializedDerived && property.isDerived()) {
			return false;
		}else {
			if (property.getType()!= null) {
				return UMLUtil.getStereotypeApplication(property.getType(), ConstraintBlock.class)== null ;
			}else {
				return true;
			}
			
		}
		
	}
	
	protected ValueSpecification generateValueSpecification(Slot slot) {
		ValueSpecification result = super.generateValueSpecification(slot);
		result.unsetName();
		
		return result;
	}
	
}
