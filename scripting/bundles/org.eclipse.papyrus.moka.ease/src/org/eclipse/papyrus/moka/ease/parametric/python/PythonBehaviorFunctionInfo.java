/*****************************************************************************
 * Copyright (c) 2020 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.ease.parametric.python;

import java.util.ArrayList;
import java.util.List;

public class PythonBehaviorFunctionInfo {
	
	public static class PythonAccess{
		private String name;
		private int line;
		private int column;
		private boolean read;
		
		public PythonAccess(String name, int line, int column, boolean read) {
			super();
			this.name = name;
			this.line = line;
			this.column = column;
			this.read = read;
		}
		
		public String getName() {
			return name;
		}
		public int getLine() {
			return line;
		}
		public int getColumn() {
			return column;
		}
		
		public boolean isRead() {
			return read;
		}
	}
	
	public static class PythonError{
		private Throwable error;
		private int line;
		private int column;
		
		public PythonError(Throwable error, int line, int column) {
			super();
			this.error = error;
			this.line = line;
			this.column = column;
		}
		public Throwable getError() {
			return error;
		}
		public int getLine() {
			return line;
		}
		public int getColumn() {
			return column;
		}
		public String getFirstErrorLine() {
			if( this.error == null )
				return null;
			
			String msg = this.error.getMessage();
			int index = msg.indexOf('\n');
			if( index > 0 )
				return msg.substring(0, index);
			
			return msg;
		}
	}
	
	private String name;
	private int codeLineDefinition;
	private String blockParamName;
	private List<PythonAccess> paramAccesses;
	private PythonError parserError;
	
	public PythonBehaviorFunctionInfo() {
		paramAccesses = new ArrayList<>();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBlockParamName() {
		return blockParamName;
	}
	public void setBlockParamName(String blockParamName) {
		this.blockParamName = blockParamName;
	}
	public List<PythonAccess> getParamAccesses() {		
		return paramAccesses;
	}
	
	public int getCodeLineDefinition() {
		return codeLineDefinition;
	}
	public void setCodeLineDefinition(int codeLineDefinition) {
		this.codeLineDefinition = codeLineDefinition;
	}
	
	public PythonError getParserError() {
		return parserError;
	}
	public void setParserError(PythonError parserError) {
		this.parserError = parserError;
	}
	@Override
	public String toString() {
		return "PythonOpaqueBehaviorFunction [name=" + name + ", blockParamName=" + blockParamName + ", paramsAccessed="
				+ paramAccesses + "]";
	}
	
}
