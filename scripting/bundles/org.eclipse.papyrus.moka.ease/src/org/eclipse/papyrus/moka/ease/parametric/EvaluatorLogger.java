/*****************************************************************************
 * Copyright (c) 2020 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.ease.parametric;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.papyrus.moka.ease.semantics.proxy.LoggedValue;
import org.eclipse.papyrus.moka.ease.semantics.proxy.MapProxy;

public class EvaluatorLogger {
	private List<LoggedValue> log = new ArrayList<>();
	
	public void clearLog() {
		this.log.clear();
	}
	
	public void addValue(LoggedValue value) {
		this.log.add(value);
	}
	/**
	 * Enables the recording of a given variable in the locus
	 * @param variablePath : the "." separated feature path to the variable to log
	 * @param alias : an alias for the variable name 
	 */
	public void log(String variablePath, String alias, MapProxy proxyObject) {
		log.add(new LoggedValue(variablePath, alias, proxyObject));
	}
	
	/**
	 * Enables the recording of a given variable in the locus, using the variable path as default alias
	 * @param variablePath : the "." separated feature path to the variable to log
	 */
	public void log(String variablePath, MapProxy objectProxy) {
		log(variablePath, variablePath, objectProxy);
	}

	/**
	 * 
	 * @return the log containing logged values
	 */
	public List<LoggedValue> getLog() {
		return log;
	}
	
	/**
	 * This method forces the recording of the current value for all logged values 
	 */
	public void logValues() {
		
		for (LoggedValue loggedValue : log) {
			loggedValue.logValue();
		}

	}
}
