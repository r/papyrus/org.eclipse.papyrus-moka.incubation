/*****************************************************************************
 * Copyright (c) 2020 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.ease.parametric;

import org.eclipse.papyrus.moka.ease.semantics.proxy.TimeValue;
import org.eclipse.papyrus.moka.fuml.loci.ILocus;
import org.eclipse.papyrus.moka.fuml.structuredclassifiers.IObject_;
import org.eclipse.papyrus.moka.parametric.ParametricEvaluator;
import org.eclipse.papyrus.moka.parametric.semantics.ParametricObject;
import org.eclipse.uml2.uml.InstanceSpecification;

public class DeferredParametricEvaluator extends ParametricEvaluator {

	private EvaluatorLogger logger = new EvaluatorLogger();
	
	@Override
	public ParametricObject init(InstanceSpecification instance, ILocus locus) {
		ParametricObject obj = super.init(instance, locus);
		logger.clearLog();
		return obj;
	}
	
	/**
	 * Single step evaluation
	 * @return the root Object of the locus
	 */
	public IObject_ evaluate() {
		IObject_ object = super.evaluate();

		logger.logValues();

		return object;

	}

	/**
	 * Multiple steps evaluation
	 * @return the root Object of the locus
	 */
	public IObject_ evaluate(int steps) {
		//we log initial values
		logger.logValues();
		
		for (int i = 0; i < steps; i++) {
			evaluate();
		}
		return mainObject;

	}
	
	
	/**
	 * Multiple steps evaluation
	 * @return the root Object of the locus
	 */
	public IObject_ evaluate(double startTime, double endTime, double dt) {
		//we create a time value vector 
		TimeValue time = new TimeValue(startTime, dt);
		logger.addValue(time);
		//we log initial values
		logger.logValues();
		
		while (time.getValue() <= endTime)  {
			//evalue will call logValues, which automatically increments time
			evaluate();
		}
		return mainObject;

	}
	
	public EvaluatorLogger getLogger() {
		return this.logger;
	}
	
}
