/*****************************************************************************
 * Copyright (c) 2018 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
    
 *****************************************************************************/
package org.eclipse.papyrus.moka.ease.semantics.proxy;


/**
 * Specialization of LoggedValue to store current time when {@link org.eclipse.papyrus.moka.parametric.QuietCausalParametricInterpreter#evaluate(double, double, double) 
 * evaluate(start, end, dt)} is called
 *
 */
public class TimeValue extends LoggedValue{

	double currentTime =0.0;
	double step = 0.0;
	
	/**
	 * Dedicated constructor taking start time and time step as parameters
	 * @param start : the start time
	 * @param step : the time step to increment time
	 */
	public TimeValue(double start,  double step) {
		currentTime = start;
		this.step = step;
		alias = "time";
	
	}
	

	/** 
	 * Stores current time value and increments its value after
	 */
	@Override
	public	void logValue() {
		values.add(currentTime);
		currentTime += step;
	}
	
	@Override
	public Double getValue() {
		return currentTime;
	}
	
}