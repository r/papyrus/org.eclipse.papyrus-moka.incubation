/*****************************************************************************
 * Copyright (c) 2018 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.ease.semantics.proxy;

import java.util.Map;

import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.InstanceValue;
import org.eclipse.uml2.uml.LiteralBoolean;
import org.eclipse.uml2.uml.LiteralInteger;
import org.eclipse.uml2.uml.LiteralNull;
import org.eclipse.uml2.uml.LiteralReal;
import org.eclipse.uml2.uml.LiteralString;
import org.eclipse.uml2.uml.LiteralUnlimitedNatural;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Slot;
import org.eclipse.uml2.uml.StructuralFeature;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.ValueSpecification;

public class ValueSpecificationProxyAdapter implements ProxyAdapter<ValueSpecification>{
	
	public Object getProxyForInstanceSpecification(InstanceSpecification is) {
		return new MapProxy( new InstanceSpecificationAccessor(is, this));
	}
	
	public Object getProxyForInstanceValue(InstanceValue value) {
		return new MapProxy(new InstanceSpecificationAccessor(value.getInstance(), this));
	}

	@Override
	public Object getProxyFor(ValueSpecification value) {
		
		if( value instanceof LiteralBoolean )
			return value.booleanValue();
		
		if( value instanceof LiteralInteger )
			return value.integerValue();
		
		if( value instanceof LiteralNull )
			return null;
		
		if( value instanceof LiteralReal )
			return value.realValue();
		
		if( value instanceof LiteralString )
			return value.stringValue();
		
		if( value instanceof LiteralUnlimitedNatural )
			return value.integerValue();
		
		if( value instanceof InstanceValue )
			return getProxyForInstanceValue((InstanceValue) value);
		
		return null;
	}

	@Override
	public ValueSpecification getValueFor(StructuralFeature feature, Object obj) {
		Type type = feature.getType();
		
		if (obj instanceof Map) {
			if( type instanceof org.eclipse.uml2.uml.Class) {
				return createInstanceValue(( org.eclipse.uml2.uml.Class) type, (Map)obj);
			}
		}
		
		if( obj instanceof Double || obj instanceof Float ) {
			LiteralReal realValue = UMLFactory.eINSTANCE.createLiteralReal();
			realValue.setValue( (Double)obj );
			return realValue;
		}
		
		if( obj instanceof Integer ) {
			LiteralInteger integerValue = UMLFactory.eINSTANCE.createLiteralInteger();
			integerValue.setValue( (Integer)obj);
			return integerValue;
		}
		
		if( obj instanceof Boolean ) {
			LiteralBoolean boolValue = UMLFactory.eINSTANCE.createLiteralBoolean();
			boolValue.setValue( (Boolean)obj );
			return boolValue;
		}
		
		if( obj instanceof String ) {
			LiteralString strValue =  UMLFactory.eINSTANCE.createLiteralString();
			strValue.setValue( (String)obj );
			return strValue;
		}
		
		//Not supported primitive?
		return null;
		
	}

	private ValueSpecification createInstanceValue(Class type, Map<String, Object> map) {
		ParametricInstanceSpecGenerator generator = new ParametricInstanceSpecGenerator(null, false);
		InstanceSpecification result = generator.generateInstanceSpecification(type);
		
		for (Property prop : type.getAllAttributes()) {
			String featureName = prop.getName();
			Object mapValue = map.get(featureName);
			if( mapValue != null) {
				ValueSpecification valSpec = getValueFor(prop, mapValue);
				if (valSpec != null) {
					Slot slot = getSlot(result, prop);
					slot.getValues().add(valSpec);
				}
			}
			
		}
		return null;
	}

	private Slot getSlot(InstanceSpecification instanceSpec, Property prop) {
		for (Slot slot : instanceSpec.getSlots()) {
			if (slot.getDefiningFeature() == prop) {
				return slot;
			}
		}
		Slot result = instanceSpec.createSlot();
		result.setDefiningFeature(prop);
		return result;
	}
}
