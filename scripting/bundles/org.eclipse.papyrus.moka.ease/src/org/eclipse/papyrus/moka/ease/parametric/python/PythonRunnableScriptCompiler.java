/*****************************************************************************
 * Copyright (c) 2018 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.ease.parametric.python;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.concurrent.ExecutionException;

import org.eclipse.ease.IScriptEngine;
import org.eclipse.ease.Script;
import org.eclipse.ease.ScriptResult;
import org.eclipse.papyrus.moka.ease.parametric.EASEConstraintObject;
import org.eclipse.papyrus.moka.ease.parametric.RunnableScript;
import org.eclipse.papyrus.moka.ease.parametric.RunnableScriptCompiler;
import org.eclipse.papyrus.moka.ease.parametric.ScriptBehaviorConstants;

public class PythonRunnableScriptCompiler extends RunnableScriptCompiler{

	protected static final String SPLIT_KEYWORD = "%%%%%%SPLIT%%%%%%";
	protected static final String TAB = "   ";
	
	public PythonRunnableScriptCompiler(IScriptEngine engine) {
		super(engine);
	}

	protected String getTab(int numOfTabs) {
		String result = "";
		
		for (int i = 0;i<numOfTabs;i++) {
			result += TAB;
		}
		return result;
	}
	
	protected String getFunctionName(EASEConstraintObject obj) {
		return obj.getIdentifier().replaceAll("@|-", "")+ScriptBehaviorConstants.SCRIPT_FUNCTION_NAME;
	}
	
	public String generateGlueCode(EASEConstraintObject obj, String body) {
		
		String scriptBody = new String(body);
		
		scriptBody = scriptBody.replaceAll("\t",getTab(1));
		scriptBody = scriptBody.replaceAll("(?m)^def.*run.*", SPLIT_KEYWORD+"\n");
		
		BufferedReader lineReader = new BufferedReader(new StringReader(scriptBody));
		StringBuilder bodyBuilder = new StringBuilder();
		
		
		String bodyLine;
		try {
			bodyLine = lineReader.readLine()+'\n';
			
			while (bodyLine != null && !bodyLine.startsWith((SPLIT_KEYWORD))){
				bodyBuilder.append(bodyLine).append('\n');
				bodyLine = lineReader.readLine();
			}
			
			bodyBuilder.append("class "+getFunctionName(obj)+"(object):\n");
				
			bodyBuilder.append(getTab(1)+"def __init__(self):\n");
			bodyBuilder.append(getTab(2)+"self.block= PythonWrapper(paramObject.getProxyParam())\n");
			
			bodyBuilder.append(getTab(1)+"def evaluate(self):\n");
			bodyBuilder.append(getTab(2)+"self.run(self.block)\n\n");
				
			
			if (bodyLine != null && bodyLine.startsWith(SPLIT_KEYWORD)) {
				bodyBuilder.append(getTab(1)+"def run(self, block):\n");
			}
			
			while ((bodyLine = lineReader.readLine())!= null) {
				bodyBuilder.append(getTab(1)+bodyLine+"\n");			
			}
			
			bodyBuilder.append(getTab(1)+"class Java:\n");
			bodyBuilder.append(getTab(2)+"implements = ['"+RunnableScript.class.getName()+"']\n");
		
			bodyBuilder.append("\nparamObject.setScript("+getFunctionName(obj)+"())");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bodyBuilder.toString();
	}

	@Override
	public RunnableScript compileRunnableScript(EASEConstraintObject obj,  Object objProxy, String scriptBody) {
		ConstraintWrapper wrapper = new ConstraintWrapper(obj, objProxy);
		engine.setVariable("paramObject", wrapper);
		try {
			engine.inject(new Script(generateGlueCode(obj, scriptBody)), false);
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return wrapper.getScript();
	}

	public static class ConstraintWrapper {
		private EASEConstraintObject obj;
		private Object param;
		private RunnableScript script;

		public ConstraintWrapper(EASEConstraintObject obj, Object param) {
			super();
			this.obj = obj;
			this.param = param;
		}

		public Object getProxyParam() {
			return param;
		}

		public RunnableScript getScript() {
			return script;
		}

		public void setScript(RunnableScript script) {
			this.script = script;
		}

	}
	
}
