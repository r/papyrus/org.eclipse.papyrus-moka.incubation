/*****************************************************************************
 * Copyright (c) 2020 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.ease.modules;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.ease.modules.ScriptParameter;
import org.eclipse.ease.modules.WrapToScript;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.papyrus.moka.ease.parametric.ParametricEngineProxy;
import org.eclipse.papyrus.moka.ease.semantics.proxy.MapProxy;
import org.eclipse.papyrus.moka.ease.semantics.proxy.ParametricInstanceSpecGenerator;
import org.eclipse.papyrus.moka.ease.semantics.proxy.ScriptProxy;
import org.eclipse.papyrus.moka.fuml.structuredclassifiers.IObject_;
import org.eclipse.papyrus.moka.pscs.structuredclassifiers.CS_Reference;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.InstanceValue;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Slot;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.ValueSpecification;
import org.eclipse.uml2.uml.util.UMLUtil;

public class MokaParametricModule {

	public static final String MODULE_ID = "/Modeling/MokaParametric";
	

	@WrapToScript
	public static Object runFromFile(String umlFileURI, String elementURIFragment) {
		ResourceSetImpl rSet = new ResourceSetImpl();
		Resource resource = rSet.getResource(URI.createURI(umlFileURI), true);
		
		Collection<NamedElement> namedElements = UMLUtil.findNamedElements(resource, elementURIFragment);
		
		if( !namedElements.iterator().hasNext() ) {
			return null;
		}
		
		
		Element elm = (Element) namedElements.iterator().next();
		ParametricEngineProxy engine = getParametricEngine();
		return engine.initEngine(elm);
//		return ScriptProxy.getInstanceValueAdapter().getProxyForInstanceSpecification(run(elm));
	}
	
	@WrapToScript
	public static ParametricEngineProxy getParametricEngine() {
		return new ParametricEngineProxy();
	}

	@WrapToScript
	public static Object unwrapInstance(MapProxy proxy) {
		return proxy.getAccessor().getAccessedObject();
	}
	
	@WrapToScript
	public static Object wrap(InstanceSpecification is) {
		return ScriptProxy.getInstanceValueAdapter().getProxyForInstanceSpecification(is);
	}

	@WrapToScript
	public static Object getConfig(Classifier context,
			@ScriptParameter(defaultValue = ScriptParameter.NULL) Package targetPackage,
			@ScriptParameter(defaultValue = "false") boolean initializeDerived) {

		InstanceSpecification instance = getInstanceInstanceSpec(context, targetPackage, initializeDerived);
		if (instance != null) {
			return ScriptProxy.getInstanceValueAdapter().getProxyForInstanceSpecification(instance);
		}
		return null;
	}

	@WrapToScript
	public static void save(MapProxy configOrResult, String configName, Package receivingPackage) {

		InstanceSpecification instSpec = null;
		if (configOrResult.getAccessor() != null) {
			Object accessObject = configOrResult.getAccessor().getAccessedObject();
			if (accessObject instanceof InstanceSpecification) {
				instSpec = (InstanceSpecification) accessObject;
			} else if (accessObject instanceof CS_Reference) {
				ValueSpecification valSpec = ((CS_Reference) accessObject).getReferent().specify();
				if (valSpec instanceof InstanceValue) {
					instSpec = ((InstanceValue) valSpec).getInstance();
				}
			}else if (accessObject instanceof IObject_) {
				ValueSpecification valSpec = ((IObject_) accessObject).specify();
				if (valSpec instanceof InstanceValue) {
					instSpec = ((InstanceValue) valSpec).getInstance();
				}
			}
		}

		if (instSpec != null) {
			if (!instSpec.getClassifiers().isEmpty()) {
				instSpec.setName(instSpec.getClassifiers().get(0).getName());
			}
			
			Package configPackage = receivingPackage.createNestedPackage(configName);
			moveInstanceSpec(instSpec, configPackage);

		}

	}

	public static InstanceSpecification getInstanceInstanceSpec(Classifier context, Package targetPackage,
			boolean initializeDerived) {
		if (targetPackage == null) {
			targetPackage = UMLFactory.eINSTANCE.createPackage();
		}

		ParametricInstanceSpecGenerator generator = new ParametricInstanceSpecGenerator(targetPackage,
				initializeDerived);

		return generator.generateInstanceSpecification(context);

	}

	public static void moveInstanceSpec(InstanceSpecification instance, Package receivingPackage) {

		List<InstanceSpecification> instanceSpecsToMove = new ArrayList<>();
		instanceSpecsToMove.add(instance);
		collectInstanceSpecs(instance, instanceSpecsToMove);
		receivingPackage.getPackagedElements().addAll(instanceSpecsToMove);

	}

	private static void collectInstanceSpecs(InstanceSpecification instance,
			List<InstanceSpecification> instanceSpecsToMove) {
		for (Slot slot : instance.getSlots()) {
			for (ValueSpecification valSpec : slot.getValues()) {
				if (valSpec instanceof InstanceValue) {
					InstanceValue value = (InstanceValue) valSpec;
					InstanceSpecification referencedInstance = value.getInstance();
					if (referencedInstance != null && !instanceSpecsToMove.contains(referencedInstance)) {
						instanceSpecsToMove.add(referencedInstance);
						collectInstanceSpecs(referencedInstance, instanceSpecsToMove);
					}
				}
			}
		}

	}

}
