/*****************************************************************************
 * Copyright (c) 2018 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.ease.parametric.python;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

import org.eclipse.ease.IScriptEngine;
import org.eclipse.papyrus.moka.ease.parametric.EASEConstraintObject;
import org.eclipse.papyrus.moka.ease.parametric.RunnableScript;

public class JythonRunnableScriptCompiler extends PythonRunnableScriptCompiler{

	
	public JythonRunnableScriptCompiler(IScriptEngine engine) {
		super(engine);
	}

	@Override
	public String generateGlueCode(EASEConstraintObject obj, String body) {
		
		String scriptBody = new String(body);
		
		scriptBody = scriptBody.replaceAll("\t",getTab(1));
		scriptBody = scriptBody.replaceAll("(?m)^def.*run.*", SPLIT_KEYWORD+"\n");
		
		BufferedReader lineReader = new BufferedReader(new StringReader(scriptBody));
		StringBuilder bodyBuilder = new StringBuilder();
		
		
		String bodyLine;
		try {
			bodyLine = lineReader.readLine()+'\n';
			
			while (bodyLine != null && !bodyLine.startsWith((SPLIT_KEYWORD))){
				bodyBuilder.append(bodyLine).append('\n');
				bodyLine = lineReader.readLine();
			}
			
			
			bodyBuilder.append("from "+RunnableScript.class.getPackage().getName()+" import "+ RunnableScript.class.getSimpleName()+"\n");
			bodyBuilder.append("class "+getFunctionName(obj)+"("+RunnableScript.class.getSimpleName()+"):\n");
			
			bodyBuilder.append(getTab(1)+"def __init__(self):\n");
			bodyBuilder.append(getTab(2)+"self.block= PythonWrapper(paramObject.getProxyParam())\n");
			
			bodyBuilder.append(getTab(1)+"def evaluate(self):\n");
			bodyBuilder.append(getTab(2)+"self.run(self.block)\n\n");
				
			
			if (bodyLine != null && bodyLine.startsWith(SPLIT_KEYWORD)) {
				bodyBuilder.append(getTab(1)+"def run(self, block):\n");
			}
			
			while ((bodyLine = lineReader.readLine())!= null) {
				bodyBuilder.append(getTab(1)+bodyLine+"\n");			
			}
		
			bodyBuilder.append("\nparamObject.setScript("+getFunctionName(obj)+"())");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bodyBuilder.toString();
	}

}
