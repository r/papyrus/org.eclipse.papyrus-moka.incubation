/*****************************************************************************
 * Copyright (c) 2018 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   David L�pez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.ease.parametric;

import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import org.eclipse.core.runtime.Platform;
import org.eclipse.ease.IReplEngine;
import org.eclipse.ease.Script;
import org.eclipse.ease.ScriptResult;
import org.eclipse.ease.service.EngineDescription;
import org.eclipse.ease.service.IScriptService;
import org.eclipse.ease.service.ScriptService;
import org.osgi.framework.Bundle;

public class EASEEngineManager {

//	Name: engineID
//	==============
//	Python (Jython): org.eclipse.ease.python.jython
//	Script Archive: org.eclipse.ease.lang.scriptarchive.engine
//	JVM: org.eclipse.ease.jvm.compiled
//	Python (Py4J): org.eclipse.ease.lang.python.py4j.engine
//	Native: org.eclipse.ease.engine.native
//	JavaScript (Nashorn): org.eclipse.ease.javascript.nashorn
//	JavaScript (Rhino Debugger): org.eclipse.ease.javascript.rhinoDebugger
//	JavaScript (Rhino): org.eclipse.ease.javascript.rhino
//	Python (Py4J Debugger): org.eclipse.ease.lang.python.py4j.debugger.engine
//	Python (Jython Debugger): org.eclipse.ease.python.jythonDebugger

	public static enum EASELanguage {
		Python("org.eclipse.ease.lang.python.py4j.engine", "scripts/python/bootstrap.py"),
		Jython("org.eclipse.ease.python.jython", "scripts/python/bootstrap.py"), // For the lack of a better name ...
		Javascript("org.eclipse.ease.javascript.nashorn", "scripts/js/bootstrap.js");

		private String id;
		private String bootstrapPath;

		private EASELanguage(String id, String bootstrapPath) {
			this.id = id;
			this.bootstrapPath = bootstrapPath;
		}

		public String getId() {
			return id;
		}

		public String getBootstrapPath() {
			return bootstrapPath;
		}

		public static EASELanguage fromSimpleName(String simpleName) {
			EASELanguage lang = null;

			try {
				lang = EASELanguage.valueOf(EASELanguage.class, simpleName);
			} catch (IllegalArgumentException e) {
				return null;
			}

			return lang;
		}
	}

	private static final HashMap<EASELanguage, IReplEngine> engines = new HashMap<>();

	public static IReplEngine getEngine(EASELanguage language) {
		IReplEngine engine = engines.get(language);

		if (engine == null || engine.isFinished())
			return startEngine(language);

		return engine;
	}

	private static IReplEngine startEngine(EASELanguage language) {
		final IScriptService scriptService = ScriptService.getService();
		EngineDescription ed = scriptService.getEngineByID(language.getId());

		IReplEngine engine = (IReplEngine) ed.createEngine();

		engine.setTerminateOnIdle(false);
		engine.schedule();

		// Run bootstrap scripts according to language.
		runBootstrap(engine, language);

		engines.put(language, engine);

		return engine;
	}

	private static void runBootstrap(IReplEngine engine, EASELanguage language) {
		Bundle bundle = Platform.getBundle("org.eclipse.papyrus.moka.ease");
		//URI uri = new URI("platform:/plugin/org.eclipse.papyrus.moka.parametric.ease/"+ language.getBootstrapPath());
		URL entryUrl = bundle.getEntry(language.getBootstrapPath());
		Script script = new Script("EASEBootstrapScript", entryUrl, false);

		ScriptResult result = engine.execute(script);
		try {
			// TODO we should check if we can do it asynchronously without wait for the result
			result.get(); // Wait for result
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}
}
