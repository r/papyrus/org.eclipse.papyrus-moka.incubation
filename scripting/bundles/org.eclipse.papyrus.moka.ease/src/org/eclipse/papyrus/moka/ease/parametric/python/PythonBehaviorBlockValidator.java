/*****************************************************************************
 * Copyright (c) 2018 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.ease.parametric.python;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.papyrus.aceeditor.CodeAnnotation;
import org.eclipse.papyrus.sysml16.constraintblocks.ConstraintBlock;

public class PythonBehaviorBlockValidator {
	
	
	public static List<CodeAnnotation> validate(String function, ConstraintBlock block){
		
		
		ArrayList<CodeAnnotation> annotations = new ArrayList<>();

		// FIXME This code has been comment to avoid the dependency to pydev (no valid CQ)

		// if (function == null || function.trim().equals(""))
		// return annotations;
		//
		//
		// PythonBehaviorFunctionInfo info = PythonBehaviorHelper.buildFunctionInfo(function);
		//
		// // Check function name
		// if (isFunctionNameValid(info))
		// if (info.getName() == null) {
		// annotations.add(new CodeAnnotation(info.getCodeLineDefinition(), 0, "The script should have a function called: '" + ScriptBehaviorConstants.SCRIPT_FUNCTION_NAME + "'", CodeAnnotationType.Error));
		// }
		//
		// if (info.getBlockParamName() == null) {
		// annotations.add(new CodeAnnotation(info.getCodeLineDefinition(), 0, "The function '" + ScriptBehaviorConstants.SCRIPT_FUNCTION_NAME + "' should have exactly one parameter", CodeAnnotationType.Error));
		// }
		//
		// if (!ScriptBehaviorConstants.SCRIPT_FUNCTION_PARAM_NAME.equals(info.getBlockParamName())) {
		// annotations.add(new CodeAnnotation(info.getCodeLineDefinition(), 0, "The parameter of the function '" + ScriptBehaviorConstants.SCRIPT_FUNCTION_NAME + "' must be called '" + ScriptBehaviorConstants.SCRIPT_FUNCTION_PARAM_NAME + "'",
		// CodeAnnotationType.Error));
		// }
		//
		// Set<String> outputs = new HashSet<>();
		// Set<String> inputs = new HashSet<>();
		//
		// for (Property p : block.getParameters()) {
		// if (p.isDerived())
		// outputs.add(p.getName());
		// else
		// inputs.add(p.getName());
		// }
		//
		// Set<String> used = new HashSet<>();
		//
		// for (PythonAccess pa : info.getParamAccesses()) {
		//
		// boolean isDerived = outputs.contains(pa.getName());
		//
		// boolean isNotDerived = inputs.contains(pa.getName());
		//
		// if (pa.isRead() && isDerived) {
		// annotations.add(new CodeAnnotation(pa.getLine(), pa.getColumn(), "Reading derived property: '" + pa.getName() + "'", CodeAnnotationType.Warning));
		// }
		// if (!pa.isRead() && isNotDerived) {
		// annotations.add(new CodeAnnotation(pa.getLine(), pa.getColumn(), "Writing to non derived property: '" + pa.getName() + "'", CodeAnnotationType.Warning));
		// }
		//
		// if (!isDerived && !isNotDerived) {
		// if (pa.isRead())
		// annotations.add(new CodeAnnotation(pa.getLine(), pa.getColumn(), "Reading non-existing property: '" + pa.getName() + "'", CodeAnnotationType.Error));
		// else
		// annotations.add(new CodeAnnotation(pa.getLine(), pa.getColumn(), "Writing to non-existing property: '" + pa.getName() + "'", CodeAnnotationType.Warning));
		// }
		//
		// used.add(pa.getName());
		// }
		//
		//
		// for (String input : inputs) {
		// if (!used.contains(input)) {
		// annotations.add(new CodeAnnotation(info.getCodeLineDefinition(), 0, "Property '" + input + "' not used in the behavior", CodeAnnotationType.Warning));
		// }
		// }
		//
		// for (String output : outputs) {
		// if (!used.contains(output)) {
		// annotations.add(new CodeAnnotation(info.getCodeLineDefinition(), 0, "Not setting the value of '" + output + "'", CodeAnnotationType.Warning));
		// }
		// }
		//
		//
		// PythonError error = info.getParserError();
		// if (error != null) {
		// annotations.add(new CodeAnnotation(error.getLine(), error.getColumn(), error.getFirstErrorLine(), CodeAnnotationType.Error));
		// }
		
		return annotations;
	}
	
	@SuppressWarnings("unused")
	private static boolean isFunctionNameValid(PythonBehaviorFunctionInfo info) {
		return info.getName() != null;
		
	}
	
}
