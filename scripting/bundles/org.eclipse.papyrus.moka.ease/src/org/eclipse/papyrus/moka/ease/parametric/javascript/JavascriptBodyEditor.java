/*****************************************************************************
 * Copyright (c) 2018 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   David L�pez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.ease.parametric.javascript;

import org.eclipse.papyrus.aceeditor.AceBodyEditor;
import org.eclipse.papyrus.aceeditor.AceCodeEditorConfiguration;
import org.eclipse.swt.widgets.Composite;

public class JavascriptBodyEditor extends AceBodyEditor{

	@Override
	public void createWidget(Composite parent, int style) {
		super.createWidget(parent, style);
		
		
		AceCodeEditorConfiguration config = getEditorConfiguration();
		config.setLanguage("javascript");
		config.setTheme("eclipse");
		refreshEditor();
	}
	
}
