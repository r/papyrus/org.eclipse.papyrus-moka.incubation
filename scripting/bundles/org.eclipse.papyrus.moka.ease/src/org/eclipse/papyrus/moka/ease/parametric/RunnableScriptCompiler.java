/*****************************************************************************
 * Copyright (c) 2018 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.ease.parametric;

import org.eclipse.ease.IScriptEngine;

public abstract class RunnableScriptCompiler {
	
	protected IScriptEngine engine;
	
	public RunnableScriptCompiler(IScriptEngine engine) {
		this.engine = engine;
	}
	
	public abstract RunnableScript compileRunnableScript(EASEConstraintObject obj,  Object objProxy, String scriptBody);
}
