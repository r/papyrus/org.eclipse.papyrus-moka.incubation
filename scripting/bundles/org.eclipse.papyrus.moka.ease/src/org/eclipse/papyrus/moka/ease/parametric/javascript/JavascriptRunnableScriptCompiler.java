/*****************************************************************************
 * Copyright (c) 2018 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.ease.parametric.javascript;

import java.util.concurrent.ExecutionException;

import org.eclipse.ease.IScriptEngine;
import org.eclipse.ease.Script;
import org.eclipse.ease.ScriptResult;
import org.eclipse.papyrus.moka.ease.parametric.EASEConstraintObject;
import org.eclipse.papyrus.moka.ease.parametric.RunnableScript;
import org.eclipse.papyrus.moka.ease.parametric.RunnableScriptCompiler;

public class JavascriptRunnableScriptCompiler extends RunnableScriptCompiler {

	public JavascriptRunnableScriptCompiler(IScriptEngine engine) {
		super(engine);
	}

	public String generateGlueCode(String scriptBody) {
		String code = "var RunnableScript =  Java.type(\"org.eclipse.papyrus.moka.ease.parametric.RunnableScript\");\r\n" + 
				"var JSRunnableScript = Java.extend(RunnableScript);\r\n" + 
				"\r\n" + 
				scriptBody + "\r\n" + 
				"\r\n" + 
				"new JSRunnableScript() {\r\n" + 
				"	block : paramObject,\r\n" + 
				"	\r\n" + 
				"    evaluate: function() {\r\n" + 
				"		run(this.block);\r\n" + 
				"    }\r\n" + 
				"};";
		return code;
	}

	@Override
	public RunnableScript compileRunnableScript(EASEConstraintObject obj, Object objProxy, String scriptBody) {
		engine.setVariable("paramObject", objProxy);
		String body = generateGlueCode(scriptBody);
		try {
			return (RunnableScript)engine.inject(new Script(body), false);
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return null;
	}

}
