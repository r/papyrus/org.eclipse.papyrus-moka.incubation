/*****************************************************************************
 * Copyright (c) 2018 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   David L�pez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.ease.parametric.python;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.eclipse.papyrus.aceeditor.AceCodeEditorConfiguration;
import org.eclipse.papyrus.aceeditor.CodeAnnotation;
import org.eclipse.papyrus.moka.ease.parametric.bodyeditor.AceConstraintBlockBodyEditor;
import org.eclipse.swt.widgets.Composite;

public class PythonBodyEditor extends AceConstraintBlockBodyEditor {
	
	//private int delay = 200;
	private Timer timer;
	private TimerTask currentValidateTask;
	
	@Override
	public void createWidget(Composite parent, int style) {
		super.createWidget(parent, style);
		AceCodeEditorConfiguration config = getEditorConfiguration();
		config.setLanguage("python");
		config.setTheme("eclipse");
		refreshEditor();
		
		timer = new Timer(true);
		
		scheduleValidation();
	}

	@Override
	public void setInput(String value) {
		super.setInput(value);
		//scheduleValidation(value);
	}

	private void scheduleValidation(/*String value*/) {
		
		//cancelCurrentValidateTask();
		
		currentValidateTask = new TimerTask() {

			@Override
			public void run() {
				editor.runInFXThread(new Runnable() {
					
					@Override
					public void run() {				
						//String value = PythonBodyEditor.this.getValue();
						String value = (String) editor.getValue();
						runValidation(value);
					}
				});
			}
		};
		
		timer.scheduleAtFixedRate(currentValidateTask, 200, 1000);
	}

	private void runValidation(String value) {
		//System.out.println("Validation found " + annotations.size() + " markers.");
		//System.out.println(value);
		List<CodeAnnotation> annotations = PythonBehaviorBlockValidator.validate(value, PythonBodyEditor.this.blockFromContext(context));
		editor.setAnnotations(annotations);
	}
	
	
	@Override
	protected void onCommiting(String newValue) {
		editor.markClean();
		//runValidation(newValue);
		//scheduleValidation(newValue);
	}

	@Override
	public void onChange(String newValue) {
		//scheduleValidation(newValue);
	}
	
	private void cancelCurrentValidateTask() {
		if( currentValidateTask != null ) {
			currentValidateTask.cancel();
			currentValidateTask = null;
		}
		
	}
	
	@Override
	public void dispose() {
		cancelCurrentValidateTask();
		
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
		super.dispose();
	}
	
}
