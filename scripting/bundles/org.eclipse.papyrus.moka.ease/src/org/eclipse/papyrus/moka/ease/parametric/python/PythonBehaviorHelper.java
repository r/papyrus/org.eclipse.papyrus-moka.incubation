/*****************************************************************************
 * Copyright (c) 2020 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.ease.parametric.python;

public class PythonBehaviorHelper {
	
	public static PythonBehaviorFunctionInfo buildFunctionInfo(String pythonCode) {
		final PythonBehaviorFunctionInfo info = new PythonBehaviorFunctionInfo();
		
		// FIXME This code has been comment to avoid the dependency to pydev (no valid CQ)

		// //Tuple<SimpleNode, IGrammar> parseResult = null;
		//
		// ParseOutput parseOut = PyParser.reparseDocument(new ParserInfo(new Document(pythonCode), IGrammarVersionProvider.LATEST_GRAMMAR_PY3_VERSION, true, null));
		//// try {
		////
		////
		////// parseResult = PyParser.reparseDocumentInternal(new Document(pythonCode), true,
		////// IGrammarVersionProvider.LATEST_GRAMMAR_VERSION);
		//// } catch (ParseException e) {
		//// e.printStackTrace();
		//// }
		//
		// SimpleNode node = (SimpleNode)parseOut.ast;
		//
		//
		// //System.out.println(parseOut.error);
		//
		// if( parseOut.error != null ) {
		//
		//
		// if( parseOut.error instanceof ParseException ) {
		// ParseException ex = (ParseException)parseOut.error;
		//
		// if( ex.currentToken != null )
		// info.setParserError(new PythonError(ex, ex.currentToken.beginLine, ex.currentToken.beginColumn));
		// }
		//
		// if( parseOut.error instanceof TokenMgrError) {
		// TokenMgrError err = (TokenMgrError)parseOut.error;
		//
		// info.setParserError(new PythonError(err, err.errorLine, err.errorColumn));
		// }
		// }
		//
		// if( node == null )
		// return info;
		//
//		try {
//			
		// node.accept(new VisitorBase() {
		//
		// @Override
		// protected Object unhandled_node(SimpleNode node) throws Exception {
		// return null;
		// }
		//
		// @Override
		// public void traverse(SimpleNode node) throws Exception {
		// node.traverse(this);
		// }
		//
		// @Override
		// public Object visitFunctionDef(FunctionDef fDef) throws Exception {
		// String fName = ((NameTok)fDef.name).id;
		//
		// if( !fName.equals(ScriptBehaviorConstants.SCRIPT_FUNCTION_NAME) )
		// return super.visitFunctionDef(fDef);
		//
		// //Function name
		// info.setName(fName);
		// info.setCodeLineDefinition(fDef.beginLine);
		//
		// if( fDef.args.args.length != 1 ) {
		// return super.visitFunctionDef(fDef);
		// }
		//
		// Name blockParam = (Name)fDef.args.args[0];
		// info.setBlockParamName(blockParam.id);
		//
		// //Fill the blockParam uses
		// findParamAccess(info, fDef);
		//
		// return super.visitFunctionDef(fDef);
		// }
		//
		// });
//			
		// } catch (Exception e) {
//			e.printStackTrace();
//		}
		
		return info;
	}
	
	
}
