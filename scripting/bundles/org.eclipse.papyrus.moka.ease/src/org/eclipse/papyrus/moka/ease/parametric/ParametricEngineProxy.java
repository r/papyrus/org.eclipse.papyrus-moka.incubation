/*****************************************************************************
 * Copyright (c) 2020 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.ease.parametric;

import java.util.List;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.papyrus.moka.ease.semantics.proxy.LoggedValue;
import org.eclipse.papyrus.moka.ease.semantics.proxy.MapProxy;
import org.eclipse.papyrus.moka.ease.semantics.proxy.ParametricInstanceSpecGenerator;
import org.eclipse.papyrus.moka.ease.semantics.proxy.ScriptProxy;
import org.eclipse.papyrus.moka.fuml.structuredclassifiers.IObject_;
import org.eclipse.papyrus.moka.kernel.engine.EngineConfiguration;
import org.eclipse.papyrus.moka.kernel.engine.ExecutionEngineException;
import org.eclipse.papyrus.moka.kernel.service.ServiceOperatingMode;
import org.eclipse.papyrus.moka.parametric.CausalParametricEngine;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.UMLFactory;

public class ParametricEngineProxy {
	private InstanceSpecification executionEntryPoint;
	private DeferredParametricEvaluator evaluator;
	private CausalParametricEngine engine;
	private EngineConfiguration<InstanceSpecification> engineConfiguration;
	private MapProxy proxyObject;
	
	public ParametricEngineProxy() {
		evaluator = new DeferredParametricEvaluator();
		engine = new CausalParametricEngine(evaluator, new NullPostParametricEvaluationTask());
	}
	
	private void init(Element source) {
		engineConfiguration = new EngineConfiguration<>();
		
		if(source instanceof Class) {
			ParametricInstanceSpecGenerator generator = new ParametricInstanceSpecGenerator(
					UMLFactory.eINSTANCE.createPackage(), false);
			executionEntryPoint = generator.generateInstanceSpecification((Class)source);
		}else if( source instanceof InstanceSpecification ) {
			executionEntryPoint = (InstanceSpecification) source;
		} else {
			//Element is not executable...
		}
		
		engineConfiguration.setExecutionSource(executionEntryPoint);
		engineConfiguration.setMode(ServiceOperatingMode.QUIET);
		engineConfiguration.setModelURI(source.getModel().eResource().getURI());
//		engineConfiguration.setProject(null);
		
		//Run it !
		try {
			engine.run(engineConfiguration, SubMonitor.convert(new NullProgressMonitor()));
		} catch (ExecutionEngineException e) {
			e.printStackTrace();
		}
		this.proxyObject = (MapProxy) ScriptProxy.getKernelAdapter().getProxyFor(evaluator.getParametricObject());
	}
	
	public Object initEngine(Element source) {
		this.init(source);
		return ScriptProxy.getKernelAdapter().getProxyFor (evaluator.getParametricObject());
	}
	
	/**
	 * Enables the recording of a given variable in the locus
	 * @param variablePath : the "." separated feature path to the variable to log
	 * @param alias : an alias for the variable name 
	 */
	public void log(String variablePath, String alias) {
		evaluator.getLogger().log(variablePath, alias, proxyObject);
	}
	
	/**
	 * Enables the recording of a given variable in the locus, using the variable path as default alias
	 * @param variablePath : the "." separated feature path to the variable to log
	 */
	public void log(String variablePath) {
		log(variablePath, variablePath);
	}
	
	/**
	 * 
	 * @return the log containing logged values
	 */
	public List<LoggedValue> getLog() {
		return evaluator.getLogger().getLog();
	}

	/**
	 * Single step evaluation
	 * @return the root Object of the locus
	 */
	public IObject_ evaluate() {
		return evaluator.evaluate();
	}

	/**
	 * Multiple steps evaluation
	 * @return the root Object of the locus
	 */
	public IObject_ evaluate(int steps) {
		return evaluator.evaluate(steps);
	}
	
	/**
	 * Multiple steps evaluation
	 * @return the root Object of the locus
	 */
	public IObject_ evaluate(double startTime, double endTime, double dt) {
		return evaluator.evaluate(startTime, endTime, dt);
	}
}
