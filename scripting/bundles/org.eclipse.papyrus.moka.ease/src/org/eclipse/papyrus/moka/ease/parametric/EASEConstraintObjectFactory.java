/*****************************************************************************
 * Copyright (c) 2018 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   David L�pez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/

package org.eclipse.papyrus.moka.ease.parametric;

import org.eclipse.papyrus.moka.ease.parametric.EASEEngineManager.EASELanguage;
import org.eclipse.papyrus.moka.parametric.semantics.ConstraintObject;
import org.eclipse.papyrus.moka.parametric.utils.IConstraintObjectFactory;
import org.eclipse.papyrus.moka.parametric.utils.NameUtils;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.OpaqueExpression;

public class EASEConstraintObjectFactory implements IConstraintObjectFactory {
	
	@Override
	public boolean canInstantiateConstraintObject(Class type) {
		// Checks if the there is an EASE Language script inside the ConstraintBlock
		if (type.getAppliedStereotype(NameUtils.SYSML_CONSTRAINTBLOCK_STEREOTYPE_NAME) != null) {
			for (Constraint c : type.getOwnedRules()) {
				if (c.getSpecification() instanceof OpaqueExpression) {
					OpaqueExpression o = (OpaqueExpression)c.getSpecification() ;
					if (o.getLanguages().size() == 1 && isSupportedEASELanguage(o.getLanguages().get(0))) {
						if (!o.getBodies().isEmpty() && !(o.getBodies().get(0)==null) && ! "".equals(o.getBodies().get(0))) { //$NON-NLS-1$
							return true ;
						}
					}
				}
			}
		}
		return false ;
	}

	private boolean isSupportedEASELanguage(String lang) {
		return EASELanguage.fromSimpleName(lang) != null;
	}
	
	@Override
	public ConstraintObject instantiate(Class type) {
		return new EASEConstraintObject();
	}

}
