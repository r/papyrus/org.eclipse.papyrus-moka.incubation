
include("platform:/plugin/org.eclipse.papyrus.ease/scripts/python/papyrusutils.py")
include("platform:/plugin/org.eclipse.papyrus.moka.ease/scripts/python/bootstrap.py")    

loadModule("/Modeling/MokaParametric")
loadModule("/Modeling/PapyrusUtils")
import pandas
import numpy as np

def wrap(obj):
    return PythonWrapper(obj)
  
  
def unwrap(obj):
    return  obj.unwrap()  
    
    
def get_dataframe(engine):
    
    log = engine.getLog()
    df = pandas.DataFrame()
    
    for value in log:
        #optimized byte-buffer-based java to python transfer for Double and Integer values
        if (value.getType()== 'Double') :
            array = np.frombuffer(value.getByteArray(), dtype=np.float64)
            series =pandas.Series(data=array) 
        
        elif  (value.getType()== 'Integer') :
            array = np.frombuffer(value.getByteArray(), dtype=np.int32)
            series =pandas.Series(data=array) 
            
        else :
            #default transfer copying values, much slower
            series =pandas.Series(data=list(value.getValues())) 
        df[value.getAlias()] = series
            
    return df


def init_parametric_engine(simulation_entrypoint) :
    #we get the parametric engine instance
    engine = getParametricEngine()
    return (engine, wrap(engine.initEngine(simulation_entrypoint)))