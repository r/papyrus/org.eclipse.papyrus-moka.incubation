include ('platform:/plugin/org.eclipse.papyrus.ease/scripts/python/papyrusutils.py')

class PythonListWrapper:    
    def __init__(self, list):
        self.list = list;
        self.i = 0;
        self.n = list.size();
    
    def __getitem__(self, key):        
        obj = self.list[key];        
        return self.wrap(self.list[key]);
    
    def __setitem__(self, key, val):
        self.list[key] = val;
        
    def __iter__(self):        
        return self;
    
    def __len__(self):        
        return self.n;
    
    def __next__(self):        
        if self.i < self.n:            
            i = self.i
            self.i += 1
            return self.wrap(self.list.get(i))
        else:
            raise StopIteration()
    
    next = __next__  # Alias for supporting Python 2
    
    def wrap(self, value):        
        return PythonWrapper(value);

def getListClass():
     if is_jython() :
        
         return  org.eclipse.papyrus.moka.ease.semantics.proxy.ValueListProxy
     else:
        return py4j.java_collections.JavaList


def getMapClass():
    
     if is_jython() :
         return org.eclipse.papyrus.moka.ease.semantics.proxy.MapProxy
     else:
        return py4j.java_collections.JavaMap


class PythonWrapper:
    def __init__(self, block):
        self.block = block;
        
    def __getattr__(self, name):        
        value = self.block[name];
       
        if isinstance(value, PythonWrapper):
            return value.block[name];
        
        if isinstance(value, getListClass()):            
            return PythonListWrapper(value);
        
        if isinstance(value, getMapClass()):
            return PythonWrapper(value);
        
        return value;

    def __setattr__(self, name, value):
        if( name == "block" ):
            self.__dict__["block"] = value;
        else :
            self.__dict__["block"][name] = value;
            
            
    def __str__(self):
        return self.block.toString()
    
    def unwrap(self):
        return self.__dict__["block"] 
            
#print("Python Engine started.")
