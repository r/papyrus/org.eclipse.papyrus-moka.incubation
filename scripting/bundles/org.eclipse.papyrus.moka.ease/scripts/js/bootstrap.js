function isHashmap(obj){
	return ( "class java.util.HashMap" == obj.class );
}

//Closure
function makePropertyAccessor(proxy, name){
	return {
		get: function(){
			var value = proxy.get(name);
			
			if( !isHashmap(value) )
				return value;
			
			return new MyProxy(value);		
		}
	}
}

function MyProxy(obj){
	this.obj = obj;
	var me = this;
	var names = obj.keySet();
	var iter = names.iterator();
	
	while( iter.hasNext() ){
		var name = iter.next();
		Object.defineProperty(me, name, makePropertyAccessor(me.obj, name));
	}
}