/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.suml.ease;

import java.net.URI;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import org.eclipse.ease.AbstractScriptEngine;
import org.eclipse.ease.IReplEngine;
import org.eclipse.ease.Script;
import org.eclipse.ease.ScriptResult;
import org.eclipse.ease.service.EngineDescription;
import org.eclipse.ease.service.IScriptService;
import org.eclipse.ease.service.ScriptService;
import org.eclipse.papyrus.moka.engine.suml.script.IBodyScriptFactory;

public abstract class EASEBodyScriptFactory implements IBodyScriptFactory {
	private String languageId;
	private IReplEngine engine;
	protected URI modelURI;

	public EASEBodyScriptFactory(String languageId) {
		this.languageId = languageId;
	}

	public IReplEngine getEngine() {
		if (engine == null || engine.isFinished()) {
			engine = createEngine();
			startEngine(engine);
		}

		return engine;
	}

	abstract protected URL getBootstrap();

	protected IReplEngine createEngine() {
		final IScriptService scriptService = ScriptService.getService();
		EngineDescription ed = scriptService.getEngineByID(languageId);
		IReplEngine eng = (IReplEngine) ed.createEngine();

		if (eng instanceof AbstractScriptEngine) {
			((AbstractScriptEngine) eng).setExecutionRootFile(this.modelURI);
		}

		return eng;
	}

	protected void startEngine(IReplEngine engine) {
		engine.setTerminateOnIdle(false);
		engine.schedule();

		URL bootstrapEntry = getBootstrap();
		if (bootstrapEntry != null)
			runBootstrap(engine, bootstrapEntry);

	}

	protected void runBootstrap(IReplEngine engine, URL bootstrapEntry) {
		Script script = new Script("EASEBootstrapScript", bootstrapEntry, false);
		ScriptResult result = engine.execute(script);
		try {
			// TODO check if we can do it asynchronously without wait for result
			result.get(); // wait for the result
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void configure(URI modelURI) {
		this.modelURI = modelURI;
	}

	@Override
	public void terminate() {
		getEngine().terminate();
	}

}
