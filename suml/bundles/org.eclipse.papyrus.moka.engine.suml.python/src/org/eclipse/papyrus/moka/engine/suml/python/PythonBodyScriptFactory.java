/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.suml.python;

import java.net.URL;
import java.util.concurrent.ExecutionException;

import org.eclipse.core.runtime.Platform;
import org.eclipse.ease.IReplEngine;
import org.eclipse.ease.Script;
import org.eclipse.papyrus.moka.engine.suml.ease.EASEBodyScriptFactory;
import org.eclipse.papyrus.moka.engine.suml.script.IBodyScript;
import org.osgi.framework.Bundle;

public class PythonBodyScriptFactory extends EASEBodyScriptFactory {
	
	private static final String PY4J_LANG_ID = "org.eclipse.ease.lang.python.py4j.engine";
								//JYTHON_LANG_ID = "org.eclipse.ease.python.jython";
	
	public PythonBodyScriptFactory() {
		super(PY4J_LANG_ID);
	}
	
	@Override
	protected IReplEngine createEngine() {
		IReplEngine engine = super.createEngine();
		return new PythonBridgeEngine(engine);
	}

	@Override
	protected URL getBootstrap() {
		Bundle bundle = Platform.getBundle("org.eclipse.papyrus.moka.engine.suml.python");
		return bundle.getEntry("scripts/Bootstrap.py");
	}
	
	@Override
	public IBodyScript buildScript(Object ctx, String body) {
		String className = "MOKA_SUML_IBodyScript";
		String scriptBody = PythonBodyScriptCodeGen.generateInterface(className, body);
		PythonBridgeEngine engine = (PythonBridgeEngine) getEngine();
		try {
			engine.inject(new Script(scriptBody), false);
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return engine.instanceScriptObject(className);
	}
	
}
