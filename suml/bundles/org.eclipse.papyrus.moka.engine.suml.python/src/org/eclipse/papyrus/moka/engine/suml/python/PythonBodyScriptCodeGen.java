/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.suml.python;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

import org.eclipse.papyrus.moka.engine.suml.script.gen.CodeGenHelper;

public class PythonBodyScriptCodeGen {
	
	protected static final String SPLIT_KEYWORD = "%%%%%%SPLIT%%%%%%";
	
	public static String generateInterface(String className, String body) {
		
		String scriptBody = new String(body);
		
		scriptBody = CodeGenHelper.clearTabs(scriptBody);
		scriptBody = scriptBody.replaceAll("(?m)^def.*run.*", SPLIT_KEYWORD+"\n");
		
		BufferedReader lineReader = new BufferedReader(new StringReader(scriptBody));
		StringBuilder bodyBuilder = new StringBuilder();
		
		
		String bodyLine;
		try {
			bodyLine = lineReader.readLine()+'\n';
			
			while (bodyLine != null && !bodyLine.startsWith((SPLIT_KEYWORD))){
				bodyBuilder.append(bodyLine).append('\n');
				bodyLine = lineReader.readLine();
			}
			
			bodyBuilder.append("class "+className+"(object):\n");
			bodyBuilder.append(CodeGenHelper.getTab(1)+"def run(self, ctx):");
			
			while ((bodyLine = lineReader.readLine())!= null) {				
				bodyBuilder.append(CodeGenHelper.getTab(1)+bodyLine+"\n");			
			}
			bodyBuilder.append(CodeGenHelper.getTab(1)+"def evaluate(self, jctx):\n");
			bodyBuilder.append(CodeGenHelper.getTab(2)+"ctx = wrap(jctx);\n");
			bodyBuilder.append(CodeGenHelper.getTab(2)+"ctx.locus = ReflectiveModule(jctx[\"jlocus\"]);\n");
			bodyBuilder.append(CodeGenHelper.getTab(2)+"return unwrap(self.run(ctx));\n");

			bodyBuilder.append(CodeGenHelper.getTab(1)+"class Java:\n");
			bodyBuilder.append(CodeGenHelper.getTab(2)+"implements = ['org.eclipse.papyrus.moka.engine.suml.script.IBodyScript']\n");
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bodyBuilder.toString();
	}
}
