/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.suml.python;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.ease.IExecutionListener;
import org.eclipse.ease.IReplEngine;
import org.eclipse.ease.ISecurityCheck;
import org.eclipse.ease.ISecurityCheck.ActionType;
import org.eclipse.ease.Script;
import org.eclipse.ease.ScriptObjectType;
import org.eclipse.ease.ScriptResult;
import org.eclipse.ease.debugging.model.EaseDebugVariable;
import org.eclipse.ease.service.EngineDescription;

public class PythonBridgeEngine implements IReplEngine {

	private static final String BRIDGE_OBJECT = "___bridge_python_ease___";
	private IReplEngine pythonEngine;
	private PythonObjectBridge<Object> bridgeObject = new PythonObjectBridge<Object>();

	public PythonBridgeEngine(IReplEngine pythonEngine) {
		this.pythonEngine = pythonEngine;
	}

	private void injectBridge() {
		this.pythonEngine.setVariable(BRIDGE_OBJECT, bridgeObject);
	}

	public <T> T evalWithResult(String snippet) {

		String scriptBody = BRIDGE_OBJECT + ".push(" + snippet + ");";
		try {
			pythonEngine.inject(new Script(scriptBody), false);
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return (T) this.bridgeObject.pull();
	}

	public <T> T instanceScriptObject(String className) {
		return evalWithResult(className + "()");
	}

	@Override
	public void setTerminateOnIdle(boolean terminate) {
		pythonEngine.setTerminateOnIdle(terminate);
	}

	@Override
	public boolean getTerminateOnIdle() {
		return pythonEngine.getTerminateOnIdle();
	}

	@Override
	public Collection<EaseDebugVariable> getDefinedVariables() {
		return pythonEngine.getDefinedVariables();
	}

	@Override
	public ScriptObjectType getType(Object object) {
		return pythonEngine.getType(object);
	}

	@Override
	public String toString(Object object) {
		return pythonEngine.toString(object);
	}

	@Override
	public EaseDebugVariable getLastExecutionResult() {
		return pythonEngine.getLastExecutionResult();
	}

	@Override
	public ScriptResult execute(Object content) {
		return pythonEngine.execute(content);
	}

	@Override
	public Object inject(Object content, boolean uiThread) throws ExecutionException {
		return pythonEngine.inject(content, uiThread);
	}

	@Override
	public Object getExecutedFile() {
		return pythonEngine.getExecutedFile();
	}

	@Override
	public void setOutputStream(OutputStream outputStream) {
		pythonEngine.setOutputStream(outputStream);
	}

	@Override
	public void setErrorStream(OutputStream errorStream) {
		pythonEngine.setErrorStream(errorStream);
	}

	@Override
	public void setInputStream(InputStream inputStream) {
		pythonEngine.setInputStream(inputStream);
	}

	@Override
	public PrintStream getOutputStream() {
		return pythonEngine.getOutputStream();
	}

	@Override
	public PrintStream getErrorStream() {
		return pythonEngine.getErrorStream();
	}

	@Override
	public InputStream getInputStream() {
		return pythonEngine.getInputStream();
	}

	@Override
	public void setCloseStreamsOnTerminate(boolean closeStreams) {
		pythonEngine.setCloseStreamsOnTerminate(closeStreams);
	}

	@Override
	public void schedule() {
		pythonEngine.schedule();
		injectBridge();
	}

	@Override
	public void terminate() {
		pythonEngine.terminate();
	}

	@Override
	public void terminateCurrent() {
		pythonEngine.terminateCurrent();
	}

	@Override
	public void addExecutionListener(IExecutionListener listener) {
		pythonEngine.addExecutionListener(listener);
	}

	@Override
	public void removeExecutionListener(IExecutionListener listener) {
		pythonEngine.removeExecutionListener(listener);
	}

	@Override
	public String getName() {
		return pythonEngine.getName();
	}

	@Override
	public void setVariable(String name, Object content) {
		pythonEngine.setVariable(name, content);
	}

	@Override
	public Object getVariable(String name) {
		return pythonEngine.getVariable(name);
	}

	@Override
	public boolean hasVariable(String name) {
		return pythonEngine.hasVariable(name);
	}

	@Override
	public EngineDescription getDescription() {
		return pythonEngine.getDescription();
	}

	@Override
	public Map<String, Object> getVariables() {
		return pythonEngine.getVariables();
	}

	@Override
	public void registerJar(URL url) {
		pythonEngine.registerJar(url);
	}

	@Override
	public boolean isFinished() {
		return pythonEngine.isFinished();
	}

	@Override
	public void addSecurityCheck(ActionType type, ISecurityCheck check) {
		pythonEngine.addSecurityCheck(type, check);
	}

	@Override
	public void removeSecurityCheck(ISecurityCheck check) {
		pythonEngine.removeSecurityCheck(check);
	}

	@Override
	public ILaunch getLaunch() {
		return pythonEngine.getLaunch();
	}

	@Override
	public IProgressMonitor getMonitor() {
		return pythonEngine.getMonitor();
	}

	@Override
	public void joinEngine(long timeout) throws InterruptedException {
		pythonEngine.joinEngine(timeout);
	}

	@Override
	public void joinEngine() throws InterruptedException {
		pythonEngine.joinEngine();
	}

}
