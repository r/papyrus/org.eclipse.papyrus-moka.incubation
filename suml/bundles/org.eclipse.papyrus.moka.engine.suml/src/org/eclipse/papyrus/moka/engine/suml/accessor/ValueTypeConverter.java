/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.suml.accessor;

import java.util.List;

import org.eclipse.uml2.uml.Type;

public interface ValueTypeConverter<ValueType> {
	
	public abstract boolean isComposite(ValueType value);
	
	public abstract <T> List<T> getComponents(ValueType value);
	
	public abstract Object getRawValue(ValueType value);
	
	public abstract void setRawValue(ValueType value, Object obj);
	
	public abstract boolean isValueType(Object obj);
	
	public abstract ValueType newPrimitiveValue(Type type, Object obj);
	
	public abstract ValueType newValue(Object obj);
	
	public abstract boolean isPrimitive(Type type);
	
	public abstract Class<ValueType> getValueTypeClass();
	
}
