package org.eclipse.papyrus.moka.engine.suml.accessor.locus;

import java.util.List;
import java.util.Map;

import org.eclipse.papyrus.moka.engine.suml.accessor.ValueTypeConverter;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.BooleanValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IBooleanValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IEnumerationValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IIntegerValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IRealValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IStringValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IStructuredValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IUnlimitedNaturalValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IntegerValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.PrimitiveValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.RealValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.StringValue;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Type;

public class IValueConverter implements ValueTypeConverter<IValue>{

	@Override
	public Object getRawValue(IValue value) {
		
		if (value == null)
			return null;
		
		if( value instanceof IBooleanValue ) 
			return ((IBooleanValue)value).getValue();
		
		if( value instanceof IIntegerValue ) 
			return ((IIntegerValue)value).getValue();
		
		if( value instanceof IRealValue ) 
			return ((IRealValue)value).getValue();
		
		if( value instanceof IStringValue ) 
			return ((IStringValue)value).getValue();
		
		if( value instanceof IUnlimitedNaturalValue) 
			return ((IUnlimitedNaturalValue)value).getValue();
		
		if( value instanceof IEnumerationValue ) 
			return value;
		
		//Not supported, 
		throw new RuntimeException("Proxies for " + value.getClass() + " are not supported yet.");
	}
	

	@Override
	public boolean isComposite(IValue value) {
		return value instanceof IStructuredValue;
	}
	
	@Override
	public <T> List<T> getComponents(IValue val) {
		if( val instanceof IStructuredValue )
			return (List<T>) ((IStructuredValue)val).getFeatureValues();
		
		return null;
	}

	@Override
	public IValue newValue(Object obj) {
		if( obj == null )
			return null;
		
		if( obj instanceof List ) {
			throw new RuntimeException("Lists are supported yet");
		}
		
		if( obj instanceof Map ) {
			throw new RuntimeException("Maps are supported yet");
		}
		
		
		if( obj instanceof Integer ) {
			IntegerValue v = new IntegerValue();			
			v.setValue((Integer)obj);
			return v;
		}
		
		if( obj instanceof Double ) {
			RealValue v = new RealValue();
			v.setValue((Double)obj);
			return v;
		}

		if( obj instanceof Boolean ) {
			BooleanValue v = new BooleanValue();
			v.setValue((Boolean)obj);
			return v;
		}
		
		if( obj instanceof String ) {
			StringValue v = new StringValue();
			v.setValue((String)obj);
			return v;
		}
		
		throw new RuntimeException(obj + " can't be transformed to an IValue.");
	}

	private PrimitiveValue primitiveFromType(Type type, Object obj) {
		switch( type.getName() ) {
			case "Real":
				RealValue r = new RealValue();
				
				if ( !(obj instanceof Number ) )
					throw new RuntimeException("Can't assign " + obj + " to type " + type);
				
				r.setValue( ((Number)obj).doubleValue() );
				
				return r;
			case "Integer":
				IntegerValue i = new IntegerValue();
				
				if ( !(obj instanceof Number ) )
					throw new RuntimeException("Can't assign " + obj + " to type " + type);
				
				i.setValue( ((Number)obj).intValue() );
				
				return i;
			case "Boolean":
				BooleanValue b = new BooleanValue();
				
				if ( !(obj instanceof Boolean ) )
					throw new RuntimeException("Can't assign " + obj + " to type " + type);
				
				b.setValue((Boolean)obj);
				
				return b;
			case "String":
				StringValue s = new StringValue();
				s.setValue(String.valueOf(obj));
				return s;
			default: 
				//Not supported primitive?
				return null;
		}
	}

	@Override
	public IValue newPrimitiveValue(Type type, Object obj) {
		
		if( obj == null )
			return null;
		
		if (! isPrimitive(type) )
			return null;
		
		
		PrimitiveValue value = primitiveFromType(type, obj);
		value.setType((PrimitiveType) type);
		return value;
	}

	@Override
	public boolean isValueType(Object obj) {
		return obj instanceof IValue;
	}


	@Override
	public void setRawValue(IValue value, Object obj) {
		Type type = value.getTypes().get(0);
		if (! isPrimitive(type) )
			throw new RuntimeException("Couldn't assign " + obj + " to type: " + type);
		
		switch( type.getName() ) {
			case "Real":
				Number nr = (Number)obj;
				( (RealValue) value) .setValue( nr.doubleValue() );
				break;
			case "Integer":
				Number ni = (Number)obj;
				( (IntegerValue) value) .setValue( ni.intValue() );
				break;
			case "Boolean":
				( (BooleanValue) value) .setValue( (Boolean) obj );
				break;
			case "String":
				( (StringValue) value) .setValue( (String) obj );
				break;
			default: 
				throw new RuntimeException("Not supported type: " + type);
		}
		
	}


	@Override
	public boolean isPrimitive(Type type) {
		return (type instanceof PrimitiveType);
	}

	@Override
	public Class<IValue> getValueTypeClass() {
		return IValue.class;
	}

}
