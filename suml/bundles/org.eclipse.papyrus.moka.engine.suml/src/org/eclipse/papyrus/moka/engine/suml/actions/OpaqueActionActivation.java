/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.suml.actions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.papyrus.moka.engine.suml.ScriptExecutionFactoryRegistry;
import org.eclipse.papyrus.moka.engine.suml.opaquebehaviors.ScriptExecution;
import org.eclipse.papyrus.moka.fuml.actions.ActionActivation;
import org.eclipse.papyrus.moka.fuml.commonbehavior.ParameterValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IValue;
import org.eclipse.uml2.uml.InputPin;
import org.eclipse.uml2.uml.OpaqueAction;
import org.eclipse.uml2.uml.OutputPin;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.ParameterDirectionKind;
import org.eclipse.uml2.uml.Pin;
import org.eclipse.uml2.uml.UMLFactory;

public class OpaqueActionActivation extends ActionActivation {
	
	private static Parameter parameterFromPin(final Pin pin) {
			
		Parameter p = UMLFactory.eINSTANCE.createParameter();
		p.setName(pin.getName());
		p.setType(pin.getType());
		p.setUpper(pin.getUpper());
		p.setLower(pin.getLower());
		p.setType( pin.getType() );
		
		if( pin instanceof InputPin )
			p.setDirection(ParameterDirectionKind.IN_LITERAL);
		else
			p.setDirection(ParameterDirectionKind.OUT_LITERAL);
		
		return p;
	}
	
	private boolean hasBody() {
		OpaqueAction opaqueAction = (OpaqueAction)this.node;
		return ( opaqueAction.getLanguages().size() > 0 && opaqueAction.getBodies().size() > 0 );			
	}
	
	@Override
	public void doAction() {
		if( !hasBody() ) //Doesn't have a script
			return;
		
		ScriptExecution scriptExecution = ScriptExecutionFactoryRegistry.getInstance().getFactory().newScriptExecution();
		scriptExecution.setContext(getExecutionContext());
		
		OpaqueAction opaqueAction = (OpaqueAction)this.node;
		scriptExecution.setLanguage(opaqueAction.getLanguages().get(0));		
		scriptExecution.setBody(opaqueAction.getBodies().get(0));
		
		for( InputPin in : opaqueAction.getInputs() ) {
			ParameterValue parameterValue = new ParameterValue();
			parameterValue.parameter = parameterFromPin(in);
			parameterValue.values = this.takeTokens(in);
			scriptExecution.setParameterValue(parameterValue);
		}
		
		for( OutputPin out : opaqueAction.getOutputs() ) {
			ParameterValue parameterValue = new ParameterValue();
			parameterValue.parameter = parameterFromPin(out);
			parameterValue.values = new ArrayList<IValue>();
			scriptExecution.setParameterValue(parameterValue);
		}
		
		scriptExecution.execute();
		
		for( OutputPin out : opaqueAction.getOutputs() ) {
			List<IValue> values = scriptExecution.getOutParamValues(out.getName()); 
			this.putTokens(out, values );
		}
		
		scriptExecution.destroy();
	}

}
