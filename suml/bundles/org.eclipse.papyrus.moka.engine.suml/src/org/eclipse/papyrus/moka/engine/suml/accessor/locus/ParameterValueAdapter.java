/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.suml.accessor.locus;

import java.util.List;

import org.eclipse.papyrus.moka.engine.suml.accessor.ComponentAdapter;
import org.eclipse.papyrus.moka.fuml.commonbehavior.IParameterValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IValue;
import org.eclipse.uml2.uml.Parameter;

public class ParameterValueAdapter implements ComponentAdapter<IParameterValue, IValue> {
	@Override
	public List<IValue> getValues(IParameterValue element) {
		return element.getValues();
	}

	@Override
	public Parameter getDescriptor(IParameterValue element) {
		return element.getParameter();
	}

	@Override
	public Class<IParameterValue> getAdaptedClass() {
		return IParameterValue.class;
	}

	@Override
	public Class<IValue> getValueType() {
		return IValue.class;
	}

}
