/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.suml.accessor;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.papyrus.moka.engine.suml.opaquebehaviors.LocusAdapter;

public class AccessAdapterRegistry {

	private AccessAdapterRegistry() {
	}
	
	private static AccessAdapterRegistry instance = new AccessAdapterRegistry();
	
	public static AccessAdapterRegistry getInstance() {
		return instance;
	}
	
	private ComponentAccessor accessor = new ComponentAccessor();
	private HashMap<Class, ComponentAdapter> adapters = new HashMap<Class, ComponentAdapter>();
	private HashMap<Class, ValueTypeConverter> converters = new HashMap<Class, ValueTypeConverter>();
	
	private LocusAdapter locusAdapter = new LocusAdapter();
	
	public ComponentAccessor getComponentAccessor() {
		return accessor;
	}
	
	private <T> T instance(Class<T> class_) {
		try {
			//This is Deprecated in Java 9+, and replaced for:
			//class_.getDeclaredConstructor().newInstance();
			return class_.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			return null;
		}
	}
	
	public void registerAdapter(Class<? extends ComponentAdapter> adapterClass) {
		ComponentAdapter adapter = instance(adapterClass);
		adapters.put(adapter.getAdaptedClass(), adapter);
	}
	
	public void registerConverter(Class<? extends ValueTypeConverter> converterClass) {
		ValueTypeConverter converter = instance(converterClass);
		converters.put(converter.getValueTypeClass(), converter);
	}
	
	private Object findAssignableFor(Class class_, Map map) {
		Object adapter = map.get(class_);
		
		if( adapter != null )
			return adapter;
		
		for( Object o : map.entrySet() ) {
			Entry e = (Entry)o;
			if( ( (Class)e.getKey() ).isAssignableFrom(class_) )
				return e.getValue();
		}
		return null;//Not found at all
	}
	
	private Object accessOrCreate(Class class_, Map map) {
		Object obj = findAssignableFor(class_, map);
		if( obj == null ) {
			obj = instance(class_);
			map.put(class_, obj);
		}
		return obj;
	}
	
	public ComponentAdapter getAdapterForObject(Object obj) {
		ComponentAdapter adapter = (ComponentAdapter) findAssignableFor(obj.getClass(), adapters);		
		return adapter;
	}
	
	public <T extends ComponentAdapter> T getAdapterForClass(Class<T> class_) {
		return (T) accessOrCreate(class_, adapters);
	}
	
	public ValueTypeConverter getConverterForValue(Object obj) {
		ValueTypeConverter converter = (ValueTypeConverter) findAssignableFor(obj.getClass(), converters);		
		return converter;
	}
	
	public <T extends ValueTypeConverter> T getConverterForClass(Class class_) {
		return (T) accessOrCreate(class_, converters);
	}
	
	public LocusAdapter getLocusAdapter() {
		return locusAdapter;
	}

	public void setLocusAdapter(LocusAdapter adapter) {
		locusAdapter = adapter;
	}
}
