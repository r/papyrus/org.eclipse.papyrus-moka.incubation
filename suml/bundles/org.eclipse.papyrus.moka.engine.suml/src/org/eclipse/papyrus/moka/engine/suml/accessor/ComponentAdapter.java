/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.suml.accessor;

import java.util.List;

import org.eclipse.uml2.uml.MultiplicityElement;
import org.eclipse.uml2.uml.TypedElement;

public interface ComponentAdapter<ComponentType, ValueType> {
	
	public List<ValueType> getValues(ComponentType element);
	
	public <DescriptorType extends MultiplicityElement & TypedElement> DescriptorType getDescriptor(ComponentType component);
	
	public Class<ComponentType> getAdaptedClass();
	
	public Class<ValueType> getValueType();
	
	
}
