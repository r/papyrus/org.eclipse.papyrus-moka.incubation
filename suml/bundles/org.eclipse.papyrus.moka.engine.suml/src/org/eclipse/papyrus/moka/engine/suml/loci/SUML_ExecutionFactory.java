/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.suml.loci;

import org.eclipse.papyrus.moka.engine.suml.BodyScriptFactoryRegistry;
import org.eclipse.papyrus.moka.engine.suml.actions.OpaqueActionActivation;
import org.eclipse.papyrus.moka.engine.uml.time.semantics.Loci.TimedExecutionFactory;
import org.eclipse.papyrus.moka.fuml.commonbehavior.IOpaqueBehaviorExecution;
import org.eclipse.papyrus.moka.fuml.loci.ISemanticVisitor;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IEvaluation;
import org.eclipse.uml2.uml.BodyOwner;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.OpaqueAction;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.ValueSpecification;

public class SUML_ExecutionFactory extends TimedExecutionFactory {

	@Override
	public ISemanticVisitor instantiateVisitor(Element element) {
		//Look for the elements that might have scripts inside
		if (element instanceof OpaqueAction) {
			if( isSupported( (BodyOwner) element ) )
				return new OpaqueActionActivation();
		}
		 
		if( element instanceof OpaqueExpression) {
			if( isSupported( (BodyOwner) element ) )
				return new SUML_OpaqueExpressionEvaluation();
		}
			
		
		return super.instantiateVisitor(element);
	}

	private boolean isSupported(BodyOwner bodyOwner) {
		if( bodyOwner.getBodies().size() < 1 )
			return false;
		
		if( bodyOwner.getLanguages().size() < 1 )
			return false;
		
		//Maybe iterate on the languages until there's at least one supported?
		return BodyScriptFactoryRegistry.getInstance().isSupported( bodyOwner.getLanguages().get(0) );
	}
	
	//Check the prototype copying mechanism on the super class ExecutionFactory 
	@Override
	public IOpaqueBehaviorExecution instantiateOpaqueBehaviorExecution(OpaqueBehavior behavior) {
		if( !isSupported(behavior) )
			return super.instantiateOpaqueBehaviorExecution(behavior);
		
		SUML_OpaqueBehaviorExecution execution = new SUML_OpaqueBehaviorExecution();
		execution.addType(behavior);
		return execution;
	}

	@Override
	public IEvaluation createEvaluation(ValueSpecification specification) {
		return super.createEvaluation(specification);
	}

}
