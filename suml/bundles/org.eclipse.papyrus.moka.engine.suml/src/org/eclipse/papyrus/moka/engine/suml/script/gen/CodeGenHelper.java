/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.suml.script.gen;

public class CodeGenHelper {
	
	//https://www.python.org/dev/peps/pep-0008/#indentation
	protected static final int TAB_SPACES = 4;
	protected static final String TAB_PLACEHOLDER = "    ";
	
	public static String getTab(int tabsNum) {
		StringBuilder builder = new StringBuilder();
		
		for( int i = 0; i < tabsNum; i++ ) 
			builder.append(TAB_PLACEHOLDER);
		
		return builder.toString();
	}
	
	public static String clearTabs(String body) {
		return body.replaceAll("\t", getTab(1));
	}
}
