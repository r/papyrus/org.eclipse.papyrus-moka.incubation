/*****************************************************************************
 * Copyright (c) 2019, 2020 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr (CEA LIST)
 *   jeremie.tatibouet@cea.fr (CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.suml;

import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.papyrus.moka.engine.suml.accessor.AccessAdapterRegistry;
import org.eclipse.papyrus.moka.engine.suml.accessor.locus.FeatureValueAdapter;
import org.eclipse.papyrus.moka.engine.suml.accessor.locus.IValueConverter;
import org.eclipse.papyrus.moka.engine.suml.accessor.locus.ParameterValueAdapter;
import org.eclipse.papyrus.moka.engine.suml.loci.SUML_ExecutionFactory;
import org.eclipse.papyrus.moka.engine.suml.opaquebehaviors.SUMLScriptExecutionFactory;
import org.eclipse.papyrus.moka.engine.suml.tasks.SUMLTaskExecutionFactory;
import org.eclipse.papyrus.moka.engine.uml.time.UMLTimedExecutionEngine;
import org.eclipse.papyrus.moka.engine.uml.time.semantics.Loci.TimedLocus;
import org.eclipse.papyrus.moka.fuml.loci.ILocus;
import org.eclipse.papyrus.moka.fuml.tasks.IUMLTaskExecutionFactory;
import org.eclipse.papyrus.moka.kernel.engine.EngineConfiguration;
import org.eclipse.papyrus.moka.pscs.loci.CS_Executor;

public class SUMLExecutionEngine extends UMLTimedExecutionEngine {
	
	@Override
	public ILocus createLocus() {
		ILocus locus = new TimedLocus();
		locus.setExecutor(new CS_Executor());
		locus.setFactory(new SUML_ExecutionFactory());
		return locus;
	}

	@Override
	public void init(EngineConfiguration<?> configuration, SubMonitor monitor) {
		super.init(configuration, monitor);
		
		//Register the used component/value type
		AccessAdapterRegistry.getInstance().registerAdapter(ParameterValueAdapter.class);
		AccessAdapterRegistry.getInstance().registerAdapter(FeatureValueAdapter.class);
		AccessAdapterRegistry.getInstance().registerConverter(IValueConverter.class);
		
		ScriptExecutionFactoryRegistry.getInstance().setFactory(new SUMLScriptExecutionFactory());
		
		BodyScriptFactoryRegistry registry = BodyScriptFactoryRegistry.getInstance();
		
		URI modelURI = null;
		try {
			modelURI = new URI(configuration.getModelURI().toString());
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		
		//Init factories
		for( String k : registry.getRegisteredLanguages()) 
			registry.getBodyScriptFactoryFor(k).configure(modelURI);
		
	}
	
	@Override
	protected IUMLTaskExecutionFactory createUMLTaskFactory() {
		return new SUMLTaskExecutionFactory(controller.getExecutionLoop());
	}

	@Override
	protected void dispose(SubMonitor monitor) {
		super.dispose(monitor);
		BodyScriptFactoryRegistry.getInstance().terminateEngines();
	}
}
