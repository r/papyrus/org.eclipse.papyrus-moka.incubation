/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.suml.accessor.structures;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.papyrus.moka.engine.suml.accessor.IndexableMapAccess;

public class ArrayIndexableMapAccess<K, V> implements IndexableMapAccess<K, V> {
	private Map<K, V> map;
	private ArrayList<V> list;
	
	public ArrayIndexableMapAccess() {
		map = new HashMap<K, V>();
		list = new ArrayList<V>();
	}

	public void clear() {
		map.clear();
		list.clear();
	}

	public boolean containsKey(Object key) {
		return map.containsKey(key);
	}

	public boolean containsValue(Object value) {
		return map.containsValue(value);
	}

	public Set<Entry<K, V>> entrySet() {
		return map.entrySet();
	}

	public boolean equals(Object o) {
		return map.equals(o);
	}

	public V get(Object key) {
		return map.get(key);
	}

	public int hashCode() {
		return map.hashCode();
	}

	public boolean isEmpty() {
		return map.isEmpty();
	}

	public Set<K> keySet() {
		return map.keySet();
	}

	public V put(K key, V value) {
		list.add(value);
		return map.put(key, value);
	}

	public void putAll(Map<? extends K, ? extends V> m) {
		list.addAll(m.values());
		map.putAll(m);
	}

	public V remove(Object key) {
		V value = map.remove(key);
		list.remove(value);
		return value;
	}

	public int size() {
		return map.size();
	}

	public Collection<V> values() {
		return map.values();
	}

	@Override
	public V valueAt(int index) {
		return list.get(index);
	}

}
