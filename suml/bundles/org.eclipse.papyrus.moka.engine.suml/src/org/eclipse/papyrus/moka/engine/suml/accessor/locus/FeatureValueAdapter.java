/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.suml.accessor.locus;

import java.util.List;

import org.eclipse.papyrus.moka.engine.suml.accessor.ComponentAdapter;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IFeatureValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IValue;
import org.eclipse.uml2.uml.StructuralFeature;

public class FeatureValueAdapter implements ComponentAdapter<IFeatureValue, IValue> {
	@Override
	public List<IValue> getValues(IFeatureValue element) {
		return element.getValues();
	}

	@Override
	public StructuralFeature getDescriptor(IFeatureValue component) {
		return component.getFeature();
	}


	@Override
	public Class<IFeatureValue> getAdaptedClass() {
		return IFeatureValue.class;
	}


	@Override
	public Class<IValue> getValueType() {
		return IValue.class;
	}
	
}
