/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.suml.accessor.structures;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.papyrus.moka.engine.suml.accessor.ComponentAccessor;
import org.eclipse.papyrus.moka.engine.suml.accessor.ValueTypeWrapper;

public class MapAccess<ComponentType, ValueType> implements Map<String, Object>, ValueTypeWrapper<ValueType>{
	private Map<String, ComponentType> components;
	private Map<String, OperationAccess> operations;
	private ComponentAccessor<ComponentType, ValueType> adapter;
	
	private ValueType value;
	
	public MapAccess(ComponentAccessor<ComponentType, ValueType> adapter, ValueType value, Map<String, ComponentType> components) {
		this.components = components;
		this.adapter = adapter;
		this.value = value;
		this.operations = new HashMap<String, OperationAccess>();
	}
	
	public MapAccess(ComponentAccessor<ComponentType, ValueType> adapter, ValueType value, Map<String, ComponentType> components, Map<String, OperationAccess> operations) {
		this.components = components;
		this.adapter = adapter;
		this.value = value;
		this.operations = operations;
	}

	@Override
	public void clear() {
		throw new RuntimeException("Operation not supported for this object");
	}

	@Override
	public boolean containsKey(Object arg0) {
		return components.containsKey((String)arg0) || this.operations.containsKey((String)arg0) ;
	}

	@Override
	public boolean containsValue(Object arg0) {
		throw new RuntimeException("Operation not supported for this object");
	}

	@Override
	public Set<Entry<String, Object>> entrySet() {
		throw new RuntimeException("Operation not supported for this object");
	}

	@Override
	public Object get(Object arg0) {
		if( this.components.containsKey(arg0) )
			//Returns a ValueType as Raw
			return adapter.componentValueToScript(components.get((String) arg0));
		
		if( this.operations.containsKey(arg0) )
			return this.operations.get(arg0);
		
		throw new RuntimeException(arg0 + " property not found in MapAccess.");
	}

	@Override
	public boolean isEmpty() {
		return components.isEmpty();
	}

	@Override
	public Set<String> keySet() {
		Set<String> keys = new HashSet<String>();
		keys.addAll(components.keySet());
		keys.addAll(operations.keySet());
		return keys;
	}

	@Override
	public Object put(String key, Object object) {
		Object old = get(key);
		adapter.setValueFromScript(components.get((String)key), object);
		return old;
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> arg0) {
		throw new RuntimeException("Operation not supported for this object");
	}

	@Override
	public Object remove(Object arg0) {
		throw new RuntimeException("Operation not supported for this object");
	}

	@Override
	public int size() {		
		return components.size();
	}

	@Override
	public Collection<Object> values() {
		throw new RuntimeException("Operation not supported for this object");
	}
	
	@Override
	public ValueType unwrap() {
		return value;
	}

	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append("MapAccess: {");
		for ( String k : components.keySet() ) 
			buf.append(k).append(":").append(get(k)).append(", ");
		
		buf.append("}");
		return buf.toString();
	}
	
	public boolean hasOperation(String operation) {
		return operations.containsKey(operation);
	}
	
	public OperationAccess getOperationAccess(String operation) {
		return operations.get(operation);
	}

}
