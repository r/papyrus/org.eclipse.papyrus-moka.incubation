/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.suml.accessor.uml;

import java.util.List;

import org.eclipse.papyrus.moka.engine.suml.accessor.ComponentAdapter;
import org.eclipse.uml2.uml.Slot;
import org.eclipse.uml2.uml.StructuralFeature;
import org.eclipse.uml2.uml.ValueSpecification;

public class SlotAdapter implements ComponentAdapter<Slot, ValueSpecification> {

	@Override
	public List<ValueSpecification> getValues(Slot element) {
		return element.getValues();
	}

	@Override
	public StructuralFeature getDescriptor(Slot component) {
		return component.getDefiningFeature();
	}

	@Override
	public Class<Slot> getAdaptedClass() {
		return Slot.class;
	}

	@Override
	public Class<ValueSpecification> getValueType() {
		return ValueSpecification.class; 
	}

}
