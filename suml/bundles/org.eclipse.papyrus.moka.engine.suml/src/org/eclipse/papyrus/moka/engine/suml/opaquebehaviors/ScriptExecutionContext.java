/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.suml.opaquebehaviors;

import java.util.HashMap;
import java.util.List;

import org.eclipse.papyrus.moka.engine.suml.accessor.AccessAdapterRegistry;
import org.eclipse.papyrus.moka.engine.suml.accessor.ComponentAccessor;
import org.eclipse.papyrus.moka.engine.suml.accessor.structures.ArrayIndexableMapAccess;
import org.eclipse.papyrus.moka.fuml.commonbehavior.IParameterValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IValue;
import org.eclipse.papyrus.moka.fuml.structuredclassifiers.IObject_;

public class ScriptExecutionContext {
	
	public static final String KEY_INSTANCE = "instance";
	public static final String KEY_IN = "in";
	public static final String KEY_OUT = "out";
	public static final String KEY_LOCUS = "jlocus";
	
	private HashMap<String, IParameterValue> outParams;
	
	protected HashMap<String, Object> map = new HashMap<String, Object>();
	
	public ScriptExecutionContext() {
		outParams = new HashMap<String, IParameterValue>();
		map.put(KEY_IN, new ArrayIndexableMapAccess<String, Object>());
		map.put(KEY_OUT, new ArrayIndexableMapAccess<String, Object>());
	}

	public void setInstance(IObject_ instance) {
		LocusAdapter locusAdapter = AccessAdapterRegistry.getInstance().getLocusAdapter();
		locusAdapter.setExecutionInstance(instance);
		map.put(KEY_LOCUS, locusAdapter);
		
		//Adapt the object
		Object adapted = AccessAdapterRegistry.getInstance().getComponentAccessor().valueToScript( instance );
		
		map.put(KEY_INSTANCE, adapted);
	}

	public ArrayIndexableMapAccess<String, Object> getIn() {
		return (ArrayIndexableMapAccess<String, Object>) map.get(KEY_IN);
	}

	public ArrayIndexableMapAccess<String, Object> getOut() {
		return (ArrayIndexableMapAccess<String, Object>) map.get(KEY_OUT);
	}
	
	private ComponentAccessor getParameterAdapter() {
		return AccessAdapterRegistry.getInstance().getComponentAccessor();
	}

	public void addInParameter(IParameterValue paramValue) {
		Object value = getParameterAdapter().componentValueToScript(paramValue);
		getIn().put(paramValue.getParameter().getName(), value);
	}
	
	public void addOutParameter(IParameterValue paramValue) {
		Object value = getParameterAdapter().componentValueToScript(paramValue);
		getOut().put(paramValue.getParameter().getName(), value);
		
		outParams.put(paramValue.getParameter().getName(), paramValue);
	}
	
	protected void assignOutputValues() {
		for( String name : outParams.keySet() ) {
			IParameterValue p = outParams.get(name);			
			getParameterAdapter().setValueFromScript(p, getOut().get(name));
		}
	}
	
	public HashMap<String, Object> toHashMap() {
		return map;
	}
	
	public void inject(String key, Object object) {
		map.put(key, object);
	}
	
	public List<IValue> getOutParamValues(String name) {
		IParameterValue p = outParams.get(name);
		return p.getValues();
	}
	
}
