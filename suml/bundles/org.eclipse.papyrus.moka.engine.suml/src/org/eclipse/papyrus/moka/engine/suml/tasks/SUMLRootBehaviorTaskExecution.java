/*****************************************************************************
 * Copyright (c) 2020 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.suml.tasks;

import org.eclipse.papyrus.moka.engine.uml.scheduling.RootBehaviorTaskExecution;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IValue;
import org.eclipse.papyrus.moka.kernel.scheduling.control.IExecutionLoop;
import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.OpaqueBehavior;

public class SUMLRootBehaviorTaskExecution extends RootBehaviorTaskExecution {

	public SUMLRootBehaviorTaskExecution(IExecutionLoop loop, Behavior executionRoot) {
		super(loop, executionRoot);
	}

	@Override
	public boolean canExecute() {
		return super.canExecute() || root instanceof OpaqueBehavior;
	}

	@Override
	public IValue new_() {
		return new SUMLRootBehaviorTaskExecution(executionLoop, root);
	}

}
