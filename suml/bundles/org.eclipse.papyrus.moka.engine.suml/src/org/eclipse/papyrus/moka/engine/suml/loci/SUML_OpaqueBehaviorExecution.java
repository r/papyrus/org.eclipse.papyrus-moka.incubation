/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.suml.loci;

import java.util.List;

import org.eclipse.papyrus.moka.engine.suml.ScriptExecutionFactoryRegistry;
import org.eclipse.papyrus.moka.engine.suml.opaquebehaviors.ScriptExecution;
import org.eclipse.papyrus.moka.fuml.commonbehavior.IParameterValue;
import org.eclipse.papyrus.moka.fuml.commonbehavior.OpaqueBehaviorExecution;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IValue;
import org.eclipse.uml2.uml.OpaqueBehavior;

public class SUML_OpaqueBehaviorExecution extends OpaqueBehaviorExecution{

	@Override
	public void doBody(List<IParameterValue> inputParameters, List<IParameterValue> outputParameters) {
		
		ScriptExecution scriptExecution = ScriptExecutionFactoryRegistry.getInstance().getFactory().newScriptExecution();
		scriptExecution.setContext(getContext());
		
		OpaqueBehavior behavior = (OpaqueBehavior) getBehavior();
		scriptExecution.setLanguage(behavior.getLanguages().get(0));		
		scriptExecution.setBody(behavior.getBodies().get(0));
		
		for( IParameterValue in : inputParameters) 
			scriptExecution.setParameterValue(in);
			
		for( IParameterValue out : outputParameters ) 
			scriptExecution.setParameterValue(out);
		
		scriptExecution.execute();
		scriptExecution.destroy();
	}

	@Override
	public IValue new_() {
		return new SUML_OpaqueBehaviorExecution();
	}

}
