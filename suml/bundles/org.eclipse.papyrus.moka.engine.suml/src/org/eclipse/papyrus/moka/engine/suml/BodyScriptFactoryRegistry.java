/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.suml;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.papyrus.moka.engine.suml.script.IBodyScriptFactory;

public class BodyScriptFactoryRegistry {
	
	//Extension points
	public static String MOKA_BODYSCRIPTFACTORY_EXT_POINT = "org.eclipse.papyrus.moka.engine.suml.bodyScriptFactory" ; //$NON-NLS-1$
	
	private Map<String, IBodyScriptFactory> registry;	
	
	private void loadBodyScriptFactories() {
		registry = new HashMap<String, IBodyScriptFactory>();
		
		IExtensionRegistry extRegistry = Platform.getExtensionRegistry();
		IConfigurationElement[] config = extRegistry.getConfigurationElementsFor(MOKA_BODYSCRIPTFACTORY_EXT_POINT);
		try {
			for( IConfigurationElement element : config ) {
				String lang = element.getAttribute("language");//$NON-NLS-1$
				IBodyScriptFactory factory = (IBodyScriptFactory) element.createExecutableExtension("class");//$NON-NLS-1$
				registry.put(lang, factory);
			}
		} catch (CoreException ex) {
			ex.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private BodyScriptFactoryRegistry() {
		loadBodyScriptFactories();
	}
	
	private static BodyScriptFactoryRegistry instance = null;
	
	public static BodyScriptFactoryRegistry getInstance() {
		if( instance == null )
			instance = new BodyScriptFactoryRegistry();
		
		return instance;
	}
	
	public boolean isSupported(String language) {
		return registry.containsKey(language);
	}
		
	public IBodyScriptFactory getBodyScriptFactoryFor(String language) {
		return registry.get(language);
	}
	
	public void terminateEngines() {
		for( IBodyScriptFactory factory : registry.values() )
			factory.terminate();
	}
	
	public Set<String> getRegisteredLanguages(){
		return registry.keySet();
	}
}
