/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.suml.accessor.structures;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.eclipse.papyrus.moka.engine.suml.accessor.ComponentAccessor;

public class ListAccess<ComponentType, ValueType> implements List<Object>{
	
	private ComponentAccessor<ComponentType, ValueType> adapter;
	private ComponentType component;
	
	public ListAccess(ComponentAccessor<ComponentType, ValueType> adapter, ComponentType component) {
		this.adapter = adapter;
		this.component = component;
	}

	@Override
	public boolean add(Object e) {
		return adapter.addValueFromScript(component, e);
	}

	@Override
	public void add(int index, Object e) {
		throw new RuntimeException("Operation not supported for this object");
	}

	@Override
	public boolean addAll(Collection<? extends Object> c) {
		throw new RuntimeException("Operation not supported for this object");
	}

	@Override
	public boolean addAll(int index, Collection<? extends Object> c) {
		throw new RuntimeException("Operation not supported for this object");
	}

	@Override
	public void clear() {
		adapter.getValues(component).clear();
	}

	@Override
	public boolean contains(Object o) {
		throw new RuntimeException("Operation not supported for this object");
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		throw new RuntimeException("Operation not supported for this object");
	}

	@Override
	public Object get(int index) {
		return adapter.componentValueIndexToScript( component, index );
	}

	@Override
	public int indexOf(Object o) {
		throw new RuntimeException("Operation not supported for this object");
	}

	@Override
	public boolean isEmpty() {
		return adapter.getValues(component).isEmpty();
	}

	@Override
	public Iterator<Object> iterator() {
		return null; //There's a special object to create
	}

	@Override
	public int lastIndexOf(Object o) {
		throw new RuntimeException("Operation not supported for this object");
	}

	@Override
	public ListIterator<Object> listIterator() {
		throw new RuntimeException("Operation not supported for this object");
	}

	@Override
	public ListIterator<Object> listIterator(int index) {
		throw new RuntimeException("Operation not supported for this object");
	}

	@Override
	public boolean remove(Object o) {
		throw new RuntimeException("Operation not supported for this object");
	}

	@Override
	public Object remove(int index) {
		throw new RuntimeException("Operation not supported for this object");
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		throw new RuntimeException("Operation not supported for this object");
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		throw new RuntimeException("Operation not supported for this object");
	}

	@Override
	/**
	 * Supposes there's a value at index. If not an exception will be raised
	 */
	public Object set(int index, Object obj) {
		adapter.setValueFromScriptAt(component, index, obj);
		return null;
	}

	@Override
	public int size() {		
		return adapter.getValues(component).size();
	}

	@Override
	public List<Object> subList(int fromIndex, int toIndex) {
		throw new RuntimeException("Operation not supported for this object");
	}

	@Override
	public Object[] toArray() {
		throw new RuntimeException("Operation not supported for this object");
	}

	@Override
	public <T> T[] toArray(T[] a) {
		throw new RuntimeException("Operation not supported for this object");
	}
	
}
