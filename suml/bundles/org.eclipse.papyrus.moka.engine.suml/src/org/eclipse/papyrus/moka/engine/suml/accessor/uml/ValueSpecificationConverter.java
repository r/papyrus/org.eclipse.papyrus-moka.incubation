/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.suml.accessor.uml;

import java.util.List;

import org.eclipse.papyrus.moka.engine.suml.accessor.ValueTypeConverter;
import org.eclipse.uml2.uml.InstanceValue;
import org.eclipse.uml2.uml.LiteralBoolean;
import org.eclipse.uml2.uml.LiteralInteger;
import org.eclipse.uml2.uml.LiteralNull;
import org.eclipse.uml2.uml.LiteralReal;
import org.eclipse.uml2.uml.LiteralString;
import org.eclipse.uml2.uml.LiteralUnlimitedNatural;
import org.eclipse.uml2.uml.Slot;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.ValueSpecification;

public class ValueSpecificationConverter implements ValueTypeConverter<ValueSpecification> {

	@Override
	public boolean isComposite(ValueSpecification value) {
		return value instanceof InstanceValue;
	}

	@Override
	public List<Slot> getComponents(ValueSpecification value) {
		InstanceValue iv = (InstanceValue) value;
		return iv.getInstance().getSlots();
	}

	@Override
	public Object getRawValue(ValueSpecification value) {
		if( value instanceof LiteralBoolean )
			return value.booleanValue();
		
		if( value instanceof LiteralInteger )
			return value.integerValue();
		
		if( value instanceof LiteralNull )
			return null;
		
		if( value instanceof LiteralReal )
			return value.realValue();
		
		if( value instanceof LiteralString )
			return value.stringValue();
		
		if( value instanceof LiteralUnlimitedNatural )
			return value.integerValue();
		
		return null;
	}

	@Override
	public boolean isValueType(Object obj) {
		return obj instanceof ValueSpecification;
	}

	@Override
	public ValueSpecification newPrimitiveValue(Type type, Object obj) {
		if( obj instanceof Double || obj instanceof Float ) {
			LiteralReal realValue = UMLFactory.eINSTANCE.createLiteralReal();
			realValue.setValue( (Double)obj );
			return realValue;
		}
		
		if( obj instanceof Integer ) {
			LiteralInteger integerValue = UMLFactory.eINSTANCE.createLiteralInteger();
			integerValue.setValue( (Integer)obj);
			return integerValue;
		}
		
		if( obj instanceof Boolean ) {
			LiteralBoolean boolValue = UMLFactory.eINSTANCE.createLiteralBoolean();
			boolValue.setValue( (Boolean)obj );
			return boolValue;
		}
		
		if( obj instanceof String ) {
			LiteralString strValue =  UMLFactory.eINSTANCE.createLiteralString();
			strValue.setValue( (String)obj );
			return strValue;
		}
		
		return null;
	}

	@Override
	public void setRawValue(ValueSpecification value, Object obj) {		
		if( value instanceof LiteralBoolean )
			((LiteralBoolean)value).setValue((Boolean) obj);
		
		if( value instanceof LiteralInteger )
			((LiteralInteger)value).setValue((Integer) obj);
		
		if( value instanceof LiteralUnlimitedNatural )
			((LiteralUnlimitedNatural)value).setValue((Integer) obj);
		
		if( value instanceof LiteralReal )
			((LiteralReal)value).setValue((Double) obj);
		
		if( value instanceof LiteralString )
			((LiteralString)value).setValue((String) obj);
		
	}

	@Override
	public boolean isPrimitive(Type type) {
		return false;
	}

	@Override
	public Class<ValueSpecification> getValueTypeClass() {
		return ValueSpecification.class;
	}

	@Override
	public ValueSpecification newValue(Object obj) {
		return null;
	}
}
