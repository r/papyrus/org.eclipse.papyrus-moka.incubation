/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.suml.loci;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.papyrus.moka.engine.suml.ScriptExecutionFactoryRegistry;
import org.eclipse.papyrus.moka.engine.suml.accessor.AccessAdapterRegistry;
import org.eclipse.papyrus.moka.engine.suml.accessor.ComponentAccessor;
import org.eclipse.papyrus.moka.engine.suml.accessor.locus.IValueConverter;
import org.eclipse.papyrus.moka.engine.suml.accessor.structures.ArrayIndexableMapAccess;
import org.eclipse.papyrus.moka.engine.suml.opaquebehaviors.ScriptExecution;
import org.eclipse.papyrus.moka.engine.suml.opaquebehaviors.ScriptExecutionContext;
import org.eclipse.papyrus.moka.fuml.commonbehavior.ICallEventOccurrence;
import org.eclipse.papyrus.moka.fuml.commonbehavior.IEventOccurrence;
import org.eclipse.papyrus.moka.fuml.commonbehavior.IParameterValue;
import org.eclipse.papyrus.moka.fuml.commonbehavior.ISignalEventOccurrence;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IFeatureValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IValue;
import org.eclipse.papyrus.moka.pscs.commonbehavior.CS_EventOccurrence;
import org.eclipse.papyrus.moka.pssm.values.SM_OpaqueExpressionEvaluation;
import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.BodyOwner;
import org.eclipse.uml2.uml.OpaqueExpression;

public class SUML_OpaqueExpressionEvaluation extends SM_OpaqueExpressionEvaluation{

	private ScriptExecution scriptExecution = ScriptExecutionFactoryRegistry.getInstance().getFactory().newScriptExecution();
	
	@Override
	public void initialize(IEventOccurrence eventOccurrence) {
		
		IEventOccurrence evtInstance = eventOccurrence;
		
		if( eventOccurrence instanceof CS_EventOccurrence ) {
			evtInstance = ( (CS_EventOccurrence)eventOccurrence ).getWrappedEventOccurrence();
		}
		
		
		if( evtInstance instanceof ISignalEventOccurrence ) {
			setupSignalEvent((ISignalEventOccurrence) evtInstance);
		}
		
		if( evtInstance instanceof ICallEventOccurrence ) {
			setupCallEvent((ICallEventOccurrence) evtInstance);
		}
		
		
	}
	
	private void setupSignalEvent(ISignalEventOccurrence evt) {
		
		ArrayIndexableMapAccess<String, Object> event = new ArrayIndexableMapAccess<String, Object>();
		ComponentAccessor ca = AccessAdapterRegistry.getInstance().getComponentAccessor();
		
		for( IFeatureValue paramValue : evt.getSignalInstance().getFeatureValues() ) {
			Object value = ca.componentValueToScript(paramValue);
			event.put(paramValue.getFeature().getName(), value);			
		}
		ScriptExecutionContext ctx = scriptExecution.getScriptExecutionContext();
		
		ctx.inject("event", event);
	}

	
	private void setupCallEvent(ICallEventOccurrence evt) {
		
		ArrayIndexableMapAccess<String, Object> event = new ArrayIndexableMapAccess<String, Object>();
		ComponentAccessor ca = AccessAdapterRegistry.getInstance().getComponentAccessor();
		
		for( IParameterValue paramValue : evt.getParameterValues() ) {
			Object value = ca.componentValueToScript(paramValue);
			event.put(paramValue.getParameter().getName(), value);			
		}
		ScriptExecutionContext ctx = scriptExecution.getScriptExecutionContext();
		
		ctx.inject("event", event);
	}
	

	@Override
	public List<IValue> executeExpressionBehavior() {
		OpaqueExpression expression = (OpaqueExpression)this.specification;
		Behavior behavior = expression.getBehavior();
		return executeBodyOwner(expression);
	}
	
	private List<IValue> executeBodyOwner(BodyOwner bo){
		scriptExecution.setContext(getContext());
		scriptExecution.setLanguage(bo.getLanguages().get(0));		
		scriptExecution.setBody(bo.getBodies().get(0));
		
		for( IParameterValue param : this.parameterValues) 
			scriptExecution.setParameterValue(param);
			
		scriptExecution.execute();
		
		//Adapt Value
		IValueConverter conv = (IValueConverter)AccessAdapterRegistry.getInstance().getConverterForClass(IValue.class);
		IValue value = conv.newValue(scriptExecution.getReturnObject());
		
		List<IValue> list = new ArrayList<IValue>();
		list.add(value);
		
		scriptExecution.destroy();
		
		return list;
	}

}
