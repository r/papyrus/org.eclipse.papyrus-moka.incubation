/*****************************************************************************
 * Copyright (c) 2020 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.suml.tasks;

import org.eclipse.papyrus.moka.engine.uml.scheduling.UMLTaskExecutionFactory;
import org.eclipse.papyrus.moka.fuml.tasks.IUMLRootTaskExecution;
import org.eclipse.papyrus.moka.kernel.scheduling.control.IExecutionLoop;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.OpaqueBehavior;

public class SUMLTaskExecutionFactory extends UMLTaskExecutionFactory {

	public SUMLTaskExecutionFactory(IExecutionLoop loop) {
		super(loop);
	}

	@Override
	public IUMLRootTaskExecution<?> createRootExecution(Element executionRoot) {
		IUMLRootTaskExecution<?> rootExecution = null;
		if (executionRoot instanceof OpaqueBehavior) {
			rootExecution = new SUMLRootBehaviorTaskExecution(executionLoop, (OpaqueBehavior) executionRoot);
		} else {
			rootExecution = super.createRootExecution(executionRoot);
		}
		return rootExecution;
	}

}
