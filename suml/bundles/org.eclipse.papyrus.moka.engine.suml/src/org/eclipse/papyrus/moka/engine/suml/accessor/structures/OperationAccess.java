/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.suml.accessor.structures;

import java.util.List;

import org.eclipse.papyrus.moka.engine.suml.accessor.AccessAdapterRegistry;
import org.eclipse.papyrus.moka.fuml.structuredclassifiers.IReference;
import org.eclipse.uml2.uml.Operation;

public class OperationAccess<ValueType> {
	private Operation op;
	private ValueType target;
	
	public OperationAccess(ValueType target, Operation op) {
		super();
		this.op = op;
		this.target = target;
	}
	
	public Object call(List<Object> params) {
		Object ret = AccessAdapterRegistry.getInstance().getLocusAdapter().callOperation(op, (IReference)target, params);		
		return ret;
	}
}
