/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.suml.opaquebehaviors;

import java.util.List;

import org.eclipse.papyrus.moka.engine.suml.BodyScriptFactoryRegistry;
import org.eclipse.papyrus.moka.engine.suml.script.IBodyScript;
import org.eclipse.papyrus.moka.engine.suml.script.IBodyScriptFactory;
import org.eclipse.papyrus.moka.fuml.commonbehavior.Execution;
import org.eclipse.papyrus.moka.fuml.commonbehavior.IParameterValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IValue;
import org.eclipse.uml2.uml.ParameterDirectionKind;

public class ScriptExecution extends Execution {

	private String body, language;
	protected ScriptExecutionContext ctx;
	private Object returnObject;
	private String returnParameterName;

	protected ScriptExecution() {
		this.ctx = new ScriptExecutionContext();
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	private void buildScriptContext() {
		ctx.setInstance(getContext());

		for (IParameterValue param : this.getParameterValues()) {
			if (param.getParameter().getDirection() == ParameterDirectionKind.IN_LITERAL ||
					param.getParameter().getDirection() == ParameterDirectionKind.INOUT_LITERAL)
				ctx.addInParameter(param);

			if (param.getParameter().getDirection() == ParameterDirectionKind.OUT_LITERAL ||
					param.getParameter().getDirection() == ParameterDirectionKind.INOUT_LITERAL ||
					param.getParameter().getDirection() == ParameterDirectionKind.RETURN_LITERAL)
				ctx.addOutParameter(param);

			if (param.getParameter().getDirection() == ParameterDirectionKind.RETURN_LITERAL) {
				returnParameterName = param.getParameter().getName();
			}
		}
	}

	@Override
	public void execute() {
		BodyScriptFactoryRegistry registry = BodyScriptFactoryRegistry.getInstance();
		IBodyScriptFactory factory = registry.getBodyScriptFactoryFor(language);

		IBodyScript bodyScript = factory.buildScript(this, body);
		buildScriptContext();

		returnObject = bodyScript.evaluate(ctx.toHashMap());

		setReturnObjectValue(returnObject);
		ctx.assignOutputValues();
	}

	private void setReturnObjectValue(Object obj) {
		if (ctx.getOut().get(returnParameterName) == null || obj != null)
			ctx.getOut().put(returnParameterName, obj);
	}

	public ScriptExecutionContext getScriptExecutionContext() {
		return ctx;
	}

	public Object getReturnObject() {
		return returnObject;
	}

	public List<IValue> getOutParamValues(String name) {
		return ctx.getOutParamValues(name);
	}

	@Override
	public IValue new_() {
		return null;
	}
}
