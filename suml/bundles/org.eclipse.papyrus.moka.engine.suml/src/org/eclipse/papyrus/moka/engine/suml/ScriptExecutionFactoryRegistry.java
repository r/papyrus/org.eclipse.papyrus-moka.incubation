/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   David Lopez david.lopez@cea.fr(CEA LIST)
 *   
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.suml;

public class ScriptExecutionFactoryRegistry {

	private static ScriptExecutionFactoryRegistry instance = new ScriptExecutionFactoryRegistry();

	private ScriptExecutionFactory factory;

	private ScriptExecutionFactoryRegistry() {

	}

	public static ScriptExecutionFactoryRegistry getInstance() {
		return instance;
	}

	/**
	 * @return the factory
	 */
	public ScriptExecutionFactory getFactory() {
		return factory;
	}

	/**
	 * @param factory
	 *            the factory to set
	 */
	public void setFactory(ScriptExecutionFactory factory) {
		this.factory = factory;
	}

}
