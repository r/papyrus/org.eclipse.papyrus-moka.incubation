/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.uml.stochastic;

import org.junit.Assert;
import org.junit.Test;

public class DistributionTests {
	
	@Test
	public void testPseudoRNG() {
		// Arrange
		PseudoRandomGenerator generator = new PseudoRandomGenerator();
		
		// Act
		generator.updateSeed();
		long current_seed = generator.getSeed();
		PseudoRandomGenerator new_generator = new PseudoRandomGenerator(current_seed, generator.getMultiplier(), generator.getAdditive(), generator.getModulus());
		double u = generator.nextDouble();
		double v = new_generator.nextDouble();
		System.out.println(System.currentTimeMillis());
		
		// Assert
		Assert.assertEquals(u, v, 0.0);
	}
	
	@Test
	public void testDistribution() {
		
	}
	
	

}
