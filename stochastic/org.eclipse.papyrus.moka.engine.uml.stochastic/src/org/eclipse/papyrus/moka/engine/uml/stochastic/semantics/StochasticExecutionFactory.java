/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.uml.stochastic.semantics;

import org.eclipse.papyrus.moka.engine.uml.time.UMLTimedExecutionEngineUtils;
import org.eclipse.papyrus.moka.engine.uml.time.activities.Timed_ActivityExecution;
import org.eclipse.papyrus.moka.engine.uml.time.semantics.Loci.TimedExecutionFactory;
import org.eclipse.papyrus.moka.fuml.loci.ISemanticVisitor;
import org.eclipse.uml2.uml.AcceptCallAction;
import org.eclipse.uml2.uml.AcceptEventAction;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.OpaqueExpression;

public class StochasticExecutionFactory extends TimedExecutionFactory {

	private static final String STOCHASTIC = "stochastic";

	@Override
	public ISemanticVisitor instantiateVisitor(Element element) {
		// Extends fUML semantics in the sense that newly introduced
		// semantic visitors are instantiated instead of fUML visitors
		ISemanticVisitor visitor = null;
		if (element instanceof OpaqueExpression && isStochasticLanguage((OpaqueExpression) element)) {
			visitor = new StochasticOpaqueExpressionEvaluation();
		} else if (element instanceof AcceptEventAction && !(element instanceof AcceptCallAction)
				&& UMLTimedExecutionEngineUtils.isTimeTriggered(((AcceptEventAction) element).getTriggers())) {
			visitor = new StochasticAcceptEventActionActivation();
		} else if (element instanceof Activity) {
			visitor = new StochasticActivityExecution();
		} else {
			visitor = super.instantiateVisitor(element);
		}
		return visitor;
	}

	private boolean isStochasticLanguage(OpaqueExpression element) {
		if (element.getLanguages().size() == 1) {
			if (element.getLanguages().get(0).equals(STOCHASTIC)) {
				return true;
			}
		}
		return false;
	}

}