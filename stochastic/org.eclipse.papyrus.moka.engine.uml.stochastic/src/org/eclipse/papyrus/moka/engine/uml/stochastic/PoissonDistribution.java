/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.uml.stochastic;

public class PoissonDistribution implements IDrawSample {

	private double l;
	private PseudoRandomGenerator generator;
	
	public PoissonDistribution(double l) {
		if (l == Double.NaN) {
			throw new IllegalArgumentException("Invalid argument: PoissonDistribution cannot take NaN argument.");
		}
		if (l <= 0) {
			throw new IllegalArgumentException("Invalid argument: lambda must belong to (0, inf).");
		} else {
			this.l = l;
			this.generator = new PseudoRandomGenerator();
		}
	}
	
	public Double nextSample() {
		double u = this.generator.nextDouble();
		double sample = 0.0;
		double y = Math.exp(-this.l);
		double x = y;
		while (x < u) {
			sample += 1;
			y = y * this.l /sample;
			x += y;
		}
		return sample;
	}
}
