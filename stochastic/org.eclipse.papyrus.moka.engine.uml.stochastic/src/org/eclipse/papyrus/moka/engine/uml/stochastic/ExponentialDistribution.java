/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.uml.stochastic;

public class ExponentialDistribution implements IDrawSample {
	
	protected double l;
	protected PseudoRandomGenerator generator;
	
	public ExponentialDistribution(double l) {
		if (l == Double.NaN) {
			throw new IllegalArgumentException("Invalid argument: ExponentialDistribution cannot take NaN argument.");
		}
		if (l <= 0) {
			throw new IllegalArgumentException("illegal argument: lambda must belong to (0, inf).");
		}
		this.l = l;
		this.generator = new PseudoRandomGenerator();
	}
	
	public Double nextSample() {
		double u = this.generator.nextDouble();
		double sample = - Math.log1p(-u) / this.l;
		return sample;
	}

}
