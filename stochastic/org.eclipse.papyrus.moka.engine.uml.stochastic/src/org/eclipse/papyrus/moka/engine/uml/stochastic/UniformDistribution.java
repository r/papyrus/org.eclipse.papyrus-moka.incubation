/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.uml.stochastic;

public class UniformDistribution implements IDrawSample {
	
	private double lb;
	private double ub;
	private PseudoRandomGenerator generator;
	
	
	public UniformDistribution(final double lb, final double ub) {
		if (lb == Double.NaN  || ub == Double.NaN) {
			throw new IllegalArgumentException("Invalid argument: UniformDistribution cannot take NaN arguments.");
		}
		if (lb >= ub) {
			throw new IllegalArgumentException("Invalid argument: lower bound greater than upper bound");
		}
		this.lb = lb;
		this.ub = ub;
		this.generator = new PseudoRandomGenerator();
	}
	
	public Double nextSample() {
		
		double sample = (this.ub - this.lb) * this.generator.nextDouble() + this.lb;
		
		return sample;
	}
}
