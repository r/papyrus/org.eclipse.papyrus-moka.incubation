package org.eclipse.papyrus.moka.engine.uml.stochastic.semantics;

import java.util.List;

import org.eclipse.papyrus.moka.engine.uml.time.activities.Timed_ActivityExecution;
import org.eclipse.papyrus.moka.engine.uml.time.scheduling.de.DEScheduler;
import org.eclipse.papyrus.moka.engine.uml.time.scheduling.de.Event;
import org.eclipse.papyrus.moka.engine.uml.time.scheduling.de.actions.CallbackAction;
import org.eclipse.papyrus.moka.fuml.activities.IActivityExecution;

public class StochasticActivityExecution extends Timed_ActivityExecution {

	@Override
	public void destroy() {
		
		List<Event> events = DEScheduler.getInstance().getEvents();
		
		for (Event event : events) {
			CallbackAction callbackAction = event.getAction();
			if (callbackAction instanceof StochasticSendTimeEventOccurrence) {
				IActivityExecution activityExecution = ((StochasticSendTimeEventOccurrence) callbackAction).getActivityExecution();
				
				if (activityExecution.equals(this)) {
					return;
				}
			}
		}
		// Stop the object activation (if any), clear all types and destroy the
		// object as an extensional value.

		// Issue FUML12-33 Extensional values should have an unique identifier

		if (this.objectActivation != null) {
			this.objectActivation.stop();
			this.objectActivation = null;
		}
		this.types.clear();
		super.destroy();
	}

}
