/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.uml.stochastic.semantics;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.papyrus.moka.engine.uml.time.scheduling.de.DEScheduler;
import org.eclipse.papyrus.moka.engine.uml.time.scheduling.de.Event;
import org.eclipse.papyrus.moka.engine.uml.time.semantics.Actions.CompleteActions.TimedAcceptEventActionActivation;
import org.eclipse.papyrus.moka.engine.uml.time.semantics.CommonBehaviors.TimedEventOccurrence;
import org.eclipse.papyrus.moka.fuml.activities.IActivityExecution;
import org.eclipse.papyrus.moka.fuml.activities.IToken;
import org.eclipse.papyrus.moka.fuml.commonbehavior.IEventOccurrence;
import org.eclipse.papyrus.moka.fuml.debug.Debug;
import org.eclipse.papyrus.moka.fuml.loci.ISemanticVisitor;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IEvaluation;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IRealValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.RealValue;
import org.eclipse.papyrus.moka.fuml.structuredclassifiers.IObject_;
import org.eclipse.papyrus.moka.pssm.values.ISM_OpaqueExpressionEvaluation;
import org.eclipse.uml2.uml.AcceptEventAction;
import org.eclipse.uml2.uml.TimeEvent;
import org.eclipse.uml2.uml.Trigger;

public class StochasticAcceptEventActionActivation extends TimedAcceptEventActionActivation {

	private Map<Trigger, RealValue> evaluationValues;

	public StochasticAcceptEventActionActivation() {
		super();
		evaluationValues = new HashMap<>();
	}

	@Override
	public void fire(List<IToken> incomingTokens) {

		// Register the event accepter for this accept event action activation
		// with the context object of the enclosing activity execution
		// and wait for an event to be accepted.
		Debug.println("[fire] Action " + this.node.getName() + "...");
		this.getExecutionContext().register(this.eventAccepter);
		this.waiting = true;
		this.firing = false;
		this.suspend();

		pushEvents(((AcceptEventAction) this.getNode()).getTriggers(), this, this.getExecutionContext(),this.getGroup().getActivityExecution());
	}

	@Override
	public Boolean match(IEventOccurrence eventOccurrence) {
		boolean matches = false;

		if (eventOccurrence instanceof TimedEventOccurrence) {
			TimedEventOccurrence timedEventOccurrence = (TimedEventOccurrence) eventOccurrence;

			AcceptEventAction action = (AcceptEventAction) (this.node);

			for (Trigger trigger : action.getTriggers()) {
				if (trigger.getEvent() instanceof TimeEvent) {
					TimeEvent timeEvent = (TimeEvent) trigger.getEvent();
					if (timeEvent.getWhen() != null && timeEvent.getWhen().getExpr() != null) {
						IValue evaluatedInstant =  getEvaluationValue(trigger);

						if (evaluatedInstant instanceof IRealValue) {

							if (timeEvent.isRelative()) {
								matches = timedEventOccurrence.getReferenceInstance().getValue() + ((IRealValue) evaluatedInstant)
										.getValue() == timedEventOccurrence.getOccurrenceInstant().getValue();
							} else {
								matches = timedEventOccurrence.getOccurrenceInstant().equals(evaluatedInstant);
							}
						}
					}
				}

			}

		}
		return matches;

//		return eventOccurrence.matchAny(action.getTriggers());

//		evaluationValues.get(even)

		// Return true if the given event occurrence matches a trigger of the
		// accept event action of this activation.
//		AcceptEventAction action = (AcceptEventAction) (this.node);
//		return eventOccurrence.matchAny(action.getTriggers());
	}

	private void pushEvents(List<Trigger> triggers, ISemanticVisitor visitor, IObject_ context, IActivityExecution activityExecution) {
		// Register timers for triggers that may accept a time event in the future.
		// When the timer will fire the context object will receive a time event
		// occurrence.
		for (Trigger trigger : triggers) {
			pushEvent(trigger, visitor, context,activityExecution);
		}
	}

	private void pushEvent(Trigger trigger, ISemanticVisitor visitor, IObject_ context, IActivityExecution activityExecution) {
		// Register a timer for a trigger associated to a time event. The specification
		// of the timer consists in an event registered to the DEScheduler. This event
		// is time
		// stamped with the instant at which the timer shall fire.
		if (context != null && trigger != null && trigger.getEvent() instanceof TimeEvent) {
			TimeEvent timeEvent = (TimeEvent) trigger.getEvent();
			if (timeEvent.getWhen() != null && timeEvent.getWhen().getExpr() != null) {
				IEvaluation evaluation = context.getLocus().getFactory()
						.createEvaluation(timeEvent.getWhen().getExpr());
				if (evaluation != null) {
					if (evaluation instanceof ISM_OpaqueExpressionEvaluation) {
						((ISM_OpaqueExpressionEvaluation) evaluation).setContext(context);
					}
					IValue evaluationValue = evaluation.evaluate();
					
					if (evaluationValue instanceof RealValue) {
						RealValue evaluationRealValue = (RealValue) evaluationValue;
						
						evaluationValues.put(trigger, evaluationRealValue);

						if (getEvaluationValue(trigger) != null && getEvaluationValue(trigger) instanceof IRealValue) {
							double clockTime = DEScheduler.getInstance().getCurrentTime();
							Event clockEvent = new Event(((IRealValue) getEvaluationValue(trigger)).getValue(),
									new StochasticSendTimeEventOccurrence((IRealValue) getEvaluationValue(trigger),
											clockTime, visitor, context,activityExecution));
							if (timeEvent.isRelative()) {
								DEScheduler.getInstance().pushEvent(clockEvent);
							} else {
								DEScheduler.getInstance().pushEvent(clockEvent,
										((IRealValue) getEvaluationValue(trigger)).getValue());
							}
						}
					}

					
				}
			}
		}
	}

	public IValue getEvaluationValue(Trigger trigger) {
		return evaluationValues.get(trigger);
	}

}
