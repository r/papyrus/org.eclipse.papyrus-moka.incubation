/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.uml.stochastic;

public class TruncatedNormalDistribution extends NormalDistribution implements IDrawSample {

	private double mean;
	private double sd;
	private double lb;
	private double ub;
	private PseudoRandomGenerator generator;
	
	public TruncatedNormalDistribution(double mean, double sd, double lb, double ub) {
		super(mean, sd);
		if (lb == Double.NaN || ub == Double.NaN) {
			throw new IllegalArgumentException("Invalid argument : TruncatedNormalDistribution cannot take NaN argument.");
		} else if (ub > ub) {
			throw new IllegalArgumentException("Invalid argument : lower bound cannot be greater than upper bound.");
		}
		this.lb = lb;
		this.ub = ub;
	}
	
	public Double nextSample() {
		
		double sample = Double.NaN;
		
		if (this.sd == 0.0) {
			if (this.lb <= this.mean && this.mean <= this.ub) {
				sample = this.mean;
			}
		} else {
			double rho = 0.0;
			double u = this.generator.nextDouble();

			if (this.ub == Double.MAX_VALUE) {

				// Location-scale rescaling of the boundaries to standard normal distribution
				double standardised_lb = (this.lb - this.mean) / this.sd;

				double alpha = (standardised_lb + Math.sqrt(Math.pow(standardised_lb, 2) + 4.0)) / 2.0;
				TruncatedExponentialDistribution distribution = new TruncatedExponentialDistribution(alpha,
						standardised_lb);

				while (u > rho) {
					double z = distribution.nextSample();
					rho = Math.exp(-Math.pow((z - alpha), 2) / 2.0);
					u = this.generator.nextDouble();
					sample = z;
				}

				return sample;

			} else if (this.lb == Double.MIN_VALUE) {

				// Location-scale rescaling of the boundaries to standard normal distribution
				double standardised_ub = (this.ub - this.mean) / this.sd;

				double alpha = (-standardised_ub + Math.sqrt(Math.pow(standardised_ub, 2) + 4.0)) / 2.0;
				TruncatedExponentialDistribution distribution = new TruncatedExponentialDistribution(alpha,
						-standardised_ub);

				while (u > rho) {
					double z = distribution.nextSample();
					rho = Math.exp(-Math.pow((z - alpha), 2) / 2.0);
					u = this.generator.nextDouble();
					sample = z;
				}

			} else {

				// Location-scale rescaling of the boundaries to standard normal distribution
				double standardised_lb = (this.lb - this.mean) / this.sd;
				double standardised_ub = (this.ub - this.mean) / this.sd;

				UniformDistribution distribution = new UniformDistribution(standardised_lb, standardised_ub);

				while (u > rho) {
					double z = distribution.nextSample();
					if (standardised_lb <= 0 && 0 <= standardised_ub) {
						rho = Math.exp(-Math.pow(z, 2) / 2);
					} else if (standardised_ub < 0) {
						rho = Math.exp((Math.pow(this.ub, 2) - Math.pow(z, 2)) / 2.0);
					} else {
						rho = Math.exp((Math.pow(this.lb, 2) - Math.pow(z, 2)) / 2.0);
					}
					u = distribution.nextSample();
					sample = z;
				}
			}
			// We return the 'unrescaled' sample
			sample = this.mean + this.sd * sample;
		}
		return sample;
	}
}
