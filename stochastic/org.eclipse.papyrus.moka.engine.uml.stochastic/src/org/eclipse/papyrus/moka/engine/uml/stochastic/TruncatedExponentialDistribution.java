/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.uml.stochastic;

public class TruncatedExponentialDistribution extends ExponentialDistribution implements IDrawSample {

	private double bound;
	
	public TruncatedExponentialDistribution(double l, double bound) {
		super(l);
		if (bound == Double.NaN) {
			throw new IllegalArgumentException("Invalid argument: TruncatedExponentialDistribution cannot take NaN arguments.");
		}
		if (bound < 0) {
			throw new IllegalArgumentException("Invalid argument: the troncated bound must be positive.");
		}
		this.bound = bound;
	}
	
	public Double nextSample() {
		double u = this.generator.nextDouble();
		double sample =  - Math.log1p(-u) / this.l + this.bound;
		return sample;
	}
	
}
