/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.uml.stochastic;

public class DiscreteUniformDistribution implements IDrawSample {

	private int a;
	private int b;
	private PseudoRandomGenerator generator;
	
	public DiscreteUniformDistribution(int a, int b) {
		if (a > b) {
			throw new IllegalArgumentException("Invalid argument: lower bound cannot be greater than upper bound.");
		}
		this.a = a;
		this.b = b;
		this.generator = new PseudoRandomGenerator();
	}
	
	public Double nextSample() {
		double sample = Double.valueOf(this.a);
		if (this.a != this.b) {
		// A verifier !!
			double u = this.generator.nextDouble();
			sample = Math.ceil((this.b - this.a + 1) * u);
		}
		return sample;
	}
}
