/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.uml.stochastic.semantics;

import org.eclipse.papyrus.moka.engine.uml.time.actions.SendTimeEventOccurrence;
import org.eclipse.papyrus.moka.engine.uml.time.scheduling.de.DEScheduler;
import org.eclipse.papyrus.moka.fuml.activities.IActivityExecution;
import org.eclipse.papyrus.moka.fuml.loci.ISemanticVisitor;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IRealValue;
import org.eclipse.papyrus.moka.fuml.structuredclassifiers.IObject_;
import org.eclipse.papyrus.moka.fuml.structuredclassifiers.IReference;
import org.eclipse.papyrus.moka.fuml.structuredclassifiers.Reference;

public class StochasticSendTimeEventOccurrence extends SendTimeEventOccurrence {
	private IRealValue evaluationValue;
	private IActivityExecution activityExecution;
	

	public StochasticSendTimeEventOccurrence(IRealValue evaluationValue, double referenceInstant,
			ISemanticVisitor visitor, IObject_ target, IActivityExecution activityExecution) {
		super(referenceInstant, visitor, target);
		this.evaluationValue = evaluationValue;
		this.activityExecution = activityExecution;
	}

	@Override
	public void execute() {
		// Register a time event occurrence to the target object. This
		// enables the target object classifier behavior to react (if possible)
		// to the fact that clock time has evolved.
		StochasticEventOccurrence eventOccurrence = new StochasticEventOccurrence();
		eventOccurrence.setReferenceInstant(this.referenceInstant);
		eventOccurrence.setOccurrenceInstant(DEScheduler.getInstance().getCurrentTime());
		IReference targetReference = new Reference();
		targetReference.setReferent(this.target);
		eventOccurrence.setTarget(targetReference);
		eventOccurrence.register();
		eventOccurrence.setEvaluationValue(evaluationValue);
	}

	public IActivityExecution getActivityExecution() {
		return activityExecution;
	}
}
