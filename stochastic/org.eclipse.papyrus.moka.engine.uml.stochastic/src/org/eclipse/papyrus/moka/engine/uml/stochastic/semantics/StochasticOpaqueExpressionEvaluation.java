/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.uml.stochastic.semantics;

import org.eclipse.papyrus.moka.engine.uml.stochastic.DistributionsInterpreter;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IRealValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IValue;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.RealValue;
import org.eclipse.papyrus.moka.pssm.values.SM_OpaqueExpressionEvaluation;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.PrimitiveType;

public class StochasticOpaqueExpressionEvaluation extends SM_OpaqueExpressionEvaluation {

	@Override
	public IValue evaluate() {

		if (specification instanceof OpaqueExpression) {
			OpaqueExpression opaqueExpression = (OpaqueExpression) specification;

			if (opaqueExpression.getBodies().size() == 1) {
				String expression = opaqueExpression.getBodies().get(0);
				double sample = DistributionsInterpreter.matchDistribution(expression);
				if (sample != Double.NaN) {
//					if(sample < 0) {
						Math.abs(sample);
//					}
					IRealValue result = new RealValue();
					result.setValue(sample);
					result.setType((PrimitiveType) this.locus.getFactory().getBuiltInType("Real"));

					return result;
				} else {
					System.out.println("Pb !!!");
				}

			}

		}

		return super.evaluate();
	}

}