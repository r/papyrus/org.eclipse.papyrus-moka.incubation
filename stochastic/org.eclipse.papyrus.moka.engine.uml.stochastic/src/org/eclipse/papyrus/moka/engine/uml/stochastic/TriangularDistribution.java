/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.uml.stochastic;

public class TriangularDistribution implements IDrawSample {

	private double a;
	private double b;
	private double c;
	private PseudoRandomGenerator generator;
	
	public TriangularDistribution(double a, double b, double c) {
		if (a == Double.NaN || b == Double.NaN || c == Double.NaN) {
			throw new IllegalArgumentException(
					"Invalid argument: TriangularDistribution cannot accept NaN parameters");
		}
		if (!(a <= c && c <= b)) {
			throw new IllegalArgumentException(
					"Invalid argument: parameters must be ordered as follows a < c < b.");
		}
		this.a = a;
		this.b = b;
		this.c = c;
		this.generator = new PseudoRandomGenerator();
	}
	
	public TriangularDistribution(double a, double b) {
		if (a == Double.NaN || b == Double.NaN) {
			throw new IllegalArgumentException(
					"Invalid argument: TriangularDistribution cannot accept NaN parameters");
		}
		if (a >= b) {
			throw new IllegalArgumentException(
					"Invalid argument: b must be greater than a.");
		}
		this.a = a;
		this.b = b;
		this.c = (b - a) / 2.0;
		this.generator = new PseudoRandomGenerator();
	}
	
	public Double nextSample() {
		double u = this.generator.nextDouble();
		double sample;
		if (u <= (this.c - this.a) / (this.b - this.a)) {
			sample = Math.sqrt((this.b - this.a) * (this.c - this.a) * u) + this.a;
		} else {
			sample = Math.sqrt((this.b - this.a) * (this.c - this.a) * (1 - u)) + this.b;
		}
		return sample;
	}
	
}
