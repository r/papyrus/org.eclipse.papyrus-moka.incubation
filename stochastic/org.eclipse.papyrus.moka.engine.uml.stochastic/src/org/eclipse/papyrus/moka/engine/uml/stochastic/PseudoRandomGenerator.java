/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.uml.stochastic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class PseudoRandomGenerator {

	private long seed;
	private long a;
	private long b;
	private long m;
	
	public PseudoRandomGenerator(long seed, long a, long b, long m) {
		if (!(m > 0)) {
			throw new IllegalArgumentException("Invalid argument: modulus must be greater than 0.");
		} else if (!(0 < a && a < m)) {
			throw new IllegalArgumentException("Invalid argument: multiplier must belong to (0, m).");
		} else if (!(0 <= b && b < m)) {
			throw new IllegalArgumentException("Invalid argument: increment must belong to [0, m).");
		} else if (!(0 <= seed && seed < m)) {
			throw new IllegalArgumentException("Invalid argument: initial seed must belong to [0, m).");
		}
		
		this.seed = seed;
		this.a = a;
		this.b = b;
		this.m = m;
	}
	
	public PseudoRandomGenerator() {
		/* 
		 * Attributes are generated 'randomly' by shuffling the currentTime.
		 */
		long date = System.currentTimeMillis();
		String date_str = Long.toString(date);
		date_str = date_str.replaceAll("0", "1");
		List<Character> date_integers = new ArrayList<>();
		for (char ch : date_str.toCharArray()) {
			date_integers.add(ch);
		}
		
		Collections.shuffle(date_integers);
		
		String seed_str = ListOfChar2String.ListOfCharToString(date_integers.subList(0, 4));
		String a_str = ListOfChar2String.ListOfCharToString(date_integers.subList(4, 6));
		String b_str = ListOfChar2String.ListOfCharToString(date_integers.subList(6, 8));
		String m_str = ListOfChar2String.ListOfCharToString(date_integers.subList(8, 13));
		
		this.seed = Long.valueOf(seed_str);
		this.a = Long.valueOf(a_str);
		this.b = Long.valueOf(b_str);
		this.m = Long.valueOf(m_str);
	}
	
	public void updateSeed() {
		long new_seed = (a * this.seed + b) % m;
		this.seed = new_seed;
	}
	
	public Double nextDouble() {
		Random random = new Random(this.seed);
		updateSeed();
		return random.nextDouble();
	}
	
	public long getSeed() {
		return this.seed;
	}
	
	public long getMultiplier() {
		return this.a;
	}
	
	public long getAdditive() {
		return this.b;
	}
	
	public long getModulus() {
		return this.m;
	}
	
}
