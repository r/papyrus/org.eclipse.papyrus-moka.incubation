/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.uml.stochastic.semantics;

import org.eclipse.papyrus.moka.engine.uml.time.semantics.CommonBehaviors.TimedEventOccurrence;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IEvaluation;
import org.eclipse.papyrus.moka.fuml.simpleclassifiers.IRealValue;
import org.eclipse.papyrus.moka.pssm.values.ISM_OpaqueExpressionEvaluation;
import org.eclipse.uml2.uml.TimeEvent;
import org.eclipse.uml2.uml.Trigger;

public class StochasticEventOccurrence extends TimedEventOccurrence {
	private IRealValue evaluationValue;

	@Override
	public boolean match(Trigger trigger) {
		// Define the rule to match this event occurrence against a trigger
		// This event occurrence matches if the following condition hold:
		// 1] If the trigger is for a TimeEvent
		// 2] If the time expression can be evaluated
		// 3] If the occurrence instant (i.e., the time at which this occurrence
		// occurred) matches the time at which the model element referencing the
		// trigger was expected to fire. Note that the triggering time can either
		// relative or absolute.
		boolean matches = false;
		if (trigger.getEvent() instanceof TimeEvent) {
			TimeEvent timeEvent = (TimeEvent) trigger.getEvent();
			if (timeEvent.getWhen() != null && timeEvent.getWhen().getExpr() != null) {

				IRealValue evaluatedInstant = evaluationValue;

				if (evaluatedInstant == null) {
					IEvaluation evaluation = this.target.getReferent().getLocus().getFactory()
							.createEvaluation(timeEvent.getWhen().getExpr());
					if (evaluation != null) {
						if (evaluation instanceof ISM_OpaqueExpressionEvaluation) {
							((ISM_OpaqueExpressionEvaluation) evaluation).setContext(this.target.getReferent());
						}
						evaluatedInstant = (IRealValue) evaluation.evaluate();
					}
				}

				if (evaluatedInstant != null) {
					if (timeEvent.isRelative()) {
						matches = this.referenceInstant.getValue()
								+ evaluatedInstant.getValue() == this.occurrenceInstant.getValue();
					} else {
						matches = this.occurrenceInstant.equals(evaluatedInstant);
					}
				}
			}
		}
		return matches;
	}

	public void setEvaluationValue(IRealValue evaluationValue) {
		this.evaluationValue = evaluationValue;
	}
}
