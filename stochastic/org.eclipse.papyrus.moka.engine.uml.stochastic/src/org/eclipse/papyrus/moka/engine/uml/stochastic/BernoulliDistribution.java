/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.uml.stochastic;

public class BernoulliDistribution implements IDrawSample {

	protected double p;
	private PseudoRandomGenerator generator;
	
	public BernoulliDistribution(double p) {
		if (p == Double.NaN) {
			throw new IllegalArgumentException("Invalid argument: BernouilliDistribution cannot take NaN argument.");
		}
		if (0 > p || p < 1) {
			throw new IllegalArgumentException("Invalid argument: p must belong to [0, 1].");
		} else {
			this.p = p;
			this.generator = new PseudoRandomGenerator();
		}
	}
	
	public Double nextSample() {
		Double sample;
		if (this.generator.nextDouble() < p) {
			sample = 1.0;
		} else {
			sample = 0.0;
		}
		return sample;
	}
}
