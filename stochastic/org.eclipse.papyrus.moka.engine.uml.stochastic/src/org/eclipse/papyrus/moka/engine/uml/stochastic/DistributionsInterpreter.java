/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.uml.stochastic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DistributionsInterpreter {

	private static final String UNIFORM = "uniform";
	private static final String EXPONENTIAL = "exponential";
	private static final String TRUNCATED_EXPONENTIAL = "troncated_exponential";
	private static final String POISSON = "poisson";
	private static final String GEOMETRIC = "geomtric";
	private static final String NORMAL = "normal";
	private static final String TRUNCATED_NORMAL = "troncated_normal";
	private static final String CUSTOM = "custom";

	private static final List<String> distribution_names_librairy = Arrays.asList(UNIFORM, EXPONENTIAL,
			TRUNCATED_EXPONENTIAL, POISSON, GEOMETRIC, NORMAL, TRUNCATED_NORMAL, CUSTOM);


	public static double matchDistribution(String expression) {

		String pattern = "(.*)\\((.*)\\)";
		Pattern r = Pattern.compile(pattern);

		Matcher m = r.matcher(expression);

		if (!m.find()) {
			throw new IllegalArgumentException(
					"Invalid argument: expression must be of the form 'distribution_name(parameters)'.");
		} else {
			String distribution_name = m.group(1);
			if (distribution_names_librairy.contains(distribution_name)) {
				String parameters = m.group(2).replaceAll("\\s", "");
				String[] params = parameters.split(",");
				List<Double> parameters_list = new ArrayList<Double>();

				double sample = 0;

				IDrawSample distribution = null;

				switch (distribution_name) {
				case UNIFORM:
					if (params.length != 2) {
						throw new IllegalArgumentException(
								"Invalid argument: uniform distribution parameters must be two floats.");
					}
					for (String param : params) {
						try {
							parameters_list.add(Double.valueOf(param));
						} catch (NumberFormatException e) {
							throw new IllegalArgumentException(
									"Invalid argument: uniform distribution parameters must be two floats.");
						}
					}
					double a = parameters_list.get(0);
					double b = parameters_list.get(1);
					distribution = new UniformDistribution(a, b);

					break;

				case EXPONENTIAL:
					if (params.length != 1) {
						throw new IllegalArgumentException(
								"Invalid argument: exponential distribution parameter must be a strictly positive float.");
					}
					for (String param : params) {
						try {
							parameters_list.add(Double.valueOf(param));
						} catch (NumberFormatException e) {
							throw new IllegalArgumentException(
									"Invalid argument: exponential distribution parameter must be a strictly positive float.");
						}
					}
					double l = parameters_list.get(0);
					distribution = new ExponentialDistribution(l);

					break;

				case TRUNCATED_EXPONENTIAL:
					if (params.length != 2) {
						throw new IllegalArgumentException(
								"Invalid argument: truncated exponential distribution parameter must be two strictly positive floats.");
					}
					for (String param : params) {
						try {
							parameters_list.add(Double.valueOf(param));
						} catch (NumberFormatException e) {
							throw new IllegalArgumentException(
									"Invalid argument: truncated exponential distribution parameter must be two strictly positive floats.");
						}
					}
					l = parameters_list.get(0);
					double bound = parameters_list.get(1);
					distribution = new TruncatedExponentialDistribution(l, bound);
					break;

				case POISSON:
					if (params.length != 1) {
						throw new IllegalArgumentException(
								"Invalid argument: poisson distribution parameter must be a strictly positive float.");
					}
					for (String param : params) {
						try {
							parameters_list.add(Double.valueOf(param));
						} catch (NumberFormatException e) {
							throw new IllegalArgumentException(
									"Invalid argument: exponential distribution parameter must be a strictly positive float.");
						}
					}
					l = parameters_list.get(0);
					distribution = new PoissonDistribution(l);
					break;

				case GEOMETRIC:
					if (params.length != 1) {
						throw new IllegalArgumentException(
								"Invalid argument: geometric distribution parameter must be a float.");
					}
					for (String param : params) {
						try {
							parameters_list.add(Double.valueOf(param));
						} catch (NumberFormatException e) {
							throw new IllegalArgumentException(
									"Invalid argument: geometric distribution parameter must be a float.");
						}
					}
					double p = parameters_list.get(0);
					distribution = new GeometricDistribution(p);
					break;
					
				case NORMAL:
					if(params.length != 2) {
						throw new IllegalArgumentException(
								"Invalid argument: normal distribution parameters must be two floats.");
					}
					for (String param : params) {
						try {
							parameters_list.add(Double.valueOf(param));
						} catch (NumberFormatException e) {
							throw new IllegalArgumentException(
									"Invalid argument: normal distribution parameters must be two floats.");
						}
					}
					double mean = parameters_list.get(0);
					double sd = parameters_list.get(1);
					distribution = new NormalDistribution(mean, sd);
					
					break;
					
				case TRUNCATED_NORMAL:
					if(params.length != 2) {
						throw new IllegalArgumentException(
								"Invalid argument: truncated normal distribution parameters must be four floats.");
					}
					for (String param : params) {
						try {
							parameters_list.add(Double.valueOf(param));
						} catch (NumberFormatException e) {
							throw new IllegalArgumentException(
									"Invalid argument: truncated normal distribution parameters must be four floats.");
						}
					}
					mean = parameters_list.get(0);
					sd = parameters_list.get(1);
					double lb = parameters_list.get(2);
					double ub = parameters_list.get(3);
					distribution = new TruncatedNormalDistribution(mean, sd, lb, ub);
				}
					
				
				sample = distribution.nextSample();
				return sample;
				
			} else {
				throw new IllegalArgumentException(
						"Invalid argument: name must belong to 'uniform', 'normal', 'poisson', 'custom'}.");
			}
		}
	}

}
