/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.uml.stochastic;

import java.util.Random;

public class NormalDistribution implements IDrawSample {

	private double mean;
	private double sd;
	private PseudoRandomGenerator generator;
	
	public NormalDistribution(double mean, double sd) {
		if (mean == Double.NaN || sd == Double.NaN) {
			throw new IllegalArgumentException(
					"Invalid argument: NormalDistribution cannot take NaN argument");
		}
		if (sd < 0) {
			throw new IllegalArgumentException(
					"Invalid argument: standard deviation must be positive.");
		}
		this.mean = mean;
		this.sd = sd;
		this.generator = new PseudoRandomGenerator();
	}
	
	public Double nextSample() {
		double sample;
		if (this.sd == 0) {
			sample = this.mean;
		} else {
			Random random = new Random(this.generator.getSeed());
			sample = random.nextGaussian();
		}
		return sample;
	}
	
}
