/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.moka.engine.uml.stochastic.engine;

import org.eclipse.papyrus.moka.engine.uml.stochastic.semantics.StochasticExecutionFactory;
import org.eclipse.papyrus.moka.engine.uml.time.UMLTimedExecutionEngine;
import org.eclipse.papyrus.moka.engine.uml.time.semantics.Loci.TimedLocus;
import org.eclipse.papyrus.moka.fuml.loci.ILocus;
import org.eclipse.papyrus.moka.pscs.loci.CS_Executor;

public class UMLStochasticExecutionEngine extends UMLTimedExecutionEngine {


	/**
	 * Create and parameterize the locus
	 */
	@Override
	public ILocus createLocus() {
		ILocus locus = new TimedLocus();
		locus.setExecutor(new CS_Executor());
		locus.setFactory(new StochasticExecutionFactory());
		return locus;
	}
}
